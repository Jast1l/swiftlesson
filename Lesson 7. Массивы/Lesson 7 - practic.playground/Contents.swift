let constArray = ["a" , "b" , "c" , "d"]
constArray.count
print (constArray.count)

var array : [String]
array = ["e" , "f" , "g" , "h"]

if array.count == 0 {
    print ("Array is empty")
} else {
    print ("Array is full")
}

if array.isEmpty {
     print ("Array is empty")
} else {
   print ("Array is full")
}

array += constArray
array.append("i")

array += ["j"]

var array2 = array

array2[0] = "1"

array
array2

array2[1...4] //включает крайнюю границу
array2[1..<4] //не включая крайнюю границу
array[1...3] = ["0"]
array

array.insert("-", at: 2) //добавление нового элемента между существующими

array.remove(at: 2) //удаление (возвращение) элемента под индексом...
array

//let test = [Int](repeatElement(100, count: 10)) //заполнение одинаковыми значениями

let money = [100 , 20 , 0 , 5 , 1 , 1 , 100 , 50 , 1 , 2 , 20]

// сучий тест...
//for var i in 0..<9 {
//    print ("i = \(i)")
//    }

var sum = 0

//for i in 0..<money.count { // работа с индексом
//    print("i = \(i) is called \(money[i]) and Sum = \(sum)")
//    sum += money[i]
//}
//print (sum)



//for i in 0..<9 {
//    print("i = \(i)")
//}

//for i in money {            // работа с целым массивом
//    print("i = \(i) and Sum = \(sum)")
//    sum += i
//}
//print (sum)

for (index , value) in money.enumerated() {    // возврат массива к тюплу индекс:значение
        print("index = \(index) and value = \(value), sum = \(sum)")
        sum += value
    }
    print (sum)

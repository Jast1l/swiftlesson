//Block 1 ___________________________________________________________________________________

let yearData = [31 , 28 , 31 , 30 , 31 , 30 , 31 , 31 , 30 , 31 , 30 , 31]
print ("Array of day in month \(yearData)")

for (index, data) in yearData.enumerated() {
    print ("index month - \(index) day in \(data)")
}

print ("\n")

let yearName = ["january" , "february" , "march" , "april" , "may" , "june" , "july" , "august" , "september" , "october" , "november" , "december"]
print ("Array of name months in year \(yearName)")

for name in 0..<12 {
    print ("index month - \(name) and name - \(yearName[name])")
}

print ("\n")

print ("Сomposite loop of two arrays")
for number in 0..<yearName.count {
    print ("in \(yearName[number]) - \(yearData[number]) day")
//    print ("\"\(yearName[number])\" : \"\(yearData[number])\"")
}

//Вариант ручной сборки
//let calendar = ["january" : "31" , "february" : "28" , "march" : "31" , "april" : "30" , "may" : "31" , "june" : "30" , "july" : "31" , "august" : "31" , "september" : "30" , "october" : "31" , "november" : "30" , "december" : "31"]
//print (calendar)

print ("\n")

var yearTuples = [(month : String , day : Int)] () //инициализация пустого массива

for i in 0..<yearName.count {
    let temp = (month : yearName[i] , day : yearData[i])
    yearTuples.append(temp)
}
print (yearTuples)
//yearTuples[1].day
//yearTuples[1].month
//yearTuples[1].self


print ("\n")


//reverser - разврот печати массива в обратную сторону
print ("Разворот отображаемых данных из массива месяцев в году")
for i in (0..<yearName.count).reversed() {
    print ("\(yearName[i])")
}

//yearName = yearName.reversed()   //в случае если yearName - var
//print (yearName)


//подсчет дней до ДР на основании массива

let birthday = (month : 11 , day : 27)
var sumDay = 0

for i in 0..<(birthday.month - 1) {
    sumDay += yearData[i]
}
sumDay += birthday.day

print ("\n")
print ("Eсли день рождения \(birthday.day).\(birthday.month) с 1 января прошло \(sumDay) дней")


//Block 2 _____________________________________________________________________________________
print ("\nСумма опциональных интов с тестом на nil 3 способами:")
var numberOpt = [Int?]()
numberOpt = [1423 , 2314 , nil , nil , 2020]
var sum = 0

//Optional Binding
for i in 0..<numberOpt.count {
    if let test = numberOpt[i] {
        sum += test
    }
}
print ("\t\t\tOptional Binding - \(sum)")
sum = 0

//Force unwrapping
for i in 0..<numberOpt.count {
    if numberOpt[i] != nil {
       sum += numberOpt[i]!
    }
}
print ("\t\t\tForce unwrapping - \(sum)")
sum = 0

//Method of test for nil - "??"
for i in 0..<numberOpt.count {
    sum += numberOpt[i] ?? 0
}
print (" Method of test for nil \"??\" - \(sum)")


//Block3 _____________________________________________________________________________________________________________________________

let alphabet = "abcdefghijklmnopqrstuvwxyz"
var arrayABC = [String]()
//var index = 0

for i in alphabet {
    arrayABC.append(String(i))   //append(add) - творит хуйню и вставляет ххх раз
    //arrayABC.insert(String(i), at: 0)
}
print (arrayABC)

arrayABC = arrayABC.reversed()
print (arrayABC)




//var search : Character = "z"
//var index = 0
//
//for char in alphabet {
//    if char == search {
//        print ("index = \(index)")
//    }
//    index += 1
//}
//


//Пимечания
//Разбор сбора значения массива с запятыми и без
//var resultString = ""   // пример от кирилла
//let yearData = [31 , 28 , 31 , 30 , 31 , 30 , 31 , 31 , 30 , 31 , 30 , 31]
//
//for (index, value) in yearData.enumerated() {
//    if index == yearData.count - 1 {
//        resultString += "\(value)"
//    } else {
//        resultString += "\(value), "
//    }
//
//}
//print(resultString)


//resultString = ""   // второй вариант от Кирилла
//var index = 0
//for value in yearData {
//    if index == yearData.count - 1 {
//        resultString += "\(value)"
//    } else {
//        resultString += "\(value), "
//    }
//    index += 1
//}
//print(resultString)


//resultString = ""   третий вариант от Кирилла
//index = 0
//for value in yearData {
//    if yearData.count - 1 > index {
//        resultString += "\(value), "
//    } else {
//        resultString += "\(value)"
//    }
//    index += 1
//}
//print(resultString)


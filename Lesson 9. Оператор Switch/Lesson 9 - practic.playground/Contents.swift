let age = 55
let name = "Nic"

switch age {
    case 0...14:
        print ("Школьник")
    case 14...17:
        print ("Юноша")
    case 18 , 21:
        print ("Важные события впереди")
default:
    break
}

switch name {
    case "Serg" where age >= 30:
        print ("Все только начинается")
    default:
        break
}

var ageOld = age
var nameOld = name

var testTuple = (nameOld , ageOld)

switch testTuple {
    case ("Serg" , _):
        print("Привет братан")
    case ("Serg" , 31):
        print ("С 4-м десятком братан")
    case (_ , let testAge) where testAge >= 50 && testAge < 60:
        print ("Берегите свое здововье!")
default:
    break
}


mainLoop: for _ in 0...1000 {  //введение "закладки" на начало нужного цикла, чтобы его остановить
    for i in 0..<20 {
        if i == 10 {
            break mainLoop
        }
        print (i)
    }
}


let point = (5 , 5)

switch point {
    case (_ , let y) where y == 1:
        print ("y == 1")
    case (let x , let y) where x == y:
        print ("x == y")
    case let (x , y) where x != y:
        print ("x != y")
    default:
        break
}

//var array = [Int , Double , Float]()
//array = [5 , 5.4 , 5,4]

var array : [CustomStringConvertible] = [5 , 5.4 , Float(5.4)] // приведение к единому печатному стилю - плохой тон.

switch array[0] {
    case let a as Int:
        print ("Int")
    case _ as Float:
        print ("Float")
    case _ as Double:
        print ("Double")
    default:
        break
}

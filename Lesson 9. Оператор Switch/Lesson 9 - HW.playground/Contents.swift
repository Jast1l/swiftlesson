//Block 1 ________________________________________________________________________________________________________________________
print ("Block 1 ____________________________________________________________________________________\n\n\n")

let text = "В беседе или при знакомстве Геральт предпочитает представляться коротко: «Геральт из Ривии». Имя ему дала мать, однако сам он большую часть жизни полагал, что получил его от своего наставника — Весемира."

var sumVowels = 0
var sumVowelsBig = 0
var sumConsonats = 0
var sumConsonatsBig = 0
var sumPeriod = 0
var sumComma = 0
var sumHyphen = 0
var sumEmpty = 0

for (_ , value) in text.enumerated() {

switch value {
case "а", "е", "ё", "и", "о", "у", "ы", "э", "ю", "я":
    sumVowels += 1
case "А", "Е", "Ё", "И", "О", "У", "Ы", "Э", "Ю", "Я":
    sumVowelsBig += 1
case "б", "в", "г", "д", "ж", "з", "й", "к", "л", "м", "н", "п", "р", "с", "т", "ф", "х", "ц", "ч", "ш", "щ":
    sumConsonats += 1
case "Б", "В", "Г", "Д", "Ж", "З", "Й", "К", "Л", "М", "Н", "П", "Р", "С", "Т", "Ф", "Х", "Ц", "Ч", "Ш", "Щ":
    sumConsonatsBig += 1
case ".":
    sumPeriod += 1
case ",":
    sumComma += 1
case "—" , "-":
    sumHyphen += 1
case " ":
    sumEmpty += 1
default:
    break
    }
}

print ("""
Проверяемый текст:
    
\(text)

Текст содержит в себе:
     \(sumPeriod) - предложения
     \(sumVowels+sumVowelsBig) - гласных букв, из которых:
        \(sumVowels) - строчных
        \(sumVowelsBig) - заглавных
     \(sumConsonats+sumConsonatsBig) - согласных букв, из которых:
        \(sumConsonats) - строчных
        \(sumConsonatsBig) - заглавных
     \(sumPeriod+sumComma+sumHyphen) - знаков препинания:
        \(sumPeriod) - точек
        \(sumComma) - запятых
        \(sumHyphen) - дефис
""")





//Block 2  ________________________________________________________________________________________________________________________
print ("\n\n\nBlock 2 ____________________________________________________________________________________\n\n\n")

let age = 4

var resultString = "В возрасте \(age) лет человек находится в "

switch age {
case 0...3:
    resultString += "периоде \"младенчество\""
case 4...12:
    resultString  += "периоде \"дество\""
case 13...16:
    resultString += "периоде \"продростковый возраст\""
case 17...20:
    resultString += "периоде \"юношество\""
case 21...30:
    resultString  += "статусе \"молодой человек\""
case 31...50:
    resultString += "статусе \"седние годы\""
case 50...65:
    resultString  += "стутусе \"предпенсионный возраст\""
case _ where age >= 66:
    resultString += "стутусе \"пенсионер\""
default:
    break
}
print (resultString)



//Block 3  ________________________________________________________________________________________________________________________
print ("\n\n\nBlock 3 ____________________________________________________________________________________\n\n\n")

//префикс имя а или о оббращаемся по имени
//в противном случае
//префикс отвества в или д обращение по имени и отчеству
//в противном случае
//префикс фамилия е или з обращаемся только по фамилии
//если ни одно не сработало обращаемся по фио

var tupleName = (name: "Сергей", middleName: "Юрьевич", surName: "Пищальников")

switch tupleName {
    
case let (a , _ , _) where a.hasPrefix("А") || a.hasPrefix("О"):
    print ("Добрый день, \(tupleName.name)")
case let (_ , b , _) where b.hasPrefix("В") || b.hasPrefix("Д"):
    print ("Добрый день, \(tupleName.name) \(tupleName.middleName)")
case let (_ , _ , c) where c.hasPrefix("Е") || c.hasPrefix("З"):
    print ("Добрый день, \(tupleName.surName)")
default:
    print ("Добрый день, \(tupleName.surName) \(tupleName.name) \(tupleName.middleName)")
}



//Block 4  ________________________________________________________________________________________________________________________
print ("\n\n\nBlock 4 ____________________________________________________________________________________\n\n\n")


let x = 2       //min 1 max 10
let y = 2       //min 1 max 10

let point = (x , y)
resultString = "When firing at a point Х - \(point.0) : Y - \(point.1) you "

switch point {
case (1 , 1):               //single deck ship
    resultString += "killed the enemy ship"
case (2 , 7):               //single deck ship
    resultString += "killed the enemy ship"
case (8 , 2):               //single deck ship
    resultString += "killed the enemy ship"
case (6 , 7):               //single deck ship
    resultString += "killed the enemy ship"
case (2 , 9...10):          //two-deck ship
    resultString += "direct hit on an enemy ship"
case (4 , 7...8):           //two-deck ship
    resultString += "direct hit on an enemy ship"
case (6...7 , 9):           //two-deck ship
    resultString += "direct hit on an enemy ship"
case (3 , 2...4):           //three-deck ship
    resultString += "direct hit on an enemy ship"
case (5...7 , 4):           //three-deck ship
    resultString += "direct hit on an enemy ship"
case (9 , 7...10):          //three-deck ship
    resultString += "direct hit on an enemy ship"
default:
    resultString +=  "missed, try again!"
}
print (resultString)






//Заметки и пробы
//let vowels : [Character] = ["а", "е", "ё", "и", "о", "у", "ы", "э", "ю", "я" , "А", "Е", "Ё", "И", "О", "У", "Ы", "Э", "Ю", "Я"]
//let consonats : [Character] = ["б", "в", "г", "д", "ж", "з", "й", "к", "л", "м", "н", "п", "р", "с", "т", "ф", "х", "ц", "ч", "ш", "щ"]
//let period : [Character] = ["."]
//let comma : [Character] = [","]
//let hyphen = ["—"]

//print (sumVowels)
//print (sumConsonats)
//print (sumPeriod)
//print (sumComma)
//print (sumHyphen)
//print (sumEmpty)

/* РЕШЕНИЕ КИРИЛЛА - ОЧЕНЬ КОНСТРУКТИВНОЕ ПУТЕМ СОЗДАНИЯ НОВОЙ ПЕРЕМЕННОЙ
var surName: String?
var name: String?
var lastName: String?
var age: Int?
var isAdmin: Bool?
let emptyText = "Не задано"

surName = "Ковыршин"
name = "Кирилл"
lastName = "Анатольевич"
age = 22

if surName != nil || name != nil || lastName != nil || age != nil || isAdmin != nil  {
    if let surName = surName {
        print("Фамилия: \(surName)")
    } else {
        print("Фамилия:  \(emptyText)")
    }
    if let name = name {
        print("Имя: \(name)")
    } else {
        print("Имя:  \(emptyText)")
    }
    if let lastName = lastName {
        print("Отчество: \(lastName)")
    } else {
        print("Отчество:  \(emptyText)")
    }
    if let age = age {
        //Вычесляем падежии
        var ageName = ""
        let ageNum = abs(age) % 100;
        let ageNumTwo = age % 10;
        if ageNum > 10 && ageNum < 20 {
            ageName = "лет"
        }   else if ageNumTwo > 1 && ageNumTwo < 5 {
            ageName = "года"
        } else if ageNumTwo == 1 {
            ageName = "год"
            
        } else {
            ageName = "лет"
        }
        //
        
        print("Возраст: \(age) \(ageName)")
    } else {
        print("Возраст:  \(emptyText)")
    }
    if let isAdmin = isAdmin {
        print("Админские права: \(isAdmin ? "Да" : "Нет" )")
    } else {
        print("Админские права:  \(emptyText)")
    }
} else {
    print("Данные все пустые")
}
*/




/* МОЕ РЕШЕНИЕ - НАВОРОЧЕННО ПУТЕМ СИЛОВЫХ СВОРАЧИВАНИЙ
var name : String? = "Сергей"; let textName = "Имя"
var surName : String? = "Пищальников"; let textSurName = "Фамилия"
var nickName : String? = "Jason"; let textNickName = "Позывной"
var age : Int? = 32; let textAge = "Возраст"
var isAdmin = false

//test nil
//name = nil
//surName = nil
//nickName = nil
//age = nil

//авто-определение прав
let admName1 = "Сергей"
let admNickName1 = "Jason"
let admName2 = "Кирилл"
let admNickName2 = "Oxy"

//Block atantion
let atentionNoData = "Данные не заполнены"
let atentionNoFullData = "Добрый день! Проверьте заполненные данные:"
let welcome = "Добрый день!"
let atentionText = " Не заполнено"
let adminOn = "Права администратора подтверждены"
let adminOff = "Вы вошли как пользователь"
let admniNoData = "Вход не выполнен"

if name == nil && surName == nil && nickName == nil && age == nil {
    print (atentionNoData)
} else if name == nil || surName == nil || nickName == nil || age == nil {
    print (atentionNoFullData)
} else {
    print (welcome)
if name == nil {
    print ("\(textName):\t\(atentionText)")
        } else {
            print ("\(textName):\t\(name!)")
    }
if surName == nil {
    print ("\(textSurName):\t\(atentionText)")
        } else {
            print ("\(textSurName):\t\(surName!)")
    }
if nickName == nil {
    print ("\(textNickName):\t\(atentionText)")
        } else {
            print ("\(textNickName):\t\(nickName!)")
    }
if age == nil {
    print ("\(textAge):\t\(atentionText)")
        } else {
            print ("\(textAge):\t\(age!)")
    }
}

//Block прав
if (name == admName1 && nickName == admNickName1) || (name == admName2 && nickName == admNickName2) {
    isAdmin = true
}
if isAdmin {
    print (adminOn)
} else if name == nil || nickName == nil {
    print (admniNoData)
} else {
        print (adminOff)
}
*/






/* 3 ПРАВИЛА ОТ КИРИЛЛА
 НВ Долг Кирилл, [26 авг. 2020 г., 12:56:02 (26 авг. 2020 г., 13:01:16)]:
 let age = "60"

 //Force Unwrapping
 if Int(age) != nil {
     print(Int(age)! + 1)
 }

 //Unwrapping optional
 if let ageInt = Int(age) {
     print(ageInt + 1)
 }

 //Guard Unwrapping
 guard let ageInt = Int(age) else {
     print("в переменной age не только цифры")
          return
 }
 print(ageInt + 1)

 Вот тебе 3 абсолютно одинаковых примере, разница только в методе работы с Optional Type
 Каждый из 3 этих методов применяется в зависимости от нужд
 Первый Force Unwrapping - применяется когда, нам не нужна переменная, куда нужно это развернуть
 */

 


/* МИНИ ТЗ
var userName = "Сергей"
var isAdmin = false
if userName == "Кирилл" || userName == "Сергей" {
    print ("Привет, Друг!")
    isAdmin = true
        if isAdmin == false {
            print ("Права: пользователь")
        } else {
            print ("Права: Админ")
    }
} else {
    print ("Привет, Незнакомец")
}
*/



//func sayHello(name: String, closure: ()->()) {
//    print("Привет, \(name)")
//    closure()
//    for value in 1...100 {
//        print(value)
//    }
//
//
//
//}
//
//sayHello(name: "Сергей") {
//    print("Очень рад тебя видеть!")
//}
//
//
//func printHelloWithString(name: String, lastName: String, closure:(String)->()){
//    print("Очень рад тебя видеть!")
//    sleep(3)
//    let resultString = "\(lastName) \(name)"
//    closure(resultString)
//}
//
//
//printHelloWithString(name: "Сергей", lastName: "Пищальников") {
//    print("Привет, \($0)")
//}
//
//func printHelloWithStringBack(name: String, lastName: String, closure:(String, String)->(String)) -> String{
//    let resultClosure = closure(lastName, name)
//    return resultClosure
//}
//
//let fullname = printHelloWithStringBack(name: "Сергей", lastName: "Пищальников") { (lastname, name) -> (String) in
//    let resultString = "\(lastname) \(name)!"
//    return resultString
//}
//
//print(fullname)
//
//
//var money = 0
//var moneyReserv = 0
//
//func culcMyMoney(money: inout Int, closure: ()->(Int)) {
//    money += closure()
//}
//
//culcMyMoney(money: &moneyReserv) { () -> (Int) in
//    return 100
//}
//
//culcMyMoney(money: &money) { () -> (Int) in
//    return 110
//}
//
//culcMyMoney(money: &moneyReserv) { () -> (Int) in
//    return 120
//}
//
//culcMyMoney(money: &money) { () -> (Int) in
//    return -130
//}
//
//print(money)
//print(moneyReserv)
//
//
//let fio = printHelloWithStringBack(name: "Кирилл", lastName: "Ковыршин") { (lastName, name) -> (String) in
//    return "\(lastName) \(name)"
//}
//
//let conteiner = [["name": "Кирилл"], ["name": "Алексей"], ["name": "Сергей"], ["name": "Вячеслав"]]
//
//let temp = conteiner.filter { (dict) -> Bool in
//    return dict["name"] == "Сергей"
//}
//print(temp)
//
//let a = [99,50,30,21,1,5,3,0]
//
//a.sorted()
//
//let tempTwo = a.sorted {
//    return $0 > $1
//}
//
//print(tempTwo)

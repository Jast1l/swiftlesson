//<Block 1: ФИО студента
let surName = "Пищальников"
let name = "Сергей" //Переменная должна быть с маленькой буквы
let lastName = "Юрьевич"
print ("Ф.И.О. студента: \(surName) \(name) \(lastName)") //Так красивее

//Block 2: стилистика
print ("\n") //Пустая срока лучше оформять так

//Block 3: Характеристики студента
let age = 32
let weight = 82
let height = 180
print ("Физиологические данные\nВозраст: \(age) года\nРост: \(height) см\nВес: \(weight) кг")

//question: "\(Name) \(lastName)" - как корректно зафиксировать ПРОБЕЛ между выводимыми значениями в одной строке?

/*  то что мы делаем в циклах для последующего использования в функции
let wallet = [100, 5, 1, 5, 5, 20, 50, 100, 1, 1]
var sum = 0

for value in wallet {
    sum += value
}

sum
*/

/*   пример использования функции
func calculetMoney() {
    print ("calculetMoney")
}

calculetMoney()
*/

/* функция без возврата значения (просто принимает задачу)
func calculetMoney (wallet: [Int]) {
    var sum = 0
    for value in wallet {
        sum += value
    }
    print ("sum = \(sum)")
}

let wallet = [100, 5, 1, 5, 5, 20, 50, 100, 1, 1]

calculetMoney(wallet: wallet)
*/

/* функция с возвращением и готовая к использованию в дальнейшем
func calculetMoney (wallet: [Int]) -> Int {
    var sum = 0
    for value in wallet {
        sum += value
    }
    return sum
}

let wallet = [100, 5, 1, 5, 5, 20, 50, 100, 1, 1]

let money = calculetMoney (wallet: wallet)
*/

/*
func calculetMoney (wallet: [Int], type: Int) -> Int {
    var sum = 0
    for value in wallet {
        if value == type {
        sum += value
        }
    }
    return sum
}

let wallet = [100, 5, 1, 5, 5, 20, 50, 100, 1, 1]

let money = calculetMoney (wallet: wallet, type: 1)

calculetMoney(wallet: wallet, type: 5)
*/

/* возвращение тюпла с несколькими параметрами
func calculetMoney (wallet: [Int], type: Int) -> (total: Int, count: Int) {
    var sum = 0
    var count = 0
    
    for value in wallet {
        if value == type {
        sum += value
        count += 1
        }
    }
    return (sum, count)
}

let wallet = [100, 5, 1, 5, 5, 20, 50, 100, 1, 1]

let (money, count) = calculetMoney(wallet: wallet, type: 100)

money
count
*/

/*
func calculetMoney (wallet: [Int], type: Int?) -> (total: Int, count: Int) {
    var sum = 0
    var count = 0
    
    for value in wallet {
        
        if (type == nil) || (value == type!) {
        sum += value
        count += 1
        }
    }
    return (sum, count)
}

let wallet = [100, 5, 1, 5, 5, 20, 50, 100, 1, 1]

let (money, count) = calculetMoney(wallet: wallet, type: nil)

money
count
*/


/* Использование дефолтного значения для дальнейшего сокращения условий функций
func calculetMoney (wallet: [Int], type: Int? = nil) -> (total: Int, count: Int) {
    var sum = 0
    var count = 0
    
    for value in wallet {
        
        if (type == nil) || (value == type!) {
        sum += value
        count += 1
        }
    }
    return (sum, count)
}

let wallet = [100, 5, 1, 5, 5, 20, 50, 100, 1, 1]

let (money, count) = calculetMoney(wallet: wallet)

money
count
*/

/* Использование название внешних и локальных переменных или при идентичных названиях #wallet
func calculetMoney (inWallet wallet: [Int], withType type: Int? = nil) -> (total: Int, count: Int) {
    var sum = 0
    var count = 0
    
    for value in wallet {
        
        if (type == nil) || (value == type!) {
        sum += value
        count += 1
        }
    }
    return (sum, count)
}

let wallet = [100, 5, 1, 5, 5, 20, 50, 100, 1, 1]

let (money, count) = calculetMoney (inWallet: wallet)

money
count

calculetMoney(inWallet: wallet, withType: 100).total
calculetMoney(inWallet: wallet, withType: 100).count
*/


/*
func calculateMoney (inSequence range: Int...) -> Int {
    var sum = 0
    for value in range {
    sum += value
    }
    return sum
}

calculateMoney(inSequence: 5, 5, 10, 2, 3, 4, 3, 23, 34)
*/

func sayHi() -> () {
    print ("hi")
}

//sayHi()
//sayHi()

let hi = sayHi
hi

func sayPhrase (phrase: String) -> Int? {
    print (phrase)
    return 0
}

sayPhrase(phrase: "aaa")

let phrase = sayPhrase

phrase ("bbb")


func doSomething (whatToDo: ()->()) {
    whatToDo()
}

func whatToDo() -> () -> () {
    func printSomething() {
        print ("Hello world!!!")
    }
    return printSomething
}

doSomething (whatToDo: sayHi)

whatToDo()()  // вызывает только при заполнении условий

let iShouldDoThis = whatToDo()
iShouldDoThis()


import UIKit

//print ("Block 1 ________________________________________________________________________________\n")
//
//func wolf () -> (String) {
//    return "\u{1F43A}"
//}
//func owl () -> (String) {
//    return "\u{1F989}"
//}
//func fir () -> (String) {
//   return "\u{1F332}"
//}
//func blackHeart () -> (String) {
//    return "\u{1F5A4}"
//}
//
//print ("\(wolf()) \(owl()) \(fir()) \(blackHeart())")
//
////
//print ("\n\nBlock 2____________________________________________________________________________\n")
//
//
//func chess (letter: String, number: Int) -> String {
//    let horizontal = ["a", "b", "c", "d", "e", "f", "g", "h"]
//    for (index, value) in horizontal.enumerated() {
//        if value == letter.lowercased() && (number >= 1 && number <= 8){
//            if (index + 1) % 2 == number % 2 {
//                return "При выбранной точке \(letter):\(number) её цвет Black"
//            } else {
//                return "При выбранной точке \(letter):\(number) её цвет White"
//            }
//        } else {
//            break
//        }
//    }
//    return """
//Укажите верное значение:
//    по вертикали цифровое от 1 до 8
//    по горизонтали буквенное от "a" до "h"
//"""
//}
//
//let point = chess(letter: "a", number: 1)
//
//print (point)
//
//
//
//print ("\n\nBlock 3____________________________________________________________________________\n")
//
//
//
//// функция принимающая массив для реверса
//
//let yearName = ["january" , "february" , "march" , "april" , "may" , "june" , "july" , "august" , "september" , "october" , "november" , "december"]
//
//func revers (array: [String]) -> [String] {
//    return array.reversed()
//}
//
//let newArray = revers(array: yearName)
//print (newArray)
//
//// функция работающая с разным кол-вом элементов для реверса
//
//func reversRange (sequence: String...) -> [String] {
//    var temp = [String]()
//    for i in sequence {
//        temp.append(String(i))
//    }
//    let reversArray = revers(array: temp)    //использование функции в функции
//        return reversArray
//}
//
//let secondArray = reversRange(sequence: "january" , "february" , "march" , "april" , "may" , "june" , "july" , "august" , "september" , "october" , "november" , "december")
//
//print (secondArray)
//


//print ("\n\nBlock 4____________________________________________________________________________\n")
//
//var yearName = ["january" , "february" , "march" , "april" , "may" , "june" , "july" , "august" , "september" , "october" , "november" , "december"]
//
//func revers (array: inout [String]) {
//    var tempArray = [String]()
//    tempArray = array.reversed()
//    array = tempArray
//}
//
//revers(array: &yearName)
//
//print ("В четвертом задании не было задачи вывода на печать")


//print ("\n\nBlock 5____________________________________________________________________________\n")
//
//let text = "Поддержать добрым словом человека, попавшего в беду, часто так же важно, как вовремя переключить стрелку на железнодорожном пути: всего один дюйм отделяет катастрофу от плавного и безопасного движения по жизни."
//
//func textTransform (addText: String) -> String {
//    var resultString = addText
//    let bigLetters = ["а", "е", "ё", "и", "о", "у", "ы", "э", "ю", "я"]
//    let secondBig = ["б", "в", "г", "д", "ж", "з", "й", "к", "л", "м", "н", "п", "р", "с", "т", "ф", "х", "ц", "ч", "ш", "щ"]
//    let punctuation = [",", ":", "."]
//    for value in resultString {
//        let stringChar = String(value).lowercased()
//
//        if bigLetters.contains(stringChar) {
//            resultString = resultString.replacingOccurrences(of: stringChar, with: stringChar.lowercased())
//        }
//        if secondBig.contains(stringChar) {
//            resultString = resultString.replacingOccurrences(of: stringChar, with: stringChar.uppercased())
//        }
//        if punctuation.contains(stringChar) {
//            resultString = resultString.replacingOccurrences(of: stringChar, with: " ")
//        }
//    }
//    return resultString
//}
//
//let testPrint = textTransform(addText: text)
//
//print (testPrint)

import UIKit
import Foundation

//1. Создать структуру “Описание файла” содержащую свойства:
//- путь к файлу
//- имя файла
//- максимальный размер файла на диске
//- путь к папке, содержащей этот файл
//- тип файла (скрытый или нет)
//- содержимое файла (можно просто симулировать контент)
//
//Главная задача - это использовать правильные свойства там, где нужно, чтобы не пришлось хранить одни и те же данные в разных местах и т.д. и т.п.

//2. Создайте энум, который будет представлять некую цветовую гамму. Этот энум должен быть типа Int и как raw значение должен иметь соответствующее 3 байтное представление цвета. Добавьте в этот энум 3 свойства типа: количество цветов в гамме, начальный цвет и конечный цвет.

//3. Создайте класс человек, который будет содержать имя, фамилию, возраст, рост и вес. Добавьте несколько свойств непосредственно этому классу чтобы контролировать:
//- минимальный и максимальный возраст каждого объекта
//- минимальную и максимальную длину имени и фамилии
//- минимально возможный рост и вес
//- самое интересное, создайте свойство, которое будет содержать количество созданных объектов этого класса


//MARK: -Block1

let MaxFileSizeOnDisk : Double = 10

func arrayTostring (array: [String]) -> String {
    var resultString = ""
    for (_, value) in array.enumerated() {
        resultString += "\(value)/"
    }
    return resultString
}

struct FileDescription {
    var name : String {
        didSet {
            name = name.capitalized
        }
    }
    var size : Double {
        didSet {
            if size > MaxFileSizeOnDisk {
                print ("Внимание! Размер файла превышает допустимое значение")
            }
        }
    }

    var path : String

    var pathToFolder : String {
        get {
            var words = path.components(separatedBy: "/")
            words.removeLast()
            return arrayTostring(array: words)
        }
    }

    static var type = "Txt"

    lazy var сontent = "аыфоатлифлаоиги3гиаи  йиашгиыоаифа тиарфымиовли3шгигиориаол фиыа мафыиалотыфа иаиыфоилоаи фаотфиыалоифлыоиаифыароиыфа"
}

//test

var file = FileDescription(name: "Текстовый документ", size: 8, path: "Мой компьютер/Мои документы/Наработки/Записи по планировке/Текстовый документ")

file.path = "Мой компьютер/Мои документы/Наработки/Записи по планировке/Тест/Текстовый документ"
file.pathToFolder

file.size = 34
print (file.pathToFolder)

FileDescription.type = "Doc"



//MARK: -Block 2
//2. Создайте энум, который будет представлять некую цветовую гамму. Этот энум должен быть типа Int и как raw значение должен иметь соответствующее 3 байтное представление цвета. Добавьте в этот энум 3 свойства типа: количество цветов в гамме, начальный цвет и конечный цвет.

enum Palitra : Int {

    case red = 0xFF0000
    case green = 0x00FF00
    case blue = 0x0000FF
    
    static var colorCount = 3
    static var firstColor = Palitra.red.rawValue
    static var lastColor = Palitra.blue.rawValue
    
    
}

let test = Palitra.blue

Palitra.firstColor



//MARK: -Block3
//3. Создайте класс человек, который будет содержать имя, фамилию, возраст, рост и вес. Добавьте несколько свойств непосредственно этому классу чтобы контролировать:
//- минимальный и максимальный возраст каждого объекта
//- минимальную и максимальную длину имени и фамилии
//- минимально возможный рост и вес
//- самое интересное, создайте свойство, которое будет содержать количество созданных объектов этого класса

let MaxSymbolOfText = 15

class Human {
    var name : String {
        didSet {
            name = name.capitalized

            if name.count > MaxSymbolOfText {
                print ("Внимание! По причине превышения допустиммого кол-ва символов (\(name)) возвращено старое значение имени - \(oldValue)")
                name = oldValue
            }
        }
    }

    var surName : String {
        didSet {
            surName = surName.capitalized

            if surName.count > MaxSymbolOfText {
                print ("Внимание! По причине превышения допустиммого кол-ва символов (\(surName)) возвращено старое значение фамилии - \(oldValue)")
                surName = oldValue
            }
        }
    }

    var fullName : String {
        get {
            return "\(name) \(surName)"
        }

        set {
            let words = newValue.components(separatedBy: " ")
            if words.count > 0 {
                name = words[0]
            }
            if words.count > 1 {
                surName = words[1]
            }
        }
    }

    static var minAge = 10
    static var maxAge = 80

    var age : Int {
        didSet {
            if age < Human.minAge || age > Human.maxAge {
                print ("Внимание! По причине выхода из допустимого диаппазона значений (\(age)) возвращено старое значение возраста - \(oldValue)")
                age = oldValue
            }
        }
    }

    static var minWeight : Float = 30.5
    static var maxWeight : Float = 92.7

    var weight : Float {
        didSet {
            if weight < Human.minWeight || weight > Human.maxWeight {
                print ("Внимание! По причине выхода из допустимого диаппазона значений (\(weight)) возвращено старое значение возраста - \(oldValue)")
                weight = oldValue
            }
        }
    }

    static var totalHuman = 0

    init(name: String, surName: String, age: Int, weight: Float) {
        self.name = name
        self.surName = surName
        self.age = age
        self.weight = weight

        Human.totalHuman += 1
    }
}

var peopleOne = Human(name: "Сергей", surName: "Пищальников", age: 32, weight: 84.5)

peopleOne.name = "Константинович"

var peopleTwo = Human(name: "Александр", surName: "Лисин", age: 13, weight: 54)

peopleTwo.weight = 98
peopleTwo.age

Human.maxAge = 100

peopleTwo.age = 99

peopleTwo.age

peopleOne.fullName

var peopleThree = Human(name: "Мария", surName: "Кожевникова", age: 30, weight: 58)

peopleThree.fullName
peopleThree.fullName = "Мария Пищальникова"

peopleThree.surName

Human.totalHuman


Human.maxAge = 30

let peopleFour = Human(name: "Человек", surName: "Москвичев", age: 28, weight: 82)

peopleFour.age = 42




import UIKit
import Foundation

let MaxNameLenght = 20   // глобальная переменная

class Human {
    var name : String {
        didSet {
            if name.count > MaxNameLenght {
                name = oldValue
            }
        }
    }
    
  //  let maxAge = 100           //в стоарой версии нельзя было использовать как сторид проперти не ипользуемые переменныеБ
    class var maxAge :Int {      //поэтому их выводили на кампьютед проперти
        return 100
    }
    
    lazy var storyOfMyLife = "This is sory of my life"    //введение лейзи проперти, которая будет нил до первого к ней обращения при вызове
     
    var age : Int {
        didSet {
            if age > Human.maxAge {
                age = oldValue
            }
        }
    }
    
    
    
    init(name: String, age: Int) {   //maxAge не был запущен в инициализатор - что позволяет скрыть его
        self.name = name
        self.age = age
    }
}

struct Cat {
    
    var name : String {
        didSet {
            if name.count > MaxNameLenght {
                name = oldValue
            }
        }
    }
    
//    var maxAge = 20        // в структуре не нужен инициализатор, если условие имеет значение - оно не будет выведено для заполнения
                           // let cat = Cat(name: "Whiten", age: 10)
                           // если инициализировать условие с типом данных - необходимо будет заполенние при вызове структуры
    static let maxAge = 20
    static var totalCats = 0     //ввод счетчика вызовов инициализатора
    
    var age : Int {
        didSet {
            if age > Cat.maxAge {
                age = oldValue
            }
        }
    }
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
        
        Cat.totalCats += 1      //изменение счетчика при запуске инициализатора, хотя сам инициализатор по факту в струтуре не нужен
    }
    
}


let human = Human(name: "Peter", age: 40)   // сейчаслейзи lazy проперти - nil

let cat = Cat(name: "Whiten", age: 10)

human
human.storyOfMyLife       //оращение к lazy проперти
human                     //ntgthm lazy проперти больше не nil, а принеля установленное знаечение


human.name
cat.name





Cat.totalCats       //проверка счетчика - вызывается по запросу имени структуры (не переменной, которой она присвоена)

let cat1 = Cat(name: "Whiten1", age: 10)
let cat2 = Cat(name: "Whiten2", age: 10)

Cat.totalCats       //проверка счетчика - вызывается по запросу имени структуры (не переменной, которой она присвоена)


//MARK: такаяя же система  static работает и для ENUMов

enum Direction {
    
    static let enumDirection = "Direction in the game"
    
    case Left
    case Right
    case Top
    case Bottom
    
    var isVertical : Bool {
        return self == .Top || self == .Bottom
    }
    
    var isGorizontal : Bool {
        return !isVertical
    }
}

Direction.Left
Direction.enumDirection

let test = Direction.Right
test.isGorizontal

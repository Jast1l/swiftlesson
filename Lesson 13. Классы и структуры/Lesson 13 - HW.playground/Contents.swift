//1. Создайте структуру студент. Добавьте свойства: имя, фамилия, год рождения, средний бал. Создайте несколько экземпляров этой структуры и заполните их данными. Положите их всех в массив (журнал).
//
//2. Напишите функцию, которая принимает массив студентов и выводит в консоль данные каждого. Перед выводом каждого студента добавляйте порядковый номер в “журнале”, начиная с 1.
//
//3. С помощью функции sorted отсортируйте массив по среднему баллу, по убыванию и распечатайте “журнал”.
//
//4. Отсортируйте теперь массив по фамилии (по возрастанию), причем если фамилии одинаковые, а вы сделайте так чтобы такое произошло, то сравниваются по имени. Распечатайте “журнал”.
//
//5. Создайте переменную и присвойте ей ваш существующий массив. Измените в нем данные всех студентов. Изменится ли первый массив? Распечатайте оба массива.
//
//6. Теперь проделайте все тоже самое, но не для структуры Студент, а для класса. Какой результат в 5м задании? Что изменилось и почему?
//
//007. Уровень супермен

//MARK: -Block 1
//print ("________Block 1_________________________________________________________________")

struct Student {
    var name : String
    var surName : String
    var ageOld : Int
    var scoreMid : Int
}

let student1 = Student(name: "Сергей", surName: "Пищальников", ageOld: 1987, scoreMid: 20)

let student2 = Student(name: "Кирилл", surName: "Ковыршин", ageOld: 1987, scoreMid: 21)

let student3 = Student(name: "Кирилл", surName: "Пищальников", ageOld: 1994, scoreMid: 16)

let jurnal = [student1, student2, student3]

//MARK: -Block 2
//print ("\n\n________Block 2_________________________________________________________________")

func printJurnal (array: [Student]) {
    for (index, student) in array.enumerated() {
        
        print ("\(index + 1). \(student.surName) \(student.name) \(student.ageOld) \(student.scoreMid)")
    }
}

//printJurnal(array: jurnal)


//MARK: -Block 3
//print ("\n\n________Block 3_________________________________________________________________")

let studentSortedArray = jurnal.sorted { (student, student1) -> Bool in
    return student.scoreMid > student1.scoreMid
}

//printJurnal(array: studentSortedArray)


//MARK: -Block 4
//print ("\n\n________Block 4_________________________________________________________________")

let secondStudentSortedArray = jurnal.sorted { (student, student1) -> Bool in
    if student.surName != student1.surName {
        return student.surName < student1.surName
    } else {
        return student.name < student1.name
    }
}

printJurnal(array: secondStudentSortedArray)

//MARK: -Block 6
//print ("\n\n________Block 6_________________________________________________________________")

class StudentClass {
    var name : String
    var surName : String
    var ageOld : Int
    var scoreMid : UInt
    init(name: String, surName: String, ageOld: Int, scoreMid: UInt) {
        self.name = name
        self.surName = surName
        self.ageOld = ageOld
        self.scoreMid = scoreMid
    }
}

//MARK: -Block 7
//print ("\n\n________Block 7_________________________________________________________________")

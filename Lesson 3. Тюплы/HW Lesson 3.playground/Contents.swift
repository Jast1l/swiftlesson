let heading = "Текущие максимальные силовые показатели"
let all = "Спортсмен"

//print description - (Sortsmen 1)
var nameAthlete = "Сергей"
var nickNameAthlete = "Jason"
let sportName = "\(nameAthlete) \"\(nickNameAthlete)\""

//print description - (Sortsmen 2)
var nameSecondAthlete = "Маша"
var nickNameSecondAthlete = "Belka"
let sportNameSecond = "\(nameSecondAthlete) \"\(nickNameSecondAthlete)\""

//data (Sortsmen 1)
var (pushUps, pullUps, squats) = (15, 15, 50)

//data (Sortsmen 2)
var sport = (pushUps:15, pullUps:1, squats:70)

//workout name
let printPush = "Отжимания"
let printPull = "Подтягивания"
let printSquats = "Приседания"
//workout name 2 (test)
//sport.0 or sport.printPush
//sport.1 or sport.printPull
//sport.2 or sport.printSquats

//print result (Sortsmen 1)
print ("\(heading)\n\n\(all) \(sportName):")
print ("\t\(printPush) - \(pushUps)")
print ("\t\(printPull) - \(pullUps)")
print ("\t\(printSquats) - \(squats)")

//print result (Sortsmen 2)
print ("\n\(all) \(sportNameSecond):")
print ("\t\(printPush) - \(sport.pushUps)")
print ("\t\(printPull) - \(sport.pullUps)")
print ("\t\(printSquats) - \(sport.squats)")

//comparison
print ("\nСравнение достижений")
// comments
let item = "В упражнении :"
let ab = "может сделать больше повторений чем "
let c = "делают одинаковое количество повторений "

/*
let difPushUps1 = (pushUps - sport.0)
let difPushUps2 = (sport.0 - pushUps)
let difPullUps1 = (pullUps - sport.1)
let difPullUps2 = (sport.1 - pullUps)
let difSquats1 = (squats - sport.2)
let difSquats2 = (sport.2 - squats)
 использование abs(pushUps - sport.0) для положительного значения
*/

/*
 let a = 10
 let b = 12
 let c = a - b
 if a >= b {
     print(c)
 } else {
     print(-(c))
 }
 */

/*
 let a = 10
 let b = 12
 let c = a - b
 if a >= b {
     print(c)
 } else {
     print(-(c))
 }
 */
 
/*
 let a = 10000
 let b = 12000
 var c = 0
 if a >= b {
      c = a - b
     print(c)
 } else {
     c = -(a - b)
     print(-(c))
 }
 */

/*
 let a = 10000
 let b = 12000
 var c = 0
 if a >= b {
      c = a - b
 } else {
     c = -(a - b)

 }

 print("У меня в кошельке: \(c)"
 */

/*
 var a = 1
 a += 1
 
 var resultString = "Привет,"
 resultString += " Джейсон"
 print(resultString)
 */

// pushUps
if pushUps > sport.pushUps {
    print ("\(item) \"\(printPush)\" - \(sportName) \(ab) \(sportNameSecond) на \(abs(pushUps - sport.0))")
} else if pushUps < sport.pushUps {
    print ("\(item) \"\(printPush)\" - \(sportNameSecond) \(ab) \(sportName) на \(abs(pushUps - sport.0))")
} else {
    print ("\(item) \"\(printPush)\" - \(sportName) и \(sportNameSecond) \(c)")
}


/* модуль упращенной корректной строки
var resultString = ""
resultString += "\(item) \"\(printPush)\" -"
if pushUps > sport.pushUps {
    resultString += "\(sportName) \(ab) \(sportNameSecond) на \(abs(pushUps - sport.0))"

} else if pushUps < sport.pushUps {
    resultString += "\(sportNameSecond) \(ab) \(sportName) на \(abs(pushUps - sport.0))"
} else {
     resultString += "\(sportName) и \(sportNameSecond) \(c)"
}
print(resultString)
*/

// pullUps
if pullUps > sport.pullUps {
    print ("\(item) \"\(printPull)\" - \(sportName) \(ab) \(sportNameSecond) на \(abs(pullUps - sport.1))")
} else if pullUps < sport.pullUps {
    print ("\(item) \"\(printPull)\" - \(sportNameSecond) \(ab) \(sportName) на \(abs(pullUps - sport.1))")
} else {
    print ("\(item) \"\(printPull)\" - \(sportName) и \(sportNameSecond) \(c)")
}

// squats  - (test tuples index)
if squats > sport.2 {
    print ("\(item) \"\(printSquats)\" - \(sportName) \(ab) \(sportNameSecond) на \(abs(squats - sport.2))")
} else if pullUps < sport.2 {
    print ("\(item) \"\(printSquats)\" - \(sportNameSecond) \(ab) \(sportName) на \(abs(squats - sport.2))")
} else {
    print ("\(item) \"\(printSquats)\" - \(sportName) и \(sportNameSecond) \(c)")
}

//question - видео урок 3 (35:40) не совсем понял что он имеет ввиду поменять местами используя переменную





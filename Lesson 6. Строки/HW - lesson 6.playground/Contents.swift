import Foundation

//Block 1
//var one = "1423"
//var two = "2314"
//var three = "2020"
//var four = "NW"
//var five = "NW1423"
//var six = "Wolf"
//
//var resultSum = 0
//var resultString = ""
//
//var testOne = Int(one) ?? 0
//var testTwo = Int(two) ?? 0
//var testThree = Int(three) ?? 0
//var testFour = Int(four) ?? 0
//var testFive = Int(five) ?? 0
//var testSix = Int(six) ?? 0
//
//print ("Вывод на экран результатов сложения двумя способами:")
//
//// consolidation
//var consolidationSum = testOne + testTwo + testThree + testFour + testFive + testSix
//print ("Консолидация строк - \(testOne) + \(testTwo) + \(testThree) + \(testFour) + \(testFive) + \(testSix) = \(consolidationSum)")
//
//
//// string concatenation
//resultSum += Int(one) ?? 0
//resultString += Int(one) != nil ? "\(Int(one)!)" : " + nil"
//
// resultSum += Int(two) ?? 0
//resultString += Int(two) != nil ? " + \(Int(two)!)" : " + nil"
//
// resultSum += Int(two) ?? 0
//resultString += Int(three) != nil ? " + \(Int(three)!)" : " + nil"
//
// resultSum += Int(two) ?? 0
//resultString += Int(four) != nil ? " + \(Int(four)!)" : " + nil"
//
// resultSum += Int(two) ?? 0
//resultString += Int(five) != nil ? " + \(Int(five)!)" : " + nil"
//
// resultSum += Int(two) ?? 0
//resultString += Int(six) != nil ? " + \(Int(six)!)" : " + nil"
//
//
//print ("Конкатенация строк - \(resultString) = \(resultSum)")
//

//Block 2
//let test = "\u{1F602} + \u{1F428} + \u{2650}"
//test.count //9
//(test as NSString).length  //11



//Block 3

//
//let alphabet = "abcdefghijklmnopqrstuvwxyz"
//var search : Character = "z"
//var index = 0
//
//for char in alphabet {
//    if char == search {
//        print ("index = \(index)")
//    }
//    index += 1
//}
//



//преределка под комментарии Кирилла!!!

// мое не элегантное решение
//if let testOne = Int(one) {
//    resultSum += testOne
//    resultString += one
//    } else {
//    resultString += " + nil"
//}
//if let testTwo = Int(two){
//    resultSum += testTwo
//    resultString += " + \(two)"
//    } else {
//    resultString += " + nil"
//}
//if let testThree = Int(three) {
//    resultSum += testThree
//    resultString += " + \(three)"
//    } else {
//    resultString += " + nil"
//}
//if let testFour = Int(four) {
//    resultSum += testFour
//    resultString += "+ \(four)"
//    } else {
//    resultString += " + nil"
//}
//if let testFive = Int(five) {
//    resultSum += testFive
//    resultString += " + \(five)"
//    } else {
//    resultString += " + nil"
//}
//if let testSix = Int(six) {
//    resultSum = testSix
//    resultString += " + \(six)"
//    } else {
//    resultString += " + nil"
//}

////var str : String              суммирование Строки
//var str = "c"
//
//str = str + "a"
//str += "b"

var str1 = "a"
var str2 = str1

str1               //значение до изменения
str2

str1 = "b"         //изменение - начало области видимости

str1               //значение в области видимости (измененное)
str2

var resultString = str1
resultString += str2

print (resultString) // print - ba


str1.isEmpty

let char1 : Character = "x"
print (char1)

//let c = ""

for c in "Hello world!" {
    print (c)
}

let sparklingHeart = "\u{1F496}"
sparklingHeart

str1.append(char1)
print (str1)

let gal = "\u{2705}"

let eAcute: Character = "\u{E9}" // é
let combinedEAcute: Character = "\u{65}\u{301}" // e с последующим
// eAcute равен é, combinedEAcute равен é

let testAcute = "Привет всем людям!\u{301}\u{20dd}"

testAcute.count // изменение вместо countElements в прошлых версиях

testAcute.hasPrefix("Привет")
testAcute.hasSuffix("!")
testAcute.hasSuffix("!\u{301}\u{20dd}") //считывает единое значение условий для последнего символа

import UIKit
import Foundation

//MARK: - 1. Для нашей структуры Point перегрузить операторы: -, -=, prefix —, postfix —, /, /=, *=

//MARK: - 2. Создать структуру Rect, аналог CGRect, содержащую структуру Size и Point. Перегрузить операторы +, +=, -, -= для этой структуры.

//MARK: - 3. Перегрузить оператор + и += для String, но второй аргумент должен быть Int

//MARK: - 4. Создать свой оператор, который будет принимать 2 String и в первом аргументе, при совпадении буквы с вторым аргументом, менять совпадения на заглавные буквы



//MARK: - 1. Для нашей структуры Point перегрузить операторы: -, -=, prefix —, postfix —, /, /=, *=

struct Point {
    var x : Int
    var y : Int
}

var p1 = Point(x: 2, y: 3)
var p2 = Point(x: 3, y: 5)

// перегрузка -
func - (a: Point, b: Point) -> Point {
    return Point(x: a.x - b.x, y: a.y - b.y)
}
let p3 = p1 - p2


// перегрузка -=
func -= (a: inout Point, b: Point){
    a = a - b
}
p1 -= p2


// перегрузка prefix —-
prefix func -- (a: inout Point) -> Point {
    a.x -= 1
    a.y -= 1
    return a
}
--p1


// перегрузка postfix —-
postfix func -- (a: inout Point) -> Point {
    let b = a
    --a
    return b
}
p1--


// перегрузка postfix /
func / (a: Point, b: Point) -> Point {
    return Point(x: a.x / b.x, y: a.y / b.y)
}
let p4 = p1 / p2


// перегрузка /=
func /= (a: inout Point, b: Point){
    a = a / b
}
p1 /= p2


// перегрузка *=
func * (a: Point, b: Point) -> Point {
    return Point(x: a.x * b.x, y: a.y * b.y)
}
let p5 = p1 * p2

func *= (a: inout Point, b: Point){
    a = a * b
}
p1 *= p2


//MARK: - 2. Создать структуру Rect, аналог CGRect, содержащую структуру Size и Point. Перегрузить операторы +, +=, -, -= для этой структуры.


struct Size {
    var width : Int
    var height : Int
}

struct Rect {
    var startPoint : Point
    var size : Size
    
    init (startPoint: Point, size: Size) {
        self.startPoint = startPoint
        self.size = size
    }
    
    init (x: Int, y: Int, wight: Int, height: Int) {
        self.startPoint = Point(x: x, y: y)
        self.size = Size(width: wight, height: height)
    }
}
//var r1 = Rect(startPoint: Point.init(x: <#T##Int#>, y: <#T##Int#>), size: Size.init(width: <#T##Int#>, height: <#T##Int#>))

var r1 = Rect(x: 2, y: 2, wight: 5, height: 5)
var r2 = Rect(x: 0, y: 0, wight: 10, height: 10)

func + (figOne: Rect, figTwo: Rect) -> Rect {
    let point = Point(x: figOne.startPoint.x + figTwo.startPoint.x, y: figOne.startPoint.y + figTwo.startPoint.y)
    let size = Size(width: figOne.size.width + figTwo.size.width, height: figOne.size.height + figTwo.size.height)
    
    return Rect(startPoint: point, size: size)
}
var test1 = r1 + r2
test1.startPoint.x
test1.startPoint.y
test1.size.width
test1.size.height


func - (figOne: Rect, figTwo: Rect) -> Rect {
    let point = Point(x: figOne.startPoint.x - figTwo.startPoint.x, y: figOne.startPoint.y - figTwo.startPoint.y)
    let size = Size(width: figOne.size.width - figTwo.size.width, height: figOne.size.height - figTwo.size.height)
    
    return Rect(startPoint: point, size: size)
}
var test2 = r1 - r2
test2.startPoint.x
test2.startPoint.y
test2.size.width
test2.size.height


func += (figOne: inout Rect, figTwo: Rect){
    figOne = figOne + figTwo
}
r1 += r2
r1.startPoint.x
r1.startPoint.y
r1.size.width
r1.size.height

func -= (figOne: inout Rect, figTwo: Rect){
    figOne = figOne - figTwo
}
r1 -= r2
r1.startPoint.x
r1.startPoint.y
r1.size.width
r1.size.height


//MARK: - 3. Перегрузить оператор + и += для String, но второй аргумент должен быть Int

func + (mainString: String, additionalString: Int) -> String {
    return mainString + String(additionalString)
    
}
func += (mainString: inout String, additionalString: Int) {
    mainString = mainString + String(additionalString)
}
var testOne = "Индекс"
var testTwo = 2
let testFree = testOne + testTwo
testOne += testTwo


//MARK: - 4. Создать свой оператор, который будет принимать 2 String и в первом аргументе, при совпадении буквы с вторым аргументом, менять совпадения на заглавные буквы

infix operator ~^
func ~^ (strOne: inout String, strTwo: String) {
    var saveStrOne = strOne
    strOne = ""
    
    for letterOne in saveStrOne {
        for letterTwo in strTwo {
            if letterOne.lowercased() == letterTwo.lowercased() {
                saveStrOne = letterOne.uppercased()
                break
            } else {
                saveStrOne = String(letterOne)
            }
        }
        strOne.append(saveStrOne)
    }
}

var testString = "Тестовая строка для замены строчных букв на заглавные"
testString ~^ "овая"

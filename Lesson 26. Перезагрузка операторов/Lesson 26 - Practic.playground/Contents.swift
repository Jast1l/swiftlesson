import UIKit
import Foundation

struct Point {
    var x : Int
    var y : Int
}

var p1 = Point(x: 2, y: 3)
var p2 = Point(x: 3, y: 5)

//p1 + p2   - невозможнг пока

// перегрузка +
func + (a: Point, b: Point) -> Point {
    return Point(x: a.x + b.x, y: a.y + b.y)
}

let p3 = p1 + p2
p1
p2

// перегрузка *
func * (a: Point, b: Point) -> Point {
    return Point(x: a.x * b.x, y: a.y * b.y)
}

let p4 = p1 * p2
p1
p2


// перегрузка +=
func += (a: inout Point, b: Point){
    a = a + b
}
p1 += p2
p1


// перегрузка ==
func == (a: Point, b: Point) -> Bool {
    return a.x == b.x && a.y == b.y
}


var a = 2
var b = 3

prefix func ++ (a: inout Point) -> Point {
    a.x += 1
    a.y += 1
    return a
}

postfix func ++ (a: inout Point) -> Point {
    let b = a
    ++a
    return b
}

p1 = Point(x: 2, y: 3)
p2 = Point(x: 3, y: 5)

++p1

print (p1++)
print (p1)
print (++p1)


//infix operator ** {associatedtype. left precedencegroup 120} - необходимо найти форму после обновления Swift



var s = "Hello, World!"
//s -= "lo"

func -= (s1: inout String, s2: String) {
    let set = NSCharacterSet(charactersIn: s2)
    let components : NSArray = s1.components(separatedBy: set as CharacterSet) as NSArray
    s1 = components.componentsJoined(by: "")
}

s -= "lo"
s -= "H"

s = "123456789"

func -= (s1: inout String, i: Int) {
    s1 -= String(i)
}

s -= 5784440

import Foundation
//Рассмотрим такую ситуацию: есть семья - папа, мама и дети.
//Папа глава семьи, у него есть Мама, Мама контролирует детей, т.е. иерархия: Папа - Мама - Дети, Дети на одном уровне.
//Дети могут вызывать друг друга и они могут искать пути, как общаться с другими Детьми, например говорить "дай игрушку", спрашивать Маму: "Мама, дай конфетку", общаться с Папой: "Папа, купи игрушку".
//Вся эта иерархия лежит в объекте класса Семья, у которого есть методы, например распечатать всю Семью, т.е. метод вернёт массив всех членов Семьи.
//У Папы есть 3 кложера (closures) - 1. когда он обращается к Семье - распечатать всю Семью, 2. распечатать Маму, 3. распечатать всех Детей.
//Создать всю иерархию со связями. При выходе из зоны видимости все объекты должны быть уничтожены. Используем Command-Line Tool.



class Father {
    var name : String
    var mather : Mather?
    var children : [Child]?
    
    init (name: String) {
        self.name = name
    }
    
    var printFamily : (Family) -> () = {
        (family: Family) in
        family.printFamily()
    }
    var printMather : (Mather) -> () = {
        (mather: Mather) in
        print ("\(mather.name) is mather of my children")
    }
    var printChildren : ([Child]) -> () = {
        (children: [Child]) in
        print ("My children is:")
        for child in children {
            print ("\(child.name)")
        }
    }
    
    deinit {
        print ("The father cycle is over")
    }
}

class Mather {
    var name : String
    unowned var father : Father?
    
    init (name: String) {
        self.name = name
    }
    
    deinit {
        print ("The mather cycle is over")
    }
}

class Child {
    var name : String
    unowned var father : Father
    unowned var mother : Mather
    
    static var arrayChild = [Child]()
    
    init (name: String, father: Father, mother: Mather){
        self.name = name
        self.father = father
        self.mother = mother
        Child.arrayChild.append(self)
    }
    
    func toChild (child: Child) {
        print ("Child \(self.name) asks the child \(child.name) to give a toy")
    }
    func toMather (mather: Mather) {
        print ("Child \(self.name) asks his mather to give him a toy")
    }
    func toFather (father: Father) {
        print ("Child \(self.name) asks his mather to buy a toy")
    }
    
    deinit {
        print ("The child cycle is over")
    }
}

class Family {
    var father : Father
    var mother : Mather
    var children : [Child]
    
    init (father: Father, mother: Mather, children : [Child]) {
        self.father = father
        self.mother = mother
        self.children = children
    }
    
    func printFamily() {
        
        print ("""
        The family consists of
        Fathers name is \(father.name)
        Mother name is \(mother.name)
        Children names is:
        """)
        
        var index = 1
        
        for child in children {
            print ("Child \(index) has name \(child.name)")
            index += 1
        }
    }
    
    deinit {
        print ("The family cycle is over")
    }
}



//MARK: -Playground
var playground = true

if playground {
    let father = Father(name: "Kent")
    let mather = Mather(name: "Barby")
    
    let child1 = Child(name: "Peter", father: father, mother: mather)
    let child2 = Child(name: "Ork", father: father, mother: mather)
    let child3 = Child(name: "Matumba", father: father, mother: mather)
    
    child3.toChild(child: child2)
    child2.toChild(child: child1)
    
    print ("\n ------")
    
    child3.toFather(father: father)
    child2.toMather(mather: mather)
    
    print ("\n ------")
    
    let children = Child.arrayChild
    
    let family = Family(father: father, mother: mather, children: children)
    
    father.printFamily(family)
    
    print ("\n ------")
    
    father.printMather(mather)
    
    print ("\n ------")
    
    father.printChildren(children)
    
    print ("\nКонец Playground")
}

print ("\nThe End")

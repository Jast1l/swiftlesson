import Foundation

//MARK: создание условия плэйграунда
//var playground = true
//
//class Student {
//    unowned var teacher : Teacher?     // слабая ссылка на объект - для уничтожения после прохождения цикла (weak - для опционалов)
//    deinit {
//        print ("goodbye student")
//    }
//    init (teacher : Teacher) {
//        self.teacher = teacher
//    }
//}
//
//class Teacher {
//    var student : Student?
//    var test : (() -> ())?
//
//    lazy var test2 : (Bool) -> () = {(a: Bool) in
//       // print (self)
//        print (self.student)
//    }
//
//    deinit {
//        print ("goodbye teacher")
//    }
//    init () {
//        self.student = Student(teacher: self)
//    }
//}
//
//var clouser3 : (() -> ())?
//
////MARK: плэйграунд
//if playground {
////    var t : Teacher?
//    var teacher = Teacher()
//    teacher.test2(true)
//
//    /*
//    teacher.test = {
//        [unowned teacher] in
//        print (teacher)
//    }
//    */
//
//
//    /*    if playground {
//     var student = Student(teacher: teacher)
//     teacher.student = student
//     }*/
//    //    var clouser3 : (() -> ())?
//
//    clouser3 = {
//        //        [unowned teacher] in
//        print (teacher)
//    }
//
//    //    t = teacher
//    print ("exit playground")
//}
//
//var x = 10
//var y = 20
//
//class Human {
//    var name = "a"
//}
//var h = Human()
//
//var clouser : () -> () = {
//    [x] in
//    print ("\(x) \(y)")
//}
//
//var clouser2 : (Int) -> Int = { [x, y] (a: Int) -> Int in
//    print ("\(x) \(y) \(h.name)")
//    return a
//}
//
//clouser2(1)
//
//clouser()
//
//x = 30
//y = 40
//h = Human()
//h.name = "b"
//
//clouser()
//
//print ("end")
//
//

//MARK: Swiftbook

//Вы сами решаете, когда сделать вместо сильной (strong) ссылки слабую (weak) или бесхозную (unowned)

class Person {
    let name: String
    init(name: String) {
        self.name = name
        print("\(name) инициализируется")
    }
    deinit {
        print("\(name) деинициализируется")
    }
}

var reference1: Person?
var reference2: Person?
var reference3: Person?

reference1 = Person(name: "John Appleseed")
reference2 = reference1
reference3 = reference1

//John Appleseed инициализируется

reference1 = nil
reference2 = nil

//John Appleseed инициализируется

reference3 = nil

//John Appleseed инициализируется
//John Appleseed деинициализируется

class Person1 {
    let name: String
    init(name: String) { self.name = name }
    var apartment: Apartment?
    deinit { print("\(name) освобождается") }
}
 
class Apartment {
    let unit: String
    init(unit: String) { self.unit = unit }
    weak var tenant: Person1?
    deinit { print("Апартаменты \(unit) освобождаются") }
}

var john: Person1?
var unit4A: Apartment?

john = Person1(name: "John Appleseed")
unit4A = Apartment(unit: "4A")

john!.apartment = unit4A
unit4A!.tenant = john

//john = nil
unit4A = nil



class Customer {
    let name: String
    var card: CreditCard?
    init(name: String) {
        self.name = name
    }
    deinit { print("\(name) деинициализируется") }
}
 
class CreditCard {
    let number: UInt64
    unowned let customer: Customer
    init(number: UInt64, customer: Customer) {
        self.number = number
        self.customer = customer
    }
    deinit { print("Карта #\(number) деинициализируется") }
}

var john1: Customer?

john1 = Customer(name: "John Appleseed")
john1!.card = CreditCard(number: 1234567890123456, customer: john1!)

//john1 = nil
//John Appleseed инициализируется
//John Appleseed деинициализируется


class Department {
    var name: String
    var courses: [Course]
    init(name: String) {
        self.name = name
        self.courses = []
    }
}

class Course {
    var name: String
    unowned var department: Department
    unowned var nextCourse: Course?
    init(name: String, in department: Department) {
        self.name = name
        self.department = department
        self.nextCourse = nil
    }
}

let department = Department(name: "Horticulture")

let intro = Course(name: "Survey of Plants", in: department)
let intermediate = Course(name: "Growing Common Herbs", in: department)
let advanced = Course(name: "Caring for Tropical Plants", in: department)

intro.nextCourse = intermediate
intermediate.nextCourse = advanced
department.courses = [intro, intermediate, advanced]


class Country {
    let name: String
    var capitalCity: City!
    init(name: String, capitalName: String) {
        self.name = name
        self.capitalCity = City(name: capitalName, country: self)
    }
}
 
class City {
    let name: String
    unowned let country: Country
    init(name: String, country: Country) {
        self.name = name
        self.country = country
    }
}


class HTMLElement {
 
    let name: String
    let text: String?
    
 //   let heading = HTMLElement(name: "h1")
    let defaultText = "some default text"

 
    lazy var asHTML: () -> String = {
        if let text = self.text {
            return "<\(self.name)>\(text)</\(self.name)>"
        } else {
            return "<\(self.name) />"
        }
    }
 
    init(name: String, text: String? = nil) {
        self.name = name
        self.text = text
    }
 
    deinit {
        print("\(name) деинициализируется")
    }
}

let heading = HTMLElement(name: "h1")
let defaultText = "some default text"
heading.asHTML = {
   return "<\(heading.name)>\(heading.text ?? defaultText)</\(heading.name)>"
}
print(heading.asHTML())

var paragraph: HTMLElement? = HTMLElement(name: "p", text: "hello, world")
print(paragraph!.asHTML())
// Выведет "<p>hello, world</p>"

//class HTMLElement {
//    
//    let name: String
//    let text: String?
//    
//    lazy var asHTML: () -> String = {
//        [unowned self] in
//        if let text = self.text {
//            return "<\(self.name)>\(text)</\(self.name)>"
//        } else {
//            return "<\(self.name) />"
//        }
//    }
//    
//    init(name: String, text: String? = nil) {
//        self.name = name
//        self.text = text
//    }
//    
//    deinit {
//        print("\(name) освобождается")
//    }
//}


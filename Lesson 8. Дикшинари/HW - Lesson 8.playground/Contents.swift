//Block 1 _________________________________________________________________________________
print ("История группы хХх: \n\nВ начале в группе обучались следующие студенты с результатами последней контрольной работы:")

var studentDict = ["Анна Антипина" : 0 , "Евгений Носов" : 3 , "Валерия Михалкова" : 2 , "Екатерина Васильева" : 5 , "Владислав Гоцман" : 2]
//print (studentDict)
for key in studentDict.keys {
    print ("\(key) оценка - \(studentDict[key]!)")
}

//increasing ratings
studentDict.updateValue(4, forKey: "Валерия Михалкова")
studentDict.updateValue(3, forKey: "Анна Антипина")
print ("\nРезультаты группы с исправленными контрольными работами нескольких студентов: \n\(studentDict)")

//add new student and their score
studentDict["Глеб Подгорский"] = 4
studentDict["Алексей Зыкунов"] = 3
print ("\nНа момент написания контрольно к группе присоединилось двое новых учеников и ее состав стал следующим: \n\(studentDict)")

//exclusion of students
studentDict["Екатерина Васильева"] = nil
studentDict.removeValue(forKey: "Владислав Гоцман")
print ("\nИтоговый изменившийся состав учеников группы хХх по результатами обучения: \n\(studentDict)")

//score statistic
var sumAll = 0
var middleScore = 0

for (_ , value) in studentDict {
    sumAll += value
    middleScore = sumAll / studentDict.count
}
print ("\nПоказатели обучения группы хХх с измененным составом:")
print ("Сумма баллов группы : \(sumAll)")
print ("Средний балл студентов : \(middleScore)")




//Block 2 ______________________________________________________________________________________

let yearDict = ["january" : 31, "february" : 28, "march" : 31, "april" : 30, "may" : 31, "june" : 30, "july" : 31, "august" : 31, "september" : 30, "october" : 31, "november" : 30, "december" : 31]

//print tuples
print ("\nПечать с использованием Тюпла:")
for (key , values) in yearDict {
    print ("В месяце \(key) дней \(values)")
}

//print for keys
print ("\nПечать по ключам:")
for key in yearDict.keys {
    print ("В месяце \(key) дней \(yearDict[key]!)")
}

////Block 3 _____________________________________________________________________________________
var chess = [String : Bool]()

let vertical = [1 , 2 , 3 , 4 , 5 , 6 , 7 , 8]
let gorizontal = ["a" , "b" , "c" , "d" , "e" , "f" , "g" , "h"]

for (index , value) in gorizontal.enumerated() {     //запуск 1 - цикле по горизонтали
    for valueGorizontal in 1..<gorizontal.count {    //
        let square = "\(value)\(valueGorizontal)"
        if index % 2 == valueGorizontal % 2 {
            chess[square] = true
        } else {
            chess[square] = false
        }
    }
}
//print (chess)
print ("\nЗначения цветов шахматных клеток:")
for (key , value) in chess {
    let colorSquare = value ? "White" : "Black"
    print ("square \(key) - \(colorSquare)")
}












//Мои наработки по сборке словарь/массивы/тюплы
////Block 1 _________________________________________________________________________________
////request
//var studentOne : (name : String? , carNumber : String? , score : Int?)
//var studentTwo: (name : String? , carNumber : String? , score : Int?)
//var studentThree : (name : String? , carNumber : String? , score : Int?)
//var studentFour : (name : String? , carNumber : String? , score : Int?)
//var studentFive : (name : String? , carNumber : String? , score : Int?)
//
////incoming data
////*name
//studentOne.name = "Анна"
//studentTwo.name = "Евгений"
//studentThree.name = "Валерия"
//studentFour.name = "Екатерина"
//studentFive.name = "Владислав"
//
////*car
////studentOne.carNumber = "Е123КХ197"
//studentTwo.carNumber = "Х756ОР777"
////studentThree.carNumber = "Р321ОК77"
//studentFour.carNumber = "Х666ЕР666"
////studentFive.carNumber = "Х333ОТ50"
//
////*score
//studentOne.score = 0
//studentTwo.score = 3
//studentThree.score = 2
//studentFour.score = 5
//studentFive.score = 2
//
//var studentName = [String?]()
//var studentScore = [Int?]()
//var studentDict = [String : Int]()
//var index = 0
//
//studentName = [studentOne.name , studentTwo.name , studentThree.name , studentFour.name , studentFive.name]
//studentScore = [studentOne.score , studentTwo.score , studentThree.score , studentFour.score , studentFive.score]
//
//for i in 0..<studentName.count {
//        if studentName.indices.contains(i) && studentScore.indices.contains(i) {
//            if studentName[i] == nil || studentScore[i] == nil {
//                print ("Student №\(index) - no data")
//            } else {
//                studentDict[studentName[i]!] = studentScore[i]
//                index += 1
//        }
//    }
//}
//print (studentDict)
//
////completing homework according to the script
//
////increasing ratings
//studentDict.updateValue(4, forKey: "Валерия")
//studentDict.updateValue(3, forKey: "Анна")
//
////add new student and their score
//studentDict["Глеб"] = 4
//studentDict["Алексей"] = 3
//
////exclusion of students
//studentDict["Екатерина"] = nil
//studentDict.removeValue(forKey: "Владислав")
//
////
//print (studentDict)
//var sumAll = 0
//var middleScore = 0
//
//for (key , value) in studentDict {
//    sumAll += value
//    middleScore = sumAll / studentDict.count
//}
//print ("\nСумма баллов группы : \(sumAll)")
//print ("Средний балл студентов : \(middleScore))")


//Block 2 ______________________________________________________________________________________
//
//var yearName = [String?]()
//    yearName = ["january" , "february" , "march" , "april" , "may" , "june" , "july" , "august" , "september" , "october" , "november" , "december"]
//var yearData = [Int?]()
//    yearData = [31 , 28 , 31 , 30 , 31 , 30 , 31 , 31 , 30 , 31 , 30 , 31]
//
//var fullYear = [String : Int]()
//
//for i in 0..<yearName.count {
//        if yearName.indices.contains(i) && yearData.indices.contains(i) {
//            if yearName[i] == nil || yearData[i] == nil {
//                print ("Месяц по индексу \(index) задан не полностью")
//            } else {
//                fullYear[yearName[i]!] = yearData[i]
//                index += 1
//        }
//    }
//}
//
////print tuples
//print ("\nПечать с использованием Тюпла:")
//for (Key , Values) in fullYear {
//    print ("В месяце \(Key) дней \(Values)")
//}
//
////print for keys
//print ("\nПечать по ключам:")
//for key in fullYear.keys {
//    print ("В месяце \(key) дней \(fullYear[key]!)")
//}

var dict = ["машина" : "car" , "мужик" : "man"]
dict

var dict2 = [Int : String]()
dict2 = [ 0 : "car" , 1 : "man"]
dict2

dict["мужик"]
dict.isEmpty
dict.count

dict2[1] = "old man"
dict2

var dict3 = [String : String]()
dict3.count
dict3.isEmpty

dict["комп"] = "computer" // добавление нового ключа - значения
dict["мужик"] = nil       // удаление существующего ключа - значения
dict.removeValue(forKey: "мужик") //удаление значения и test на предудщие изменения - возвращает nil при запросе удалялся ли ключ-значение когда-либо

print (dict)
print (dict["комп"]) //ошибка намекает  на опциональный тип и рекомендует ! для чистой печати

dict.keys.description
dict.values.description

//dict["комп"] = "mac"

dict.updateValue("mac", forKey: "комп") //введение изменения test на предудщие изменения - возвращает nil при запросе менялся ли ключ-значение когда-либо

let comp = dict["комп"]
print (comp)  //для печати значения словаря нужен ! - так как возвращаемое значение опционального типа

//let comp2 : String? = dict ["комп"] // применения значения из словаря будет иметь опциональный тип, по причине возможности наличия nil

//test на nil для словаря
var myComp = String()
if myComp != dict["комп"] {
    myComp = dict["комп"]! + ""
}

print (myComp)


//dict = [:]                //аналогичные
//dict.removeAll()          //действия
dict.isEmpty

for key in dict.keys {
    print ("key = \(key) , value = \(dict[key]!)")
}
for (key , value) in dict {
    print ("key = \(key) , value = \(value)")  // при использовании тюпла выводимое значение анрапится само
}

import UIKit
import Foundation

extension UInt8 {

    func binary () -> String {
        var result = ""
        for i in 0..<8 {
            let mask = 1 << i
            let set = Int(self) & mask != 0
            result = (set ? "1" : "0") + result
        }
        return result
    }
}


var a : UInt8 = 0b00110011
var b : UInt8 = 0b11100001

// | - побитовое сложение 1 и 1 = 1, 1 и 0 = 1, 0 и 0 = 0

a.binary()
b.binary()
(a | b).binary()

// & - побитовое умножение 1 и 1 = 1, 1 и 0 = 0, 0 и 0 = 0
a.binary()
b.binary()
(a & b).binary()

//^ - сумма по мудулю (исключающее или) почти тоже самое что сумма, за исключением 1 и 1 = 0, 1 и 0 = 1, 0 и 0 = 0
a.binary()
b.binary()
(a ^ b).binary()

//~ - инверсия (разворот числа наоборот)
a.binary()
(~a).binary()

//начало использование для булевых значений
a.binary()
b = 0b00001000
b.binary()
// отсчет начинается с конца с индекса 0
//наложение маски путем умножения на маску b (если результат 0 - не установлен, если результат > 0 -установлен)
(a & b).binary() // 00000000

var c : UInt8 = 0b00110011
var d : UInt8 = 0b00010000
(c & d)
(c & d).binary()

var e : UInt8 = 0b00010001 // сравнить по двум битам невозможно путем умножения
(c & e)//= e  - значит установлены оба бита
       // > 0 - установлены какие-то, но не оба
       // < 0 - никакие не установлены


// введение бита
b = 0b00000100 // - маска введения 2го бита
a.binary()
b.binary()
(a | b).binary()

// введение бита и проверка был ли он установлен
a.binary()
b.binary()
(a ^ b).binary() // если бит был установлен (1) он сбросится, если не было (0) - он установится

// сброс бита
b = 0b00010000
(~b).binary()  // для примера как отображается инверсия
a.binary()
(a & ~b).binary()

var f : UInt8 = 1
f.binary()


enum CheckList : UInt8 {
    case Bread =   0b00000001
    case Chicken = 0b00000010
    case Apples =  0b00000100
    case Pears =   0b00001000
}

let checkList : UInt8 = 0b00001001
let bread = checkList & CheckList.Bread.rawValue  // если больше нуля - хлеб куплен
bread.binary()

let chicken = checkList & CheckList.Chicken.rawValue  //если 0 - бит чикена не установлен
chicken.binary()

let pears = checkList & CheckList.Pears.rawValue // если больше нуля - груши куплены
pears.binary()









//var binaryString : String {   фрагмент прошлого занятия
//    var result = ""
//    for i in 0..<8 {
//        result = String(self & (1 << i) > 0) + result  //не может принимать Bool - > нужно расширение
//    }
//    return result
//}

/*
 ob11111111
 oxff
 255
 */

//extension UInt8 {
//
//    func binary () -> String {
//        var result = ""
//        for i in 0..<8 {
//            let mask = 1 << i
//            let set = Int(self) & mask != 0
//            result = (set ? "1" : "0") + result
//        }
//        return result
//    }
//}
//
//var a : UInt8 = 57
//a.binary()
//a = 0b00111001
//a.binary()
//
//(5 as UInt8).binary()
//
//a.binary()
//a = a + 0b00000101
//a.binary()
//
//(4 as UInt8).binary()
//a = a - 0b00000100
//a.binary()
//
//a = a << 3 //оператор сдига влево
//a.binary()
//
////a = a * 8   //выход за прделы бита
////a.binary()  //выход за прделы бита
//
//a = a &* 4   //применение маски как подверждения осознанности действий
//a.binary()
//
//a = 0b11111111
//a = a &+ 1
//
//a = 0b00000000
//a = a &- 1


//extension Int8 {
//
//    func binary () -> String {
//        var result = ""
//        for i in 0..<8 {
//            let mask = 1 << i
//            let set = Int(self) & mask != 0
//            result = (set ? "1" : "0") + result
//        }
//        return result
//    }
//}
//
//var a : Int8 = 57
//a.binary()
//a = 0b00111001
//a.binary()
//
//(5 as Int8).binary()
//
//a.binary()
//a = a + 0b00000101
//a.binary()
//
//(4 as Int8).binary()
//a = a - 0b00000100
//a.binary()
//
//a = a << 3 //оператор сдига влево
//a.binary()
//
////a = a * 8   //выход за прделы бита
////a.binary()  //выход за прделы бита
//
//a = 0b01111111 //максимальное число
//a.binary()
//a = a &+ 1
//a.binary()
//a = a &- 1
//a.binary()
//
//
//
//a = 0
//a = a - 1
//a.binary()
//a = a - 1
//a.binary()
//
////a = a &* 4   //применение маски как подверждения осознанности действий
////a.binary()
////
////a = 0b11111111
////a = a &+ 1
////
////a = 0b00000000
////a = a &- 1
//
//a = 0b00100001
//a = a << 1
//a = a << 1



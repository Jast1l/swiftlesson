import UIKit
import Foundation

//1. Расширьте енум из урока и добавьте в него метод, помогающий установить соответствующий бит в переданную маску и метод, помогающий его прочитать. Эти методы должны принимать и возвращать маску, либо принимать адрес маски и менять его

//2. Создать цикл, который будет выводить 1 байтное число с одним установленным битом в такой последовательности, чтобы в консоли получилась вертикальная синусоида

//3. Создайте 64х битное число, которое представляет клетки на шахматной доске. Установите биты так, что 0 - это белое поле, а 1 - черное. Младший бит это клетка а1 и каждый следующий байт начинается с клетки а (а2, а3, а4) и заканчивается клеткой h(h2, h3, h4). Выбирая клетки но индексу столбца и строки определите цвет клетки опираясь исключительно на значение соответствующего бита


//MARK: -1. Расширьте енум из урока и добавьте в него метод, помогающий установить соответствующий бит в переданную маску и метод, помогающий его прочитать. Эти методы должны принимать и возвращать маску, либо принимать адрес маски и менять его
enum CheckList : UInt8 {
    case Bread =   0b00000001
    case Chicken = 0b00000010
    case Apples =  0b00000100
    case Pears =   0b00001000
}


extension CheckList {
    
    static func purchase (select: CheckList, checkList : inout UInt8) {
        checkList = checkList | select.rawValue
    }

    static func list (checkList: UInt8) -> String {
        var resultString = "В корзине:"
        var mask : UInt8 = 1   //0b00000001  - смещая маска определяет какой из битов соотвествует покупке
        
        for _ in 0..<8 {
            if (checkList & mask) != 0 {
                switch mask {
                case _ where mask & CheckList.Bread.rawValue != 0 :
                    resultString += " хлеб"
                case _ where mask & CheckList.Chicken.rawValue != 0 :
                    resultString += " курица"
                case _ where mask & CheckList.Apples.rawValue != 0 :
                    resultString += " яблоки"
                case _ where mask & CheckList.Pears.rawValue != 0 :
                    resultString += " груши"
                default :
                    ""
                }
            }
            mask = mask << 1   //0b00000001  - смещая маска определяет какой из битов соотвествует покупке
        }
        return resultString
    }
}

var checkList : UInt8 = 0b00000000
checkList

CheckList.purchase(select: .Apples, checkList: &checkList)
CheckList.purchase(select: .Pears, checkList: &checkList)
CheckList.list(checkList: checkList)




//MARK: -2. Создать цикл, который будет выводить 1 байтное число с одним установленным битом в такой последовательности, чтобы в консоли получилась вертикальная синусоида

extension UInt8 {

    func binary () -> String {
        var result = ""
        for i in 0..<8 {
            let mask = 1 << i
            let set = Int(self) & mask != 0
            result = (set ? "1" : "0") + result
        }
        return result
    }
}

var mask : UInt8 = 1

func sinusoid (line : Int) {
    
    
    var position : UInt8 = 1
    var mask = 1
    
    for _ in 1...line {
        print (position.binary())
        position = position << mask
        
        switch position {
        case 128:
            mask = -1
        case 1:
            mask = 1
        default :
            break
        }
    }
}

sinusoid(line: 20)


//MARK: -3. Создайте 64х битное число, которое представляет клетки на шахматной доске. Установите биты так, что 0 - это белое поле, а 1 - черное. Младший бит это клетка а1 и каждый следующий байт начинается с клетки а (а2, а3, а4) и заканчивается клеткой h(h2, h3, h4). Выбирая клетки но индексу столбца и строки определите цвет клетки опираясь исключительно на значение соответствующего бита

extension UInt64 {

    func binary () -> String {
        var result = ""
        for i in 0..<64 {
            let mask = 1 << i
            let set = Int(self) & mask != 0
            result = (set ? "1" : "0") + result
        }
        return result
    }
    
    subscript (letter: String, column: Int) -> String {
        
        var line : Int?
        
                switch letter.uppercased() {
                case "A" :
                    line = 1
                case "B" :
                    line = 2
                case "C" :
                    line = 3
                case "D" :
                    line = 4
                case "E" :
                    line = 5
                case "F" :
                    line = 6
                case "G" :
                    line = 7
                case "H" :
                    line = 8
                default :
                    line = nil
                }
        
        if line != nil {
            if column >= 1 && column <= 8 {
                let mask = UInt64(1) << UInt64(8 * line! + column)
                if self & mask != 0 {
                    return "Black"
                } else {
                    return "White"
                }
            } else {
                return "Ошибка ввода цифры"
            }
        } else {
            return "Ошибка ввода буквы"
        }
    }
}
    
var chessBoard : UInt64 = 0b10101010_01010101_10101010_01010101_10101010_01010101_10101010_01010101 //0b10101010_01010101_10101010_01010101_10101010_01010101_10101010_01010101
chessBoard["a",1]
chessBoard["a",3]
chessBoard["a",4]
chessBoard["b",1]
chessBoard["b",3]
chessBoard["b",4]
chessBoard["k",5]
chessBoard["d",9]




















import UIKit
import Foundation

// 1. Создайте расширение для Int с пропертисами isNegative, isPositive, bool

// 2. Добавьте проперти, которое возвращает количество символов в числе

// 3. Добавьте сабскрипт, который возвращает символ числа по индексу:
// let a = 8245
// a[1] // 4
// a[3] // 8
// Профи могут определить и сеттер :)

// 4. Расширить String, чтобы принимал сабскрипт вида s[0..<3] и мог также устанавливать значения используя его

// 5. Добавить стрингу метод truncate, чтобы отрезал лишние символы и , если таковые были, заменял их на троеточие:
// let s = "Hi hi hi"

// s.truncate(4) // Hi h...
// s.truncate(10) // Hi hi hi


// 1. Создайте расширение для Int с пропертисами isNegative, isPositive, bool
extension Int {
    var isPositive : Bool {
        return self >= 0
    }
    
    var isNegative : Bool {
        return !isPositive
    }
    
    var bool : Bool {
        return self != 0
    }
}

// 2. Добавьте проперти, которое возвращает количество символов в числе
extension Int {
    var count : Int {
//        let textNumber = String(self)
//        let count = textNumber.count
        return (String(self)).count
    }
}
    
// 3. Добавьте сабскрипт, который возвращает символ числа по индексу:
// let a = 8245
// a[1] // 4
// a[3] // 8
// Профи могут определить и сеттер :)
extension Int {
    subscript (_ index : Int) -> Int {
        get {
            let textNumber = String(self)
            var arrayInt = [Int?]()
            for i in textNumber {
                arrayInt.append(Int("\(i)")!)
            }
            arrayInt.reverse()   //при активации разворачивает массив
            return arrayInt[index]!
        }
        set {
            let textNumber = String(self)
            var arrayInt = [Int?]()
            var textString = ""
            
            for i in textNumber {
                arrayInt.append(Int("\(i)")!)
            }
            arrayInt.reverse()   //при активации разворачивает массив
            arrayInt[index] = newValue
            
            for i in arrayInt {
                textString += String(i!)
            }
            
            self = Int(textString)!
        }
    }
}

var a = 5
var b = -5
var c = 1423

a.isPositive
b.isPositive

a.count
c.count

c[0]
c[1]
c[2]
c[3] = 3
c[3]

// 4. Расширить String, чтобы принимал сабскрипт вида s[0..<3] и мог также устанавливать значения используя его
extension String {
    subscript (_ range: Range<Int>) -> Substring {
        get {
            
//            var arraySelf = [String]()
//            var viewString = ""
//
//            for letter in self {
//                arraySelf.append(String(letter))
//            }
//
//            for i in range {
//                viewString += arraySelf[i]
//            }
//            return viewString
            
            let start = self.index(self.startIndex, offsetBy: range.startIndex)
            let end = self.index(self.startIndex, offsetBy: range.endIndex)
            let rangeIndex = start..<end
            
            return self[rangeIndex]
        }
        set {
//            var arraySelf = [String]()
//            var viewString = ""
//
//            var start = range.startIndex
//            var end = range.endIndex
//
//            for letter in self {
//                arraySelf.append(String(letter))
//            }
//
//            for i in range {
//                viewString += arraySelf[i]
//            }
            let start = self.index(self.startIndex, offsetBy: range.startIndex)
            let end = self.index(self.startIndex, offsetBy: range.endIndex)
            let rangeIndex = start..<end
            
            self.replaceSubrange(rangeIndex, with: newValue)
        }
    }
}

var testString = "Developer"
testString[0..<3] = "Хуйтебесука"
testString


// 5. Добавить стрингу метод truncate, чтобы отрезал лишние символы и , если таковые были, заменял их на троеточие:
// let s = "Hi hi hi"

// s.truncate(4) // Hi h...
// s.truncate(10) // Hi hi hi

extension String {
    func trancate (_ quantity: Int) -> String {
        if quantity < self.count {
            return self[0..<quantity] + "..."
        } else {
            return self
        }
    }
}
    
testString.trancate(7)









//extension String {
//    init (_ value: Bool) {
//        self.init(value ? 1 : 0)
//    }
//    subscript(start: Int, lenght: Int) -> String {
//        let start = self.index(self.startIndex, offsetBy: start)
//        let end = self.index(start, offsetBy: lenght)  //advance - более не работает
//        // let range = Range(start: start, end: end)    - более не работает
//        return "\(self[start..<end])"
//    }
//}

//MARK: пример
//FIXME: пример
//TODO: пример

//MARK: -Block 1
//print ("________Block 1_________________________________________________________________\n")

enum ChessSelect {
    
    enum Name : String {
        case King = "Король"
        case Queen = "Королева"
        case Bishop = "Слон"
        case Knight = "Конь"
        case Rook = "Ладья"
        case Pawn = "Пешка"
    }
    
    enum Picture : String {
        case WhiteKing = "\u{2654}"
        case WhiteQueen = "\u{2655}"
        case WhiteBishop = "\u{2657}"
        case WhiteKnight = "\u{2658}"
        case WhiteRook = "\u{2656}"
        case WhitePawn = "\u{2659}"
        
        case BlackKing = "\u{265A}"
        case BlackQueen = "\u{265B}"
        case BlackBishop = "\u{265D}"
        case BlackKnight = "\u{265E}"
        case BlackRook = "\u{265C}"
        case BlackPawn = "\u{265F}"
    }
    
    enum ColorSide : String {
        case Black = "Черный(ая)"
        case White = "Белый(ая)"
    }
        
    enum Letters : String {
        case A = "A"
        case B = "B"
        case C = "C"
        case D = "D"
        case E = "E"
        case F = "F"
        case G = "G"
        case H = "H"
        static var allCases : [Letters] {
            return [A, B, C, D, E, F, G, H]
        }
    }
    
    enum Numbers : Int {
        case One = 1
        case Two = 2
        case Three = 3
        case Four = 4
        case Five = 5
        case Six = 6
        case Seven = 7
        case Eight = 8
    }
}

enum ChessLetToInt : Int {
    case A = 1
    case B = 2
    case C = 3
    case D = 4
    case E = 5
    case F = 6
    case G = 7
    case H = 8
}

typealias Chess = (name: ChessSelect.Name, color: ChessSelect.ColorSide, picture: ChessSelect.Picture, letters: ChessSelect.Letters, numbers: ChessSelect.Numbers)



var whiteRookFirst = Chess(.Rook, .White, .WhiteRook, .A, .One)
var whiteKnightFirst = Chess(.Knight, .White, .WhiteKnight, .B, .One)
var whiteBishopFirst = Chess(.Bishop, .White, .WhiteBishop, .C, .One)
var whiteQueen = Chess(.Queen, .White, .WhiteQueen, .D, .One)
var whiteKing = Chess(.King, .White, .WhiteKing, .E,.One)
var whiteBichopSecond = Chess(.Bishop, .White, .WhiteBishop, .F, .One)
var whiteKnightSecond = Chess(.Knight, .White, .WhiteKnight, .G, .One)
var whiteRookSecond = Chess(.Rook, .White, .WhiteRook, .H, .One)

var whitePawnOne = Chess(.Pawn, .White, .WhitePawn, .A, .Two)
var whitePawnTwo = Chess(.Pawn, .White, .WhitePawn, .B, .Two)
var whitePawnThree = Chess(.Pawn, .White, .WhitePawn, .C, .Two)
var whitePawnFour = Chess(.Pawn, .White, .WhitePawn, .D, .Two)
var whitePawnFive = Chess(.Pawn, .White, .WhitePawn, .E, .Two)
var whitePawnSix = Chess(.Pawn, .White, .WhitePawn, .F, .Two)
var whitePawnSeven = Chess(.Pawn, .White, .WhitePawn, .G, .Two)
var whitePawnEight = Chess(.Pawn, .White, .WhitePawn, .H, .Two)


var blackRookFirst = Chess(.Rook, .Black, .BlackRook, .A, .Eight)
var blackKnightFirst = Chess(.Knight, .Black, .BlackKnight, .B, .Eight)
var blackBishopFirst = Chess(.Bishop, .Black, .BlackBishop, .C, .Eight)
var blackQueen = Chess(.Queen, .Black, .BlackQueen, .D, .Eight)
var blackKing = Chess(.King, .Black, .BlackKing, .E, .Eight)
var blackBichopSecond = Chess(.Bishop, .Black, .BlackBishop, .F, .Eight)
var blackKnightSecond = Chess(.Knight, .Black, .BlackKnight, .G, .Eight)
var blackRookSecond = Chess(.Rook, .Black, .BlackRook, .H, .Eight)

var blackPawnOne = Chess(.Pawn, .Black, .BlackPawn, .H, .Seven)
var blackPawnTwo = Chess(.Pawn, .Black, .BlackPawn, .G, .Seven)
var blackPawnThree = Chess(.Pawn, .Black, .BlackPawn, .F, .Seven)
var blackPawnFour = Chess(.Pawn, .Black, .BlackPawn, .E, .Seven)
var blackPawnFive = Chess(.Pawn, .Black, .BlackPawn, .D, .Seven)
var blackPawnSix = Chess(.Pawn, .Black, .BlackPawn, .C, .Seven)
var blackPawnSeven = Chess(.Pawn, .Black, .BlackPawn, .B, .Seven)
var blackPawnEight = Chess(.Pawn, .Black, .BlackPawn, .A, .Seven)


var arrayPiece = [whiteRookFirst, whiteKnightFirst, whiteBishopFirst, whiteQueen, whiteKing, whiteBichopSecond, whiteKnightSecond, whiteRookSecond, whitePawnOne, whitePawnTwo, whitePawnThree, whitePawnFour, whitePawnFive, whitePawnSix, whitePawnSeven, whitePawnEight, blackRookFirst, blackKnightFirst, blackBishopFirst, blackQueen, blackKing, blackBichopSecond, blackKnightSecond, blackRookSecond, blackPawnOne, blackPawnTwo, blackPawnThree, blackPawnFour, blackPawnFive, blackPawnSix, blackPawnSeven, blackPawnEight]

enum choosingChessPiece {
    case WhiteRookFirst
    case WhiteKnightFirst
    case WhiteBishopFirst
    case WhiteQueen
    case WhiteKing
    case WhiteBichopSecond
    case WhiteKnightSecond
    case WhiteRookSecond
        case WhitePawnOne
        case WhitePawnTwo
        case WhitePawnThree
        case WhitePawnFour
        case WhitePawnFive
        case WhitePawnSix
        case WhitePawnSeven
        case WhitePawnEight
    case BlackRookFirst
    case BlackKnightFirst
    case BlackBishopFirst
    case BlackQueen
    case BlackKing
    case BlackBichopSecond
    case BlackKnightSecond
    case BlackRookSecond
        case BlackPawnOne
        case BlackPawnTwo
        case BlackPawnThree
        case BlackPawnFour
        case BlackPawnFive
        case BlackPawnSix
        case BlackPawnSeven
        case BlackPawnEight
}

////MARK: -Block 2
////print ("\n\n\n________Block 2_________________________________________________________________\n")

//func analys (unit: Chess) {
//    print ("\(unit.color.rawValue) \(unit.name.rawValue) находится на позиции \(unit.letters.rawValue)\(unit.numbers.rawValue)")
//}
//
//func printArray (arrayChess: [Chess]) {
//    for chess in arrayChess {
//        analys(unit: chess)
//    }
//}
//
//print (printArray(arrayChess: arrayPiece))


////MARK: -Block 3
////print ("\n\n\n________Block 3_________________________________________________________________\n")


func bordView (conditions: [Chess]) -> [String: String] {
    let letter = ["A", "B", "C", "D", "E", "F", "G", "H"]
    let number = [1, 2, 3, 4, 5, 6, 7, 8]
    var board = [String: String]()                                       // введение шахматной доски black & white
    
    for num in number {
        for (index, value) in letter.enumerated() {
            if let condIndex = conditions.firstIndex(where: { (figure) -> Bool in
                return figure.letters.rawValue == value && figure.numbers.rawValue == num
                })
            {
                let tempFigure = conditions[condIndex]
                board["\(value)\(num)"] = tempFigure.picture.rawValue
            } else {
                if num % 2 == (index + 1) % 2 {
                    board["\(value)\(num)"] = "\u{25a0}"
                    } else {
                    board["\(value)\(num)"] = "\u{25a1}"
                }
            }
        }
    }
    var finalLine = ""
    for num in number {
        var line = ""
        
        for value in letter {
            line += "\(board["\(value)\(num)"]!) "
        }
        finalLine += "\(line)\n"
    }
    return board
}


func print_Block3 (chessData array: [String: String]) {
    var finalLine = "  A B C D E F G H\n"
    let letter = ["A", "B", "C", "D", "E", "F", "G", "H"]
    let number = [1, 2, 3, 4, 5, 6, 7, 8]
    for num in number {
        var line = ""
        
        for value in letter {
            line += "\(array["\(value)\(num)"]!) "
        }
        finalLine += "\(num) \(line)\(num)\n"
    }
    finalLine += "  A B C D E F G H"
    print (finalLine)
}

let displayedBoardValue = bordView(conditions: arrayPiece)

//print_ChesspositionOnBlock3(chessData: displayedBoardValue)





////MARK: -Block 4
////print ("\n\n\n________Block4_________________________________________________________________\n")

// func принимающая
//    1) шахматную фигуру
//    2) тюпл - буква и цифра   (новая позиция на нахматной доске)
// проверка:
//    1) за пределами доски
//    2) по запрещенным ходам
//  передвинуть несколько фигур и распечатать доску

//   энумы тюплы строки

//typealias Chess = (name: ChessSelect.Name, color: ChessSelect.ColorSide, picture: ChessSelect.Picture, letters: ChessSelect.Letters, numbers: ChessSelect.Numbers)

var firstActionWhite = true
var firstActionBlack = true

func getIntFromLetter (stringData: ChessSelect.Letters) -> Int? {
    if let index = ChessSelect.Letters.allCases.firstIndex(of: stringData) {
        return index
    }
        return nil
}

func checkChessMove (nameChess: ChessSelect.Name, colorChess: ChessSelect.ColorSide, oldLet: ChessSelect.Letters, oldNum: ChessSelect.Numbers, newLet: ChessSelect.Letters, newNum: ChessSelect.Numbers) -> Bool {
    
    let tempChessTuple = (nameChess, colorChess)
    
    switch tempChessTuple {
    case let (name, color) where name == .Pawn && color == .White :
        if firstActionWhite {
            if oldNum == .Two {
                let tempMove = newNum.rawValue - oldNum.rawValue
                if tempMove <= 2 && oldLet == newLet {
                    return true
                } else {
                    return false
                }
            }
        } else {
            let tempMove = newNum.rawValue - oldNum.rawValue
            
            if tempMove <= 1 && oldLet == newLet {          // доработать в отдельную функцию
                return true
            } else {
                return false
            }
        }
    default:
        return false
    }
    return false
}



func moveOneChess (arrayChessPosition: inout [Chess], chooseFigure: (letter: String, number: Int), newPosition: (letter: String, number: Int)) -> [String: String]? {
   
    guard let newLetter = ChessSelect.Letters(rawValue: newPosition.letter) else {
        print ("ошибка буквы")
        return nil
    }
    guard let newNumber = ChessSelect.Numbers(rawValue: newPosition.number) else {
        print ("ошибка цифры")
        return nil
    }
    
    guard let oldLetter = ChessSelect.Letters(rawValue: chooseFigure.letter) else {
        print ("ошибка буквы")
        return nil
    }
    guard let oldNumber = ChessSelect.Numbers(rawValue: chooseFigure.number) else {
        print ("ошибка цифры")
        return nil
    }
    
        
    
    for (index, value) in arrayChessPosition.enumerated() {
        
        if value.letters == oldLetter && value.numbers == oldNumber {
            print ("\nХод \(oldLetter.rawValue)\(oldNumber.rawValue) на \(newLetter.rawValue)\(newNumber.rawValue)\n")
            if checkChessMove(nameChess: value.name, colorChess: value.color, oldLet: value.letters, oldNum: value.numbers, newLet: newLetter, newNum: newNumber) {
                
                if value.color == .Black {
                    firstActionBlack = false
                } else if value.color == .White {
                    firstActionWhite = false
                }
                
                var tempTuples = value
            
                tempTuples.letters = newLetter
                tempTuples.numbers = newNumber
                arrayChessPosition[index] = tempTuples
                
                
            } else {
                print ("иди на хуй\n")
            }
            
        }
       }
    return bordView(conditions: arrayChessPosition)
}

   
//var whiteKing = Chess(.King, .White, .WhiteKing, .E,.One)

if let test = moveOneChess(arrayChessPosition: &arrayPiece, chooseFigure: (letter: "A", number: 2), newPosition: (letter: "A", number: 4)) {
    print (print_Block3(chessData: test))
}

if let test = moveOneChess(arrayChessPosition: &arrayPiece, chooseFigure: (letter: "A", number: 4), newPosition: (letter: "A", number: 6)) {
    print (print_Block3(chessData: test))
}
    
if let test = moveOneChess(arrayChessPosition: &arrayPiece, chooseFigure: (letter: "B", number: 2), newPosition: (letter: "B", number: 3)) {
    print (print_Block3(chessData: test))
}
    


//MARK: пример
//FIXME: пример
//TODO: пример

enum Direction : String{
        case Left = "Left!"
        case Right = "Right!"
    }


enum Action {
    case Walk (meters: Int)
    case Run (meters: Int, speed: Double)
    case Stop
    case Turn (direction: Direction)  //вначале создается enum Direction

//    enum Direction {    //можно хранить внутри
//        case Left
//        case Right
//    }
}

var action = Action.Run(meters: 20, speed: 15)

action = .Stop
//action = .Walk(meters: 110)
action = .Run(meters: 200, speed: 15.0)
action = .Turn(direction: .Left)


//var direction = Direction(rawValue: "Right!")!   //присвоение типа энума по строчному значению
//action = .Turn(direction: direction)             //присвоение типа энума по строчному значению



switch action {
    case .Stop: print ("Stop")
    case .Walk(let meters) where meters < 100:
        print ("short walk")
    case .Walk(let meters):
        print ("long walk")
    case .Run(let meters, let speed):
        print ("run \(meters) meters whith \(speed) speed")
    case .Turn(let dir) where dir == .Left:
        print ("turn left")
    case .Turn(let dir) where dir == .Right:
        print ("turn right")
    
default:
    break
}

print (Direction.Left.rawValue)



//
//  main.swift
//  test
//
//  Created by MACBOOK on 31.12.2020.
//
//
//import Foundation
//
//print("Hello, World!")
//
//class Pacient {
//    var inHospital = true {
//        willSet {
//            print ("MARK: pacient is out from hospital")
//        }
//
//        didSet {
//            if inHospital == false {
//                inSport = true
//                needDrugs = 0
//                storyAboutHospital = "I leave from hospital, where i did heave a madical help"
//            }
//        }
//    }
//    var inSport = false
//    var needDrugs = 5
//    var storyAboutHospital = ""
//}
//
//var human = Pacient()
//
//human.inHospital = false
//
//let testString = "Hello, World!"
//
//testString.components(separatedBy: " ")

//class Person {
//  var dog: Dog?
//
//  deinit {
//    print("Person is Free")
//  }
//}
//
//class Dog {
//  unowned var person: Person
//
//  init() {
//    self.person = Person()
//  }
//
//  deinit {
//    print("Dog is Free")
//  }
//}
//
//let firstScope = true
//let secondScope = true
//
//
////------------------FIRST-------------------------------------
//if firstScope {
//
//  let person = Person()
//  let dog = Dog()
//
//  //----------------SECOND---------------------------
//  if secondScope {
//
//
//
//    person.dog = dog
//    dog.person = person
//
//
//
//    print("secondScope has ended")
//  }
//  //----------------SECOND ENDS-----------------------
//
//
//  print("firstScope has ended")
//}
////------------------FIRST ENDS---------------------------------
//
//print("finish")



enum PossibleErrors: Error {
  case notInStock
  case notEnoughMoney
}

struct Book {
  let price: Int
  var count: Int
}


class Library {

  var deposit = 11
  var libraryBooks = ["Book1": Book(price: 10, count: 1), "Book2": Book(price: 11, count: 0), "Book3": Book(price: 12, count: 3)]
  
  
  func getTheBook(withName: String) throws {
    guard var book = libraryBooks[withName] else {
      throw PossibleErrors.notInStock
    }
    
    guard book.count > 0 else {
      throw PossibleErrors.notInStock
    }
    
    guard book.price <= deposit else {
      throw PossibleErrors.notEnoughMoney
    }
    
    deposit -= book.price
    book.count -= 1
    libraryBooks[withName] = book
    print("You got the Book: \(withName)")
    
  }
}

let library = Library()
try? library.getTheBook(withName: "Book1")
//library.deposit
//library.libraryBooks

do {
  try library.getTheBook(withName: "Book1")
} catch PossibleErrors.notInStock {
  print("Book is not in stock")
} catch PossibleErrors.notEnoughMoney {
  print("Not enough money")
}

func doConnection() throws -> Int {
  return 10
}

let x = try? doConnection()

let y: Int?

do {
  y = try doConnection()
} catch {
  y = nil
}


var attempt = 0
func whateverFunc(param: Int) -> Int {
  defer {
    attempt += 2
  }
  
  defer {
    attempt *= 10
  }
  
  switch param {
  case 0:  return 100
  case 1:  return 200
  default: return 400
  }
}

let testOne = whateverFunc(param: 1)
let testTwo = attempt

print (testOne)
print (testTwo)

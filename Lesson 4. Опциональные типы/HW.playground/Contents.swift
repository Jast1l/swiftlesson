////Block - 1 ____________________________________________________________________
//
//// assignment of values
//var one = "1423"
//var two = "2314"
//var three = "2020"
//var four = "NW"
//var five = "NW1423"
//var six = "Wolf"
//
//var resultSum = 0
//
////test for nil
//// 1
//if let one = Int(one) {
//    resultSum += one
//}
//// 2
//if Int(two) != nil {
//    resultSum += Int(two)!
//}
//
////3
//resultSum += Int(three) != nil ? Int(three)! : 0
//
////4
//if let four = Int(four) {
//    resultSum += four
//}
//
////5
//if Int(five) != nil {
//    resultSum += Int(five)!
//}
//
////6
//resultSum += Int(six) != nil ? Int(six)! : 0
//
//print (resultSum)


////Block 2 ____________________________________________________________________
//
//let answer1 : ( statusCod : Int? , message : String? , erroMessage : String?)
//
//answer1.statusCod = 202
//answer1.message = "Good job"
//answer1.erroMessage = "Critical error"
//
//if let testServer1 = answer1.statusCod {
//    if testServer1 >= 200 && testServer1 < 300 {
//        if let testMessage = answer1.message {
//            print (testMessage)
//        }
//    } else if testServer1 > 300 {
//        if let testErrorMessage = answer1.erroMessage {
//            print (testErrorMessage)
//        }
//    }
//} else {
//    if let testMessage = answer1.message {
//        print (testMessage)
//    }
//    if let testErrorMessage = answer1.erroMessage {
//        print (testErrorMessage)
//    }
//}


//Block 3 ____________________________________________________________________
////request
//var studentOne : (name : String? , carNumber : String? , score : Int?)
//var studentTwo: (name : String? , carNumber : String? , score : Int?)
//var studentThree : (name : String? , carNumber : String? , score : Int?)
//var studentFour : (name : String? , carNumber : String? , score : Int?)
//var studentFive : (name : String? , carNumber : String? , score : Int?)
//
////incoming data
////*name
//studentOne.name = "Анна"
//studentTwo.name = "Евгений"
//studentThree.name = "Валерия"
//studentFour.name = "Екатерина"
//studentFive.name = "Владислав"
//
////*car
////studentOne.carNumber = "Е123КХ197"
//studentTwo.carNumber = "Х756ОР777"
////studentThree.carNumber = "Р321ОК77"
//studentFour.carNumber = "Х666ЕР666"
////studentFive.carNumber = "Х333ОТ50"
//
////*score
////studentOne.score = 0
//studentTwo.score = 3
//studentThree.score = 2
//studentFour.score = 5
////studentFive.score = 2
//
////print data
//let table = "Студент(ка): "
//let out = "Данных о студенте нет"
//let car = "\tАвтомобиль: "
//let noCar = "\tНет автомобиля"
//let score = "\tОценка за последнюю контрольную  работу - "
//let noScore = "\tПоследняя контрольная работа не выполнялась"
//
////conditions
////1
//if studentOne.name != nil {
//    print ("\(table) \(studentOne.name!)")
//    if studentOne.carNumber != nil {
//        print ("\(car) \(studentOne.carNumber!)")
//    } else {
//        print (noCar) }
//    if studentOne.score != nil {
//        print ("\(score) \(studentOne.score!)")
//        print ("\n")
//    } else {
//        print (noScore)
//        print ("\n")
//    }} else {
//    print (out)
//    print ("\n")
//}
//
////2
//if studentTwo.name != nil {
//    print ("\(table) \(studentTwo.name!)")
//    if studentTwo.carNumber != nil {
//        print ("\(car) \(studentTwo.carNumber!)")
//    } else {
//        print (noCar)
//    }
//    if studentTwo.score != nil {
//        print ("\(score) \(studentTwo.score!)")
//        print ("\n")
//    } else {
//        print (noScore)
//        print ("\n")
//    }
//} else {
//    print (out)
//    print ("\n")
//}
//
////3
//if let testName = studentThree.name {
//    print ("\(table) \(testName)")
//    if let testCar = studentThree.carNumber {
//        print ("\(car) \(testCar)")
//    } else {
//        print (noCar)
//    }
//    if let testScore = studentThree.score {
//        print ("\(score) \(testScore)")
//        print ("\n")
//    } else {
//        print (noScore)
//        print ("\n")
//    }
//} else {
//    print (out)
//    print ("\n")
//}
//
////4
//if let testName = studentFour.name {
//    print ("\(table) \(testName)")
//    if let testCar = studentFour.carNumber {
//        print ("\(car) \(testCar)")
//    } else {
//        print (noCar)
//    }
//    if let testScore = studentFour.score {
//        print ("\(score) \(testScore)")
//        print ("\n")
//    } else {
//        print (noScore)
//        print ("\n")
//    }
//} else {
//    print (out)
//    print ("\n")
//}
//
////5
//if studentFive.name != nil {
//    print ("\(table) \(studentFive.name!)")
//    if let testCar = studentFive.carNumber {
//        print ("\(car) \(testCar)")
//    } else {
//        print (noCar)
//    }
//    if studentFive.score != nil {
//        print ("\(score) \(studentFive.score!)")
//    } else {
//        print (noScore)
//    }
//} else {
//    print (out)
//}







//training ___________________________________________________________________________________

//resultString += fifth != nil ? fifth! + " " : ""
//

//var age = 19
//
//if age >= 18 && age < 30 {
//    print ("Ты еще молод")
//}




// 200 - ok
// 301 - redirect
// 401 & 403 - нехватает прав
// 404 - нет страницы / нет метода
// 500 - ошибка на сервере























//let noData = "Без значения"
//
//
//var first : String?
//var second : String?
//var third : String?
//var fourth : String?
//var fifth : String?
//
//first = "Всем"
////second = "привет"
//third = "кто"
//fourth = "причастен"
//fifth = "14:23"
//
//var resultString = ""
//
//resultString += first != nil ? first! + " " : ""
//
//resultString += second != nil ? second! + " " : ""
//
//resultString += third != nil ? third! + " " : ""
//
//resultString += fourth != nil ? fourth! + " " : ""
//
//resultString += fifth != nil ? fifth! + " " : ""
//
//print(resultString)



//var age: Int?
//age = 18

//if let age = age, age >= 18 {
//    print("Ты совершеннолетний")
//} else {
//    print("Ты ммаленький! Иди на хуй")
//}
//
//var ageTwo = 15
//
//if ageTwo >= 18 {
//    print("Ты совершеннолетний")
//} else {
//    print("Ты ммаленький! Иди на хуй")
//}
//
//let result = ageTwo >= 18 ? "Ты совершеннолетний" : "Ты ммаленький! Иди на хуй"
//print(result)






/*
var first = "welcom"
var second = "1423"
var third = "WFFW1423"
var fourth = "2314"
var fifth = "hello"

var sum = 0

if let first = Int(first) {           //байндинг опшион анрапт
    sum += first         //идентичные значения
    //sum = sum + first    //идентичные значения
}

if Int(second) != nil {
    sum += Int(second)!
}

if let third = Int(third) {
    sum += third
}

if var testFourth = Int(fourth) {
    sum += testFourth
    testFourth += 1
    testFourth = testFourth + 1
    //print (testFourth)
}

if Int(fifth) != nil {
    sum += Int(fifth)!
}
 
print ("Результат сложения переменных в Int составляет - \(sum)")
*/





/*
var age : Int?
age = 30
if let tempAge = age {   можно использовать одно название - переменная используется только в зоне видимости условия
    print (tempAge + 1)
} else {
    print ("nil")
}

if age != nil {
    age = age! + 1        ! - возвращает из опционального типа к работе и изменениям
}

print(age ?? "Не задано")    если age = nil выводим Не задано


print (age)

if Int(first) != nil {
    sum = sum + Int(firs)
    
}

if let convFirst = Int(first) {
    sum = sum + convFirst
}

first - one
if let one = first {      optional binding
    print (one)
} else {
    print (noData)
}
Int(one)


if second == nil {
    print (noData)
} else {
    
}


first + second + third + fourth + fifth)
let sum : Int? = (first + second + third + fourth + fifth)
*/

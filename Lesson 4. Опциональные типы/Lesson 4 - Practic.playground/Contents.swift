/*
// sell

// продажи в понедельник  50 + 20 = 70
// проджажи во вторник    10 - 10 = 0
// продажи в среду        0 - nil

//server request
// is it Saturday ?
// true / false
// true / false / nill - swift

//Петя в кармане 5 яблок
//Вася нет карманов - nil

//var apples = 5
//apples = nil - ошибка Int. если допускаем подение до Nil - Int?

//var apples : Int? = 5
//apples = nil
//apples + 1 - к ничему нельзя чтолибо приложить

/*
var apples : Int? = 5

apples = nil

if apples == nil {
    print ("nil apples")
} else {
    //print (apples)
    
    let a = apples! + 2
}
*/

/*
 if var number = apples {
    numder = number + 2
} else {
    print ("nil apples")
}
 */

let age = "60"


var apple2 : Int! = nil
apple2 = 2      //включение убирает ошибку

//assert(apple2 != nil, "Oh no")
apple2 = apple2 + 5

*/

// вторая попытка

//nil - для свифт отсутвие значения определенного типа, что не возможно в работе

//sell out
// 1) 50 + 20 = 70
// 2) 10 - 10 = 0
// 3) 0            - nil

//server request
// is it Saturday?
// true / false / nil - в случае отсутсвия ответа

// Петя в кармане 5 яблок
// Вася нет карманов - nil

//var apples : Int = 5
//apples = nil            Int - не может быть nil - приложение падает, если допускаем - ?

//var apples : Int? = 5
//apples = nil

/*
var apples : Int? = 5
apples = nil // - выведет по условию "nil apples" / при значении 3 - Oprional(3)
// apples + 1 - ошибка

if apples == nil {
    print("nil apples")
} else {
    print(apples)
}
*/

// ---------------- 1) способ проверки на nil (силовое разворачивание)
/*
 var apples : Int? = 5
//apples = nil

if apples == nil {
    print("nil apples")
} else {
    //print(apples)
}
//let a = apples! + 2 - силовое разворачивание !
*/

// ---------------- 2) способ проверки на nil (через переменную и не нужно силовое разворачивани)

 var apples : Int? = 5

//apples = nil

if apples == nil {
    print("1) nil apples") // 1-ый способ
} else {
    print(apples)
}
let a = apples! + 2 //Выражение выполнится, т.r.  apples приведена обратно к Int (!)

if var number = apples {
    number = number + 2
} else {
    print ("2) nil apples") //2-ой способ
}

// ---------------- 3)

let age = "60"
Int(age)

if Int(age) != nil {
    var ageNumber = Int(age)
}

if var ageNumber = Int(age) {
    ageNumber = ageNumber + 3
}

let text1 = "Добрый день, "


/*
var resultString = "Привет,\t"
var table : String? = "Джейсон"
resultString += table!
//table = nil
if table == nil {
    print ("Пустое поле / данные не поступили")
} else {
    print (resultString)
}
*/

var resultString = "Привет,\t"
var table : String?
table = "Jason"
//table = nil
if table == nil {
    print ("Пустое поле / данные не поступили")
} else {
        resultString += "\(table!)"
    print (resultString)
}



/*
var table = "Окси"
var resultString = "Привет, "
resultString += table

print(resultString)
*/
























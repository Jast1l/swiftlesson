import UIKit
import Foundation

//MARK: -1. Создайте тип шахматная доска.
//2. Добавьте сабскрипт, который выдает цвет клетки по координате клетки (буква и цифра).
//3. Если юзер ошибся координатами - выдавайте нил


//MARK: -2. Крестики нолики (Средний уровень)
//1. Создать тип, представляющий собой поле для игры в крестики нолики
//На каждой клетке может быть только одно из значений: Пусто, Крестик, Нолик
//Добавьте возможность красиво распечатывать поле
//2. Добавьте сабскрипт, который устанавливает значение клетки по ряду и столбцу,
//причем вы должны следить за тем, чтобы программа не падала если будет введен не существующий ряд или столбец.
//3. Также следите за тем, чтобы нельзя было устанавливать крестик либо нолик туда, где они уже что-то есть. Добавьте метод очистки поля.
//4. Если хотите, добавте алгоритм, который вычислит победителя


//MARK: -3. Морской бой (Тяжелый уровень)
//
//1. Создайте тип корабль, который будет представлять собой прямоугольник. В нем может быть внутренняя одномерная система координат (попахивает сабскриптом). Корабль должен принимать выстрелы по локальным координатам и вычислять когда он убит
//
//2. Создайте двумерное поле, на котором будут располагаться корабли врага. Стреляйте по полю и подбейте вражеский четырех трубник :)
//
//3. Сделайте для приличия пару выстрелов мимо, красивенько все выводите на экран :)



//MARK: -1. Создайте тип шахматная доска.
// - Добавьте сабскрипт, который выдает цвет клетки по координате клетки (буква и цифра).
// - Если юзер ошибся координатами - выдавайте нил


//struct Chess {
//
//    let chessLetters = ["A", "B", "C", "D", "E", "F", "G", "H"]
//    let chessNumbers = [1, 2, 3, 4, 5, 6, 7, 8]
//
//    static var chessCell = [String: String]()
//
//    func compilationBoard (letter: [String], number: [Int]){
//        for (index, value) in chessLetters.enumerated() {
//            for num in chessNumbers {
//                if num % 2 == (index + 1) % 2 {
//                    Chess.chessCell["\(value)\(num)"] = "Black"
//                } else {
//                    Chess.chessCell["\(value)\(num)"] = "White"
//                }
//            }
//        }
//    }
//
//    func keyForDict (letter: String?, numder: Int?) -> String {
//        var tempString = ""
//        if letter != nil && numder != nil {
//            let letterUp = letter!.uppercased()
//            tempString = "\(letterUp)\(numder!)"
//        }
//        return tempString
//    }
//
//
//    subscript (letter: String?, numder: Int?) -> String? {
//        if letter != nil && numder != nil {
//            let letterUp = letter!.uppercased()
//            if chessLetters.contains(letterUp) && chessNumbers.contains(numder!) {
//                compilationBoard(letter: chessLetters, number: chessNumbers)
//                return Chess.chessCell[keyForDict(letter: letter, numder: numder)]
//            } else {
//                print ("Вышли за пределы доски")
//                return nil
//            }
//        } else {
//            print ("Указаны не корректные значения - необходимо буквенное и цифровое значение ключа")
//            return nil
//        }
//    }
//}
//
//var chessBoard = Chess()
//
//chessBoard.chessLetters
//chessBoard.chessNumbers
//
//chessBoard["b",1]



//MARK: -2. Крестики нолики (Средний уровень)
//1. Создать тип, представляющий собой поле для игры в крестики нолики
//На каждой клетке может быть только одно из значений: Пусто, Крестик, Нолик
//Добавьте возможность красиво распечатывать поле
//2. Добавьте сабскрипт, который устанавливает значение клетки по ряду и столбцу,
//причем вы должны следить за тем, чтобы программа не падала если будет введен не существующий ряд или столбец.
//3. Также следите за тем, чтобы нельзя было устанавливать крестик либо нолик туда, где они уже что-то есть. Добавьте метод очистки поля.
//4. Если хотите, добавте алгоритм, который вычислит победителя


//enum Picture : String {
//    case Cross = "\u{274C}"
//    case Zero = "\u{2B55}"
//    case Empty = "\u{2B1C}"
//}
//
//struct Field {
//    var gorizontal = 3
//    var vertical = 3
//
//    static var playBoard = [String: String]()
//    static var numberMotion = 0
//    static var lastPlayer = ""
//
//    func cleanBoard () {
//        for num1 in 1...gorizontal {
//            for num2 in 1...vertical {
//                Field.playBoard["\(num1)\(num2)"] = Picture.Empty.rawValue
//            }
//        }
//    }
//
//    func rangeToArray (range: Int) -> [Int] {
//        var tempArray = [Int]()
//        for num in 1...range {
//            tempArray.append(num)
//        }
//        return tempArray
//    }
//
//    func testOutOfRange (gor: Int, vert: Int) -> Bool{
//        return 1...gorizontal ~= gor && 1...vertical ~= vert ? true : false
//    }
//
//    func checkСell (gor: Int, vert: Int, testArra: [String: String]) -> Bool {
//        return Field.playBoard["\(gor)\(vert)"] == Picture.Empty.rawValue ? true : false
//    }
//
//    func checkToWin (array: [String: String]) -> Bool {
//        if Field.playBoard["22"] != Picture.Empty.rawValue {
//            if Field.playBoard["22"] == Field.playBoard["11"] && Field.playBoard["22"] == Field.playBoard["33"] { // диагональ 1
//                return true
//            }
//            if Field.playBoard["22"] == Field.playBoard["31"] && Field.playBoard["22"] == Field.playBoard["13"] { // диагональ 2
//                return true
//            }
//            if Field.playBoard["22"] == Field.playBoard["12"] && Field.playBoard["22"] == Field.playBoard["32"] { // крест 1
//                return true
//            }
//            if Field.playBoard["22"] == Field.playBoard["21"] && Field.playBoard["22"] == Field.playBoard["23"] { // крест 2
//                return true
//            }
//        }
//        if Field.playBoard["12"] != Picture.Empty.rawValue && Field.playBoard["12"] == Field.playBoard["12"] && Field.playBoard["12"] == Field.playBoard["13"] {                                                                              // левая вертикаль
//            return true
//        }
//        if Field.playBoard["32"] != Picture.Empty.rawValue && Field.playBoard["32"] == Field.playBoard["31"] && Field.playBoard["32"] == Field.playBoard["33"] {                                                                              // правая вертикаль
//            return true
//        }
//        if Field.playBoard["21"] != Picture.Empty.rawValue && Field.playBoard["21"] == Field.playBoard["11"] && Field.playBoard["21"] == Field.playBoard["31"] {                                                                              // верхняя горизонталь
//            return true
//        }
//        if Field.playBoard["23"] != Picture.Empty.rawValue && Field.playBoard["23"] == Field.playBoard["13"] && Field.playBoard["23"] == Field.playBoard["33"] {                                                                              // нижняя горизонталь
//            return true
//        }
//
//        return false
//    }
//
//
//    subscript (gor: Int, vert: Int) -> String? {
//        get {
//            if testOutOfRange(gor: gor, vert: vert) {
//                return Field.playBoard["\(gor)\(vert)"]!
//            } else {
//                return nil
//            }
//        }
//        set {
//            Field.numberMotion += 1
//            print ("Ход № \(Field.numberMotion)")
//            if newValue != Field.lastPlayer {
//                if testOutOfRange(gor: gor, vert: vert) {
//                    if checkСell(gor: gor, vert: vert, testArra: Field.playBoard) {
//                        Field.playBoard["\(gor)\(vert)"] = newValue
//                        Field.lastPlayer = newValue!
//                        print ("\tВыбор игрока \(newValue!) клетки для ход x:\(gor) y:\(vert)")
//                    } else {
//                        print ("\tИгроку \(newValue!) необходимо выбрать другую клетку, т.к. клетка х:\(gor) y:\(vert) выбрана для значения \(Field.playBoard["\(gor)\(vert)"]!)")
//                    }
//                } else {
//                    print ("\tИгроком \(newValue!) указано значение вне поля, выберите значение X и Y в диаапазоне от 1 до 3")
//                }
//                if checkToWin(array: Field.playBoard) {
//                    print ("\tПобедил игрок - \(newValue!)")
//                }
//            } else {
//                print("\tИгрок \(newValue!) уже ходил - переход хода")
//            }
//        }
//    }
//}
//
//func arraysToString (arrayToPrint: [String: String]) -> String {
//
//    let arrayGorizontal = field.rangeToArray(range: field.gorizontal)
//    let arrayVertical = field.rangeToArray(range: field.vertical)
//
//    var finalLine = " 1  2  3"
//    var lineIndex = 0
//
//    for num in arrayVertical {
//        var line = ""
//        lineIndex += 1
//
//        for value in arrayGorizontal {
//            line += "\(arrayToPrint["\(value)\(num)"]!) "
//        }
//        finalLine += "\n\(line) \(lineIndex)"
//    }
//    return finalLine
//}
//
//let x = Picture.Cross.rawValue
//let o = Picture.Zero.rawValue
//
//var field = Field()
//
//field.cleanBoard()
//
//field[1,1] = x
//field[3,3] = o
//field[1,2] = x
//field[1,3] = o
//field[2,3] = x
//field[3,1] = o
//field[2,2] = x
//field[3,2] = o
//
//print ("\n\(arraysToString(arrayToPrint: Field.playBoard))")


//MARK: -3. Морской бой (Тяжелый уровень)
//
//1. Создайте тип корабль, который будет представлять собой прямоугольник. В нем может быть внутренняя одномерная система координат (попахивает сабскриптом). Корабль должен принимать выстрелы по локальным координатам и вычислять когда он убит
//
//2. Создайте двумерное поле, на котором будут располагаться корабли врага. Стреляйте по полю и подбейте вражеский четырех трубник :)
//
//3. Сделайте для приличия пару выстрелов мимо, красивенько все выводите на экран :)

enum FieldPicture : String{
    case ShipDecks = "\u{26F4}"
    case Miss = "\u{274C}"
    case Empty = "\u{1F7E6}"
    case Fire = "\u{1F525}"
}

enum Hide {
    case On
    case Off
}

enum Decks : Int {
    case One = 1
    case Two = 2
    case Three = 3
    case Four = 4
}

enum Orientation : String{
    case Left = "L"
    case Right = "R"
    case Up = "U"
    case Down = "D"
}

enum Letters : Int {
    case A = 1
    case B = 2
    case C = 3
    case D = 4
    case E = 5
    case F = 6
    case G = 7
    case H = 8
    case I = 9
    case J = 10
}

enum Numbers : Int {
    case One = 1
    case Two = 2
    case Three = 3
    case Four = 4
    case Five = 5
    case Six = 6
    case Seven = 7
    case Eight = 8
    case Nine = 9
    case Ten = 10
}

class Ship {
    var letter : Letters
    var number : Numbers
    var deck : Decks
    var orientation : Orientation

    static var shipArray = [String]()
    static var shipView = [String: String]()
    
    static func orientationShip (decks: Decks, direction: Orientation, lett: Letters, num: Numbers) {

        var index = 0
        if direction == .Left {
            if (lett.rawValue - decks.rawValue) >= 1  {
                for _ in 1...decks.rawValue {
                    if let _ = Ship.shipView["\(lett.rawValue - index)\(num.rawValue)"] {
                        print ("Вы патаетесь потопить свою флотилию, произошло столкновение кораблей, возможно экстренное лавирование поможет вам")
                        Ship.shipView["\(lett.rawValue)\(num.rawValue - index)"] = FieldPicture.Fire.rawValue
                        index += 1
                    } else {
                    Ship.shipView["\(lett.rawValue - index)\(num.rawValue)"] = FieldPicture.ShipDecks.rawValue
                    index += 1
                    }
                }
            } else {
                print ("При размещении карабля произошла ошибка, возможна неисправна навигация. Корабль развернут в противоположную сторону")
            }
        }
        if direction == .Right {
            if (lett.rawValue + decks.rawValue) <= 10 {
                for _ in 1...decks.rawValue {
                    if let _ = Ship.shipView["\(lett.rawValue + index)\(num.rawValue)"] {
                        print ("Вы патаетесь потопить свою флотилию, произошло столкновение кораблей, возможно экстренное лавирование поможет вам")
                        Ship.shipView["\(lett.rawValue + index)\(num.rawValue)"] = FieldPicture.Fire.rawValue
                        index += 1
                    } else {
                    Ship.shipView["\(lett.rawValue + index)\(num.rawValue)"] = FieldPicture.ShipDecks.rawValue
                    index += 1
                }
                }
            } else {
                print ("При размещении карабля произошла ошибка, возможна неисправна навигация. Корабль развернут в противоположную сторону")
            }
        }
        if direction == .Up {
            if (num.rawValue - decks.rawValue) >= 1  {
                for _ in 1...decks.rawValue {
                    if let _ = Ship.shipView["\(lett.rawValue)\(num.rawValue - index)"] {
                        print ("Вы патаетесь потопить свою флотилию, произошло столкновение кораблей, возможно экстренное лавирование поможет вам")
                        Ship.shipView["\(lett.rawValue)\(num.rawValue - index)"] = FieldPicture.Fire.rawValue
                        index += 1
                    } else {
                    Ship.shipView["\(lett.rawValue)\(num.rawValue - index)"] = FieldPicture.ShipDecks.rawValue
                    index += 1
                    }
                }
            } else {
                print ("При размещении карабля произошла ошибка, возможна неисправна навигация. Корабль развернут в противоположную сторону")
            }
        }
        if direction == .Down {
            if (num.rawValue + decks.rawValue) <= 10 {
                for _ in 1...decks.rawValue {
                    if let _ = Ship.shipView["\(lett.rawValue)\(num.rawValue + index)"] {
                        print ("Вы патаетесь потопить свою флотилию, произошло столкновение кораблей, возможно экстренное лавирование поможет вам")
                        Ship.shipView["\(lett.rawValue)\(num.rawValue + index)"] = FieldPicture.Fire.rawValue
                        index += 1
                    } else {
                    Ship.shipView["\(lett.rawValue)\(num.rawValue + index)"] = FieldPicture.ShipDecks.rawValue
                    index += 1
                    }
                }
            } else {
                print ("При размещении карабля произошла ошибка, возможна неисправна навигация. Корабль развернут в противоположную сторону")
            }
        }
    }

    init (numberOfDecks: Decks, directionOfMovement: Orientation, letters: Letters, numbers: Numbers) {
        self.letter = letters
        self.number = numbers
        self.deck = numberOfDecks
        self.orientation = directionOfMovement
        
        Ship.shipArray.append("\(deck.rawValue)\(orientation.rawValue)\(letter.rawValue)\(number.rawValue)")
        
        Ship.orientationShip(decks: deck, direction: orientation, lett: letter, num: number)
    }
    
    subscript (letter: String, number: Int) -> String? {
        get {
            if "A"..."J" ~= letter && 1...10 ~= number {
                let temp = battle.x.firstIndex(of: letter)!
                return Ship.shipView["\(temp + 1)\(number)"]
                
            } else {
                return nil
            }
        }
        set {
            if newValue != nil {
            let tempXy = newValue?.components(separatedBy: ",")
                var tempLetter = ""
                var tempNumber = ""
                
                if tempXy!.count > 0 {
                    tempLetter = "A"..."J" ~= tempXy![0] ? tempXy![0] : ""
                }
                if tempXy!.count > 1 {
                    tempNumber = "1"..."10" ~= tempXy![1] ? tempXy![1] : ""
                }
                
                func switchKey (dict: inout [String: String], oldKey: String, newKey: String) {
                        if let choose = dict.removeValue(forKey: oldKey) {
                            dict[newKey] = choose
                        }
                    }
                
                let temp = battle.x.firstIndex(of: letter)!
                if let tempNew = battle.x.firstIndex(of: tempLetter) {
                
                let old = "\(temp + 1)\(number)"
                let new = "\(tempNew + 1)\(tempNumber)"
                
                switchKey(dict: &Ship.shipView, oldKey: old, newKey: new)
                }
            } else {
                print ("Попытайтесь сделать перестановку в пределах допустимых значений")
            }
        
        }
    }
}


struct BattleField {
    let x = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
    let y = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    
    private var viewOnBord = [String: String]()
    private var miss = [String: String]()
    private var attack = [String: String]()
    
    
    var printBattle = ""
    
    mutating func shot (letter: Letters, number: Numbers) {
        if viewOnBord["\(letter.rawValue)\(number.rawValue)"] == FieldPicture.Empty.rawValue {
            miss["\(letter.rawValue)\(number.rawValue)"] = FieldPicture.Miss.rawValue
            viewOnBord["\(letter.rawValue)\(number.rawValue)"] = FieldPicture.Miss.rawValue
        }
        if viewOnBord["\(letter.rawValue)\(number.rawValue)"] == FieldPicture.ShipDecks.rawValue {
            attack["\(letter.rawValue)\(number.rawValue)"] = FieldPicture.Fire.rawValue
            Ship.shipView["\(letter.rawValue)\(number.rawValue)"] = FieldPicture.Fire.rawValue
        }
        fieldView(ship: Ship.shipView, miss: miss, attack: attack)
    }
    
    private func arraysToString (letter: [String], number: [Int], arrayToPrint: [String: String]) -> String {
        var finalLine = " A  B  C  D  E   F  G  H  I  J"
        
        var lineIndex = 0
        
        for num in number {
            var line = ""
            lineIndex += 1
            
            for (index, _) in letter.enumerated() {
                line += "\(arrayToPrint["\(index + 1)\(num)"]!) "
            }
            finalLine += "\n\(line) \(lineIndex)"
        }
        
        return finalLine
    }

    private mutating func fieldView (ship: [String: String], miss: [String: String], attack: [String: String]) {
        for (index, _) in x.enumerated() {
            for num in y {
                
                if let _ = ship["\(index+1)\(num)"] {
                    viewOnBord["\(index+1)\(num)"] = ship["\(index+1)\(num)"]
                } else if let _ = miss["\(index+1)\(num)"] {
                    viewOnBord["\(index+1)\(num)"] = miss["\(index+1)\(num)"]
                } else if let _ = attack["\(index+1)\(num)"] {
                    viewOnBord["\(index+1)\(num)"] = attack["\(index+1)\(num)"]
                } else {
                    viewOnBord["\(index+1)\(num)"] = FieldPicture.Empty.rawValue
                }
            }
        }
        printBattle = arraysToString(letter: x, number: y, arrayToPrint: viewOnBord)
    }

    subscript (letter: String, number: Int) -> String? {
        get {
            if "A"..."J" ~= letter && 1...10 ~= number {
                let temp = x.firstIndex(of: letter)!
                return viewOnBord["\(temp + 1)\(number)"]
            } else {
                return nil
            }
        }
    }

    init () {

        fieldView(ship: Ship.shipView, miss: miss, attack: attack)
    }
}

let ship1 = Ship(numberOfDecks: .Three, directionOfMovement: .Left, letters: .F, numbers: .Nine)
let ship2 = Ship(numberOfDecks: .Two, directionOfMovement: .Right, letters: .D, numbers: .One)
let ship3 = Ship(numberOfDecks: .Four, directionOfMovement: .Down, letters: .H, numbers: .One)
let ship4 = Ship(numberOfDecks: .Three, directionOfMovement: .Up, letters: .B, numbers: .Ten)
let ship5 = Ship(numberOfDecks: .Three, directionOfMovement: .Up, letters: .E, numbers: .Ten)
let ship6 = Ship(numberOfDecks: .Four, directionOfMovement: .Right, letters: .A, numbers: .Nine)

var battle = BattleField()

battle["G",5]
battle.shot(letter: .G, number: .Five)

battle["H",4]
battle.shot(letter: .H, number: .Four)

battle.shot(letter: .G, number: .Four)

battle.shot(letter: .H, number: .Three)

battle.shot(letter: .H, number: .Two)

battle.shot(letter: .H, number: .One)


print (battle.printBattle)
























































//set {
//            if newValue != nil {
//                let tempXy = newValue?.components(separatedBy: ",")
//                var tempLetter = ""
//                var tempNumber = ""
//
//                if tempXy!.count > 0 {
//                    tempLetter = "A"..."J" ~= tempXy![0] ? tempXy![0] : ""
//                }
//                if tempXy!.count > 1 {
//                    tempNumber = "1"..."10" ~= tempXy![1] ? tempXy![1] : ""
//                }
//
//                //                let temp = BattleField.x.firstIndex(of: letter)!
//                if let tempNew = BattleField.x.firstIndex(of: tempLetter) {
//
//
//                    if BattleField.viewOnBord["\(tempNew + 1)\(tempNumber)"] == FieldPicture.Empty.rawValue {
//                        BattleField.viewOnBord["\(tempNew + 1)\(tempNumber)"] == FieldPicture.Miss.rawValue
//                    }
//                    if BattleField.viewOnBord["\(tempNew + 1)\(tempNumber)"] == FieldPicture.ShipDecks.rawValue {
//                        BattleField.viewOnBord["\(tempNew + 1)\(tempNumber)"] == FieldPicture.Fire.rawValue
//                    }
//                }
//            }
//        }


//    static func checkMiss (arrayMiss: [String: String], arrayAttack: [String: String ]) {
//        for (index, _) in BattleField.x.enumerated() {
//            for num in BattleField.y {
//                if let _ = arrayMiss["\(index+1)\(num)"] {
//                    BattleField.viewOnBord["\(index+1)\(num)"] = arrayMiss["\(index+1)\(num)"]
//                }
//                if let _ = arrayAttack["\(index+1)\(num)"] {
//                    BattleField.viewOnBord["\(index+1)\(num)"] = arrayAttack["\(index+1)\(num)"]
//                }
//            }
//        }
//
//    }


//                if let _ = ship["\(index+1)\(num)"] {
//                    BattleField.viewOnBord["\(index+1)\(num)"] = ship["\(index+1)\(num)"]
//                }
//                if let _ = miss["\(index+1)\(num)"] {
//                    print ("Неверная ссылка")
//                    BattleField.viewOnBord["\(index+1)\(num)"] = miss["\(index+1)\(num)"]
//                }
 /*               if let _ = BattleField.attack["\(index+1)\(num)"] {
                    BattleField.viewOnBord["\(index+1)\(num)"] = BattleField.attack["\(index+1)\(num)"]
                }*/

////                let test = "\(index+1)\(num)"
//                switch test {
//                case ship["\(index+1)\(num)"] :
//                    BattleField.viewOnBord["\(index+1)\(num)"] = ship["\(index+1)\(num)"]
////                case miss["\(index+1)\(num)"] :
////                    BattleField.viewOnBord["\(index+1)\(num)"] = miss["\(index+1)\(num)"]
////                case attack["\(index+1)\(num)"] :
////                    BattleField.viewOnBord["\(index+1)\(num)"] = BattleField.attack["\(index+1)\(num)"]
//                default :
//                    BattleField.viewOnBord["\(index+1)\(num)"] = FieldPicture.Empty.rawValue
//                }

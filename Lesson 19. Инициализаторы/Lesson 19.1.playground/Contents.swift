import UIKit
import Foundation

//MARK: -Урок 1 Дезигнейтид и конвинеанс иницаилизаторы

class Student1 {
    var firstName : String
    var lastName : String

    var fullNmae : String {
        return firstName + " " + lastName
    }
    init() {
        self.firstName = ""
        self.lastName = ""
    }
}

class Student2 {
    var firstName = ""
    var lastName = ""
}

class Student3 {
    var firstName : String
    var lastName : String

    init (firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
}

class Student4 {
    var firstName : String
    var lastName : String

    init (_ firstName: String, _ lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
}

class Student5 {
    var middleName : String?
}

class Student6 {
    let maxAge : Int
    init () {
        maxAge = 100
    }
}

//class Student6_1 {            в дочернем классе невозможно переопределить родительское свойство
//    override init() {
//        super.maxAge = 11)
//    }
//}

struct Student7 {          // структура делает самостоятельный конструктор инициализатора
    var firstName : String
    var lastName : String
}

struct Student8 {          // два инициализатора на выбор
    var firstName : String
    var lastName : String

    init() {
        firstName = ""
        lastName = ""
    }

    init (firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
}



let s1 = Student1()
let s2 = Student2()
let s3 = Student3(firstName: "a", lastName: "b")
let s4 = Student4("a", "b")
let s5 = Student5()
s5.middleName = "Set"
let s7 = Student7(firstName: "b", lastName: "c")   // структура делает самостоятельный конструктор инициализатора
let s8 = Student8()                                     // два инициализатора на выбор
let s8_1 = Student8(firstName: "c", lastName: "d")  // два инициализатора на выбор

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Дезигнейтид и конвинеанс иницаилизаторы  (устанавливает все сторит проперти или по очереди ссылаясь на дезигнейтид)

class Human {
    var weight : Int
    var age : Int

    init (weight: Int, age: Int) {
        self.weight = weight
        self.age = age
    }

    convenience init (age: Int) {
        self.init (weight: 0, age: age)
    }

    convenience init (weight: Int) {
        self.init (weight: weight, age: 0)
    }

    convenience init () {
        self.init (weight: 0)
    }

    func test () {}
}

let h1 = Human(weight: 70, age: 25)
let h2 = Human(weight: 0, age: 25)
let h3 = Human(age: 70)
let h4 = Human(weight: 25)

class Student : Human {
    var firstName : String
    var lastName : String

    init (firstName: String, lastName: String) {
        self.firstName = firstName              // первым делом необходимо инициализировать дочерний класс, после родительский
        self.lastName = lastName
        super.init(weight: 0, age: 0)           // дезигнейтед может вызывать только дезигнейтед

        self.weight = 50  //  после прохода первой фазы можно использовать настройки родительского класса
        test()
    }

    convenience init (firstName: String) {   // не используется super
        //test()
        //self.firstName = "a"
        self.init (firstName: firstName, lastName: "")
        self.age = 28
        test()
    }
}

let student1 = Student(firstName: "a")
let student2 = Student(firstName: "a", lastName: "")







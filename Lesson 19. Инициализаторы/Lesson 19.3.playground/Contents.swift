import UIKit
import Foundation

//MARK: -Урок 3

//Фейлибол инициализатор. Инициализаторы велью тайпов - структур и энуом

class Human {
    var weight : Int
    var age : Int

    init (weight: Int, age: Int) {
        self.weight = weight
        self.age = age
    }

    convenience init (age: Int) {
        self.init (weight: 0, age: age)
    }

    convenience init (weight: Int) {
        self.init (weight: weight, age: 0)
    }

    convenience init () {
        self.init (weight: 0)
    }

    func test () {}
    
    deinit {
        print ("Human deinitialization")
    }
    
}


enum Color : Int {
    case Black
    case White
    
    init? (value: Int) {
        switch value {
        case 0: self = .Black
        case 1: self = .White
        default:
            return nil
        }
    }
}

let a = Color(value: 2)
a?.rawValue              // a!.rawValue

struct Size {
    var width : Int
    var height : Int
    
    init? (width: Int, height: Int) {
        self.width = width
        self.height = height
        return nil
    }
}


//class Friend {           // если без наследования
//    var name : String
//
//    init? (name: String) {
//        if name.isEmpty {
//            return nil
//        }
//        self.name = name
//    }
//}
//
//let f = Friend(name: "")
//f?.name


class Friend : Human {           // если без наследования
    var name : String

    let skin : Color = {
        let random = Int(arc4random_uniform(2))
        return Color(value: random)!
    }()
    
    init? (name: String) {
//        let random = Int(arc4random_uniform(2))
//        let color = Color(value: random)!
//        self.skin = color
        
        self.name = name
        super.init(weight: 0, age: 0)
        if name.isEmpty {
            return nil
        }
    }
    
    required init() {            // тип инициализатора, который должен быть переопределн обязательно в дочернем классе
        self.name = "Hi"
        
//        let random = Int(arc4random_uniform(2))
//        let color = Color(value: random)!
//        self.skin = color
        
        super.init(weight: 0, age: 0)
    }
    
    deinit {
        print ("Friend deinitialization")
    }
    
}

let f = Friend(name: "a")
f?.name

print (f?.name)

class BestFriend : Friend {
    override init? (name: String) {
        if name.isEmpty {
            super.init()
        } else {
            super.init(name: name)
        }
    }
    
    required init() {
        super.init()
    }
    
    deinit {
        print ("BestFriend deinitialization")
    }
}

let b = BestFriend(name: "")
b?.name

print (b?.name)


let f1 = Friend(name: "a")
f1?.skin.rawValue
let f2 = Friend(name: "a")
f2?.skin.rawValue
let f3 = Friend(name: "a")
f3?.skin.rawValue
let f4 = Friend(name: "a")
f4?.skin.rawValue
let f5 = Friend(name: "a")
f5?.skin.rawValue


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Деинициализаторы

struct Test {
    var bestFriend : BestFriend? = BestFriend(name: "")
}

var test : Test? = Test()

test?.bestFriend = nil



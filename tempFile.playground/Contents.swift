import UIKit

//MARK: -Practical part №1

//class Student {
//    var name : String
//    var surName : String
//    var yearOfBorn : Int
//    var mark : Double
//
//    var fullname: String {
//        return name + " " + surName
//    }
//
//    func printStudent () {
//        print ("Студент \(surName) \(name) - \(yearOfBorn) года рождения. Его оценка: \(mark)")
//    }
//
//    init (name: String, surName: String, yearOfBorn: Int, mark: Double) {
//        self.name = name
//        self.surName = surName
//        self.yearOfBorn = yearOfBorn
//        self.mark = mark
//    }
//}
//
//let studentOne = Student(name: "Masha", surName: "Kogevnikova", yearOfBorn: 1990, mark: 5.0)
////studentOne.printStudent()
//
//let studentTwo = Student(name: "Den", surName: "Smirnov", yearOfBorn: 1976, mark: 4.5)
//let studentThree = Student(name: "Serg", surName: "Jason", yearOfBorn: 1987, mark: 4.8)
//let studentFour = Student(name: "Sasha", surName: "Lisin", yearOfBorn: 2007, mark: 4.2)
//
//var arrayStudent = [studentOne, studentTwo, studentThree, studentFour]
//let sortArrayStudent = arrayStudent.sorted { (a, b) -> Bool in
//    return a.mark > b.mark
//}
//
//for student in sortArrayStudent {
//    student.printStudent()
//}



//MARK: -Practical part №2

//func emptyFunc () -> String {
//    return "emptyFunc -> emptyText"
//}
//
//class WorkBook {
//    let maxPages = 12
//    var text : Int
//    let wordsOnPage = 250
//
//    var pages : (Int, Int) {
//        return (text / wordsOnPage, 12 - (text / wordsOnPage))
//    }
//
//    var calcWorkBookQuantity : Int {
//        return pages.0 / 12
//    }
//
//    lazy var emptyProperty = emptyFunc()
//
//    init (text: Int) {
//        self.text = text
//    }
//}
//
//let workBookOne = WorkBook(text: 12678)
//workBookOne.calcWorkBookQuantity
//workBookOne.emptyProperty


//MARK: -Practical part №3

//class Pacient {
//    var inHospital = true {
//        willSet {
//            print ("MARK: pacient is out from hospital")
//        }
//
//        didSet {
//            if inHospital == false {
//                inSport = true
//                needDrugs = 0
//                storyAboutHospital = "I leave from hospital, where i did heave a madical help"
//            }
//        }
//    }
//    var inSport = false
//    var needDrugs = 5
//    var storyAboutHospital = ""
//}
//
//var human = Pacient()
//human.inSport
//human.needDrugs
//human.storyAboutHospital
//
//human.inHospital = false
//human.inSport
//human.needDrugs
//human.storyAboutHospital



//MARK: -Practical part №4

//typealias Tuple = (Int, Int, String)
//let testTuple : Tuple = (3, 2, "One")
//
//typealias  Meters = Int
//typealias  KiloMeters = Int
//
//let testMetersOne : Meters = 20
//let testMetersTwo : Meters = 30
//
//let testKiloMetersOne : KiloMeters = 30
//let testKiloMetersTwo : KiloMeters = 50
//
//let testIntOne = 20
//let testIntTwo = 50
//
//let sumMeters = testMetersOne + testMetersTwo
//let sumKM = testKiloMetersOne + testKiloMetersTwo
//let sumInt = testIntOne + testIntTwo



//MARK: -Practical part №5

//Используя протоколы написать программу «Ферма». Задача: программа «Ферма» выполняет простые действия: «собирать урожай», «поливать», «окучивать», «продавать», «удобрять» и выводит их в консоль.

//Алгоритм:
//1) Создаем класс «Ферма1» (например)
//2) Создаем протоколы действий
//3) Этим протоколам задаем методы и свойства.
//4) Активируем протоколы для класса «Ферма1»
//5) Реализуем все методы и свойства протоколов в классе
//6) Выводим результаты в консоль.

//Например: «Протокол «Собирать урожай» активирован. И уже тут оставляйте те действия, реплики работников, которые они реализуют во время сборки урожая, как пример.

//to harvest, to water, to Spud, to sell, to fertilize

protocol Harvest {
    var field : Bool { get }
    func toHarvest () -> ()
}

protocol Water {
    var waterDistribution : Bool { get }
    func toWater () -> ()
}

class FarmOne {
    var farmer : Int?
    var tractor : Int?
    var car : Int?
    
    var technikCount : Int {
        let testTractor = tractor ?? 0
        let testCar = car ?? 0
        
        return testTractor + testCar
    }
}

extension FarmOne: Harvest {
    var field : Bool { return true}
    
    func toHarvest () -> () {
        print ("Сбор урожая")
    }
}

extension FarmOne: Water {
    var waterDistribution : Bool { return true}

    func toWater() {
        print ("Полеваем урожай")
    }
}

var farm = FarmOne()
farm.car = 5
farm.tractor = 3
farm.toHarvest()
farm.technikCount
farm.toWater()

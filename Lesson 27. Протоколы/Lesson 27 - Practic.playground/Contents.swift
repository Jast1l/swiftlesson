import UIKit
import Foundation

//MARK: -Part 1

protocol Priority {
    var order : Int { get }
}

protocol EntryName : Priority {   //доступно кк наследование, так и подписание на несколько протоклов через заяпяту.
    var label : String { get }
    
    mutating func printLabel()   //PART 2 ____ для классов не нужно указывать при его создании мутатинг из протокола, а для структур уже понадобится
    
    init (name: String)
}

class Human {
    
}

class Farmer : Human, EntryName, Priority {
    var firstName : String
    var lastName : String
    
    var fullName : String {
        return firstName + " " + lastName
    }
    
    init (firstName : String, lastName : String) {
        self.firstName = firstName
        self.lastName = lastName
    }
    
    required init (name: String) {
        self.firstName = name
        self.lastName = "Annow"
    }
    
    var label : String {
        return fullName
    }
    
    let order = 1
    
    func printLabel() {
        firstName += "newName"
        print (firstName + "farmer")
    }
}

class Animal {
    
}

class Cow : Animal, EntryName{
    var name : String?
    
    var label: String {
        return name ?? "a cow"
    }
    
    let order = 2
    
    func printLabel() {
        print (label)
    }
    
    required init (name: String) {
        self.name = name
    }
}

struct Grass : EntryName {
    var type : String
    
    var label: String {
        return "Grass: " + type
    }
    
    let order = 3
    
    func printLabel() {
        print ("This is grass")
    }
    
    init (name: String) {
        self.type = name
    }
    
    init (type: String) {
        self.type = type
    }
}


let farmer1 = Farmer(firstName: "Bob", lastName: "Shmob")
let farmer2 = Farmer(firstName: "Bill", lastName: "Shmill")
let farmer3 = Farmer(firstName: "Brian", lastName: "Shmian")

let cow1 = Cow(name: "Burenka")
//cow1.name = "Burenka"
let cow2 = Cow(name: "Маришка")

let grass1 = Grass(type: "Bermuda")
let grass2 = Grass(type: "St. Augustina")

var array : [EntryName] /*[Any]*/= [cow1, farmer1, grass2, cow2, farmer3, grass1, farmer2]

//for value in array {
//    if let grass = value as? Grass {
//        print (grass.type)
//    } else if let farmer = value as? farmer {
//        print (farmer.fullName)
//    } else if let cow = value as? Cow {
//        print (cow.name ?? "a cow")
//    }
    
//    switch value {
//    case let grass as Grass: print (grass.type)
//    case let farmer as farmer: print (farmer.fullName)
//    case let cow as Cow: print (cow.name ?? "a cow")
//    default: break
//    }
//}

func printFarm (array: inout [EntryName]) {

    array.sort (by: {a, b in
        if a.order == b.order {
            return a.label.lowercased() < b.label.lowercased()
        } else {
            return a.order < b.order
        }
    })
    
    for value in array {
        print (value.label)
    }
}

printFarm(array: &array)



//MARK: -Part 2

print ("\n")

cow1.printLabel()
//farmer1.fullName
farmer1.printLabel()
grass1.printLabel()

// для массововй замены названия классов как при его создании, так и во всех используемых моментах используется edit all in scope
// при использовании повторяющихся названий с разными номерами или отличающимися незначительными символами - можно использовать ctrl + F -> replace

var someAnimal : EntryName = Cow(name: "someAnimal") // пример



UInt.max
UInt.min

protocol MiddleValue {
    static func mid() -> UInt
}

extension UInt : MiddleValue {
    static func mid() -> UInt {
        return (UInt.max + UInt.min) / 2
    }
}

UInt.mid()

extension EntryName {    // вводим расширение для протокола вынося фнкцию из класса
    func printSomeText() {
        print ("Some text")
    }
}

cow1.printSomeText()
farmer1.printSomeText()

//class Human {    вставлен перед Farmer
//
//}

var array1 : [EntryName & Priority] = [farmer1, cow1]

func someFunc (unit: EntryName & Priority) {
    print (unit.label)
    print (unit.printSomeText())
}

print ("\n")

var testArray : [Any] = [123, "sdf", farmer2, grass2, cow1]
for obj in testArray {
    print (obj)
    if let testObj = obj as? EntryName {
        print (testObj.label)
    }
}


//ДЗ
//protocol Container : Collection, CollectionType {  //Indexable
//
//    mutating func pop() -> Self.Generator.Element
//    mutating func push(element: Self.Generator.Element)
//    func peek() -> Self.Generator.Element?
//}
//
//Новичек
//1. прочитать о том, что такое стек и очередь
//2. Используя выше указанный протакол, сделать расширение "Conteiner" для классов "Array" таким образом, чтобы массивом можно было управлять как стеком. Небольшая подсказка, указать в качестве типа данных вместо Self.Generator.Element можно просто Element
//
//Студент
//далле фото на рабочем столе


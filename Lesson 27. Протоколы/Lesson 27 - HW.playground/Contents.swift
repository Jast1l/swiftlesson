import UIKit
import Foundation

//MARK: -1. Объявить протокол Food, который будет иметь проперти name (только чтение) и метод taste(), который будет выводить текст со вкусовыми ощущениями

//MARK: -2. Все продукты разных типов, которые вы принесли из супермаркета, находятся в сумке (массив) и все, как ни странно, реализуют протокол Food. Вам нужно пройтись по сумке, назвать предмет и откусить кусочек. Можете отсортировать продукты до имени. Используйте для этого отдельную функцию, которая принимает массив продуктов

//MARK: -3. Некоторые продукты могут испортиться, если их не положить в холодильник. Создайте новый протокол Storable, он наследуется от протокола Food и содержит еще булевую проперти - expired. У некоторых продуктов замените Food на Storable. Теперь пройдитесь по всем продуктам и, если продукт надо хранить в холодильнике, то перенесите его туда, но только если продукт не испорчен уже, иначе просто избавьтесь от него. Используйте функцию для вывода продуктов для вывода содержимого холодильника

//MARK: -4. Добавьте проперти daysToExpire в протокол Storable. Отсортируйте массив продуктов в холодильнике. Сначала пусть идут те, кто быстрее портятся. Если срок совпадает, то сортируйте по имени.

//MARK: -5. Не все, что мы кладем в холодильник, является едой. Поэтому сделайте так, чтобы Storable не наследовался от Food. Мы по прежнему приносим еду домой, но некоторые продукты реализуют теперь 2 протокола. Холодильник принимает только те продукты, которые еще и Storable. функция сортировки должна по прежнему работать.
    // Пример: var array : [protocol <EntryName, Priority>]





//MARK: -1. Объявить протокол Food, который будет иметь проперти name (только чтение) и метод taste(), который будет выводить текст со вкусовыми ощущениями



protocol Food {
    var name : String { get }
    func taste()
}

protocol Storable {
    var expired : Bool { get }
    var daysToExpire : Int { get }
}

//protocol <Food, Storable>

var bag = [Food]()

class Milky : Food , Storable {
    var title : String
    var impression : String
    var daysBeforeDamage : Int
    
    func taste() {
        print ("\(title) - \(impression)")
    }
    
    init (title: String, impression: String, daysBeforeDamage: Int) {
        self.title = title
        self.impression = impression
        self.daysBeforeDamage = daysBeforeDamage
        
        bag.append(self)
    }
    
    var name : String { return title }
    var expired : Bool { return daysBeforeDamage <= 0 ? true : false }
    var daysToExpire : Int { return daysBeforeDamage }
}

class Meaty : Food , Storable {
    var title : String
    var impression : String
    var daysBeforeDamage : Int
    
    func taste() {
        print ("\(title) - \(impression)")
    }
    
    init (title: String, impression: String, daysBeforeDamage: Int) {
        self.title = title
        self.impression = impression
        self.daysBeforeDamage = daysBeforeDamage
        
        bag.append(self)
    }
    
    var name : String { return title }
    var expired : Bool { return daysBeforeDamage <= 0 ? true : false }
    var daysToExpire : Int { return daysBeforeDamage }
}

class CannedFood : Food {
    var title : String
    var impression : String
    
    func taste() {
        print ("\(title) - \(impression)")
    }
    
    init (title: String, impression: String) {
        self.title = title
        self.impression = impression
        
        bag.append(self)
    }
    
    var name : String { return title }
}
 
class Fruits : Food , Storable {
    var title : String
    var impression : String
    var daysBeforeDamage : Int
    
    func taste() {
        print ("\(title) - \(impression)")
    }
    
    init (title: String, impression: String, daysBeforeDamage: Int) {
        self.title = title
        self.impression = impression
        self.daysBeforeDamage = daysBeforeDamage

        bag.append(self)
    }
    
    var name : String { return title }
    var expired : Bool { return daysBeforeDamage <= 0 ? true : false }
    var daysToExpire : Int { return daysBeforeDamage }
}

class Vegetables : Food {
    var title : String
    var impression : String
    
    func taste() {
        print ("\(title) - \(impression)")
    }
    
    init (title: String, impression: String) {
        self.title = title
        self.impression = impression
        
        bag.append(self)
    }
    
    var name : String { return title }
}

class Drugs : Food {
    var title : String
    var impression : String
    
    func taste() {
        print ("\(title) - \(impression)")
    }
    
    init (title: String, impression: String) {
        self.title = title
        self.impression = impression
        
        bag.append(self)
    }
    
    var name : String { return title }
}
//Milk(), Meat(), CannedFood(), Fruits(), Vegetables(), Drugs()


var yogurt = Milky(title: "Активия", impression: "сытная", daysBeforeDamage: 1)
var milk = Milky(title: "Домик в деревне", impression: "вкусный", daysBeforeDamage: 0)
var cheese = Milky(title: "Домашний", impression: "соленоватый", daysBeforeDamage: 3)

var sausages = Meaty(title: "Останкинские", impression: "вкусные", daysBeforeDamage: 4)
var steak = Meaty(title: "Гурман", impression: "аппетитный", daysBeforeDamage: 0)

var sprats = CannedFood(title: "Русское море", impression: "хорошая закуска")
var stewedMeat = CannedFood(title: "Главпродукт", impression: "питательная")

var apples = Fruits(title: "Антоновка", impression: "свежие", daysBeforeDamage: 4)
var oranges = Fruits(title: "Крымские", impression: "кислые", daysBeforeDamage: 0)

var potato = Vegetables(title: "Сельская", impression: "колорийная")
var beet = Vegetables(title: "Деревенская", impression: "сладкая")

var vitamins = Drugs(title: "Мультивит", impression: "полезные")
var valerian = Drugs(title: "Корень валерианы", impression: "горьковат, но успокаивает")


//MARK: -2. Все продукты разных типов, которые вы принесли из супермаркета, находятся в сумке (массив) и все, как ни странно, реализуют протокол Food. Вам нужно пройтись по сумке, назвать предмет и откусить кусочек. Можете отсортировать продукты до имени. Используйте для этого отдельную функцию, которая принимает массив продуктов

bag

func inBagAndSortABC(array: inout [Food]) {
    
    array.sort { (a, b) -> Bool in
        return a.name.lowercased() < b.name.lowercased()
        }
    
    for item in array {
        item.taste()
    }
}

inBagAndSortABC(array: &bag)


//MARK: -3. Некоторые продукты могут испортиться, если их не положить в холодильник. Создайте новый протокол Storable, он наследуется от протокола Food и содержит еще булевую проперти - expired. У некоторых продуктов замените Food на Storable. Теперь пройдитесь по всем продуктам и, если продукт надо хранить в холодильнике, то перенесите его туда, но только если продукт не испорчен уже, иначе просто избавьтесь от него. Используйте функцию для вывода продуктов для вывода содержимого холодильника

var fridge = [Food & Storable]()
var garbage = [Food & Storable]()

func sortProductToFridgeAndGarbage (product: inout [Food], fridge: inout [Food & Storable], garbage: inout [Food & Storable]) {
    
    var tempProductArray = [Food]()
    
    product.sort { (a, b) -> Bool in
        return a.name.lowercased() < b.name.lowercased()
        }
    
    for item in product {
        if let test = item as? Food & Storable {
            if test.expired {
                fridge.append(test)
            } else {
                garbage.append(test)
            }
        } else {
            tempProductArray.append(item)
        }
    }
    
    product = tempProductArray
    
    let testTittle = "После разбора купленных предметов:"
    let testFridge = " - в холодильник были убраны: "
    let fridgeIsEmpty = "в холодильник не было ничего перемещено"
    let testGarbage = " - следующие покупки были выброшены: "
    let garbageIsEmpty = "ничего не было выброшено"
    
    print ("\n")
    print (testTittle)
    
    if fridge.isEmpty {
        print (fridgeIsEmpty)
    } else {
        var resultString = testFridge
        for item in fridge {
            resultString += " \"\(item.name)\" "
        }
        print (resultString)
    }
    
    if garbage.isEmpty {
        print (garbageIsEmpty)
    } else {
        var resultString = testGarbage
        for item in garbage {
            resultString += " \"\(item.name)\" "
        }
        print (resultString)
    }
    
    let textBag = "\nСледующие продукты не обязательно убирать в холодильник: "
    let bagIsEmpty = "\nумка с покупками разобрана полностью"
    
    if product.isEmpty {
        print (bagIsEmpty)
    } else {
        var resultString = textBag
        for item in product {
            resultString += " \"\(item.name)\" "
        }
        print (resultString)
    }
}

sortProductToFridgeAndGarbage(product: &bag, fridge: &fridge, garbage: &garbage)

bag
fridge
garbage


//MARK: -4. Добавьте проперти daysToExpire в протокол Storable. Отсортируйте массив продуктов в холодильнике. Сначала пусть идут те, кто быстрее портятся. Если срок совпадает, то сортируйте по имени.

func sortFridgeByDaysToExpire (arrayOnFridge: inout [Food & Storable]) {
    arrayOnFridge.sort { (a, b) -> Bool in
        if a.daysToExpire != b.daysToExpire {
            return a.daysToExpire < b.daysToExpire
        } else {
            return a.name.lowercased() < b.name.lowercased()
        }
    }
    
    var resulrString = "В холодильнике сейчас:"
    let fridgeIsEmpty = "нет продуктов"
    if arrayOnFridge.isEmpty {
        resulrString += fridgeIsEmpty
    } else {
        for item in arrayOnFridge {
            resulrString += " \"\(item.name)\""
        }
    }
    
    print (resulrString)
}

print ("\n")

sortFridgeByDaysToExpire(arrayOnFridge: &fridge)


//MARK: -5. Не все, что мы кладем в холодильник, является едой. Поэтому сделайте так, чтобы Storable не наследовался от Food. Мы по прежнему приносим еду домой, но некоторые продукты реализуют теперь 2 протокола. Холодильник принимает только те продукты, которые еще и Storable. функция сортировки должна по прежнему работать.

//Активия - сытная
//Антоновка - свежие
//Главпродукт - питательная
//Гурман - аппетитный
//Деревенская - сладкая
//Домашний - соленоватый
//Домик в деревне - вкусный
//Корень валерианы - горьковат, но успокаивает
//Крымские - кислые
//Мультивит - полезные
//Останкинские - вкусные
//Русское море - хорошая закуска
//Сельская - колорийная
//
//
//После разбора купленных предметов:
// - в холодильник были убраны:  "Гурман"  "Домик в деревне"  "Крымские"
// - следующие покупки были выброшены:  "Активия"  "Антоновка"  "Домашний"  "Останкинские"
//
//Следующие продукты не обязательно убирать в холодильник:  "Главпродукт"  "Деревенская"  "Корень валерианы"  "Мультивит"  "Русское море"  "Сельская"
//
//
//В холодильнике сейчас: "Гурман" "Домик в деревне" "Крымские"

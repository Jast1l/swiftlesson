import UIKit
import Foundation

struct TestStudent {
    var testFirstName : String {
        willSet (newFirstName){
            print ("старое значение переменной \(testFirstName) собираемся поменять на \(newFirstName)")
        }
        didSet (oldFirstName){
            print ("при замене значение перемнной на \(testFirstName) сохранено старое значение \(oldFirstName)")
            
            testFirstName = testFirstName.capitalized
        }
    }
    
    var testSurName : String {
        didSet {
            testSurName = testSurName.capitalized
        }
    }
    
    var testFullName : String {
        
        get {
            return "\(testFirstName) \(testSurName)"
        }
        
        set {      //по умолчанию newValue
            print ("Хочу изменить FullName - \(testFullName) на \(newValue)")
            
            let words = newValue.components(separatedBy: " ")
            
            if words.count > 0 {
                testFirstName = words[0]
            }
            
            if words.count > 1 {
                testSurName = words[1]
            }
            
        }
    }
}

var testStudent = TestStudent(testFirstName: "Хуй", testSurName: "Машонкин")
testStudent.testFirstName
testStudent.testSurName
testStudent.testFullName

testStudent.testFirstName = "Куни"
testStudent.testSurName = "Лини"
testStudent.testFullName

testStudent.testFullName = "Дуня Кулачкова"

testStudent.testFullName


struct Student {
    var firstName : String {
        willSet (newName) {
            print ("old name \(firstName) собираюсь поменять на newName \(newName)")
        }
        didSet (oldName) {
            print ("oldname \(oldName) уже поменял на newName \(firstName)")
        }
    }
}

var student = Student(firstName: "Сергей")

student.firstName = "Кирилл"


var studenttwo = Student(firstName: "Денис")

var studentThree = Student(firstName: "Кирилл")


struct StudentTemp {
    var firstName : String {
        didSet  {
            if  Int(firstName) != nil {
                print("Ошибка! Ваше имя состоит из цифр")
                firstName = oldValue
            }
        }
    }
}

var stud = StudentTemp(firstName: "Сергей")
stud.firstName = "123"
print(stud.firstName)
stud.firstName = "Кирилл"
print(stud.firstName)



struct FixedLengthRange {
    var firstValue: Int
    let length: Int
}
var rangeOfThreeItems = FixedLengthRange(firstValue: 0, length: 3)
// диапазон чисел 0, 1, 2
rangeOfThreeItems.firstValue = 6
// сейчас диапазон чисел 6, 7, 8

rangeOfThreeItems.firstValue = 9

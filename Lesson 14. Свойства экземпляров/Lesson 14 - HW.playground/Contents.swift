import UIKit
import Foundation
//import

//1. Самостоятельно повторить проделанное в уроке
//
//2. Добавить студенту property «Дата рождения» (пусть это будет еще одна структура, содержащая день, месяц, год) и два computed property: первое — вычисляющее его возраст, второе — вычисляющее, сколько лет он учился (считать, что он учился в школе с 6 лет, если студенту меньше 6 лет — возвращать 0)
//
//3. Создать структуру «Отрезок», содержащую две внутренние структуры «Точки». Структуру «Точка» создать самостоятельно, несмотря на уже имеющуюся в Swift’е. Таким образом, структура «Отрезок» содержит две структуры «Точки» — точки A и B (stored properties). Добавить два computed properties: « середина отрезка» и «длина» (считать математическими функциями)
//
//4. При изменении середины отрезка должно меняться положение точек A и B. При изменении длины, меняется положение точки B

//MARK: -Block 2


func calcAge(birthday: String?) -> Int? {
    
    guard let testDate = birthday else {
        print ("!!!ОШИБКА ФУНКЦИИ!!! не поступила дата для работы функции")
        return nil
    }
    
    let dateFormater = DateFormatter()
    dateFormater.dateFormat = "dd.MM.yyyy"
    
    guard let birthdayDate = dateFormater.date(from: testDate) else {
        print ("!!!ОШИБКА ФУНКЦИИ!!! - неверная принимаемая дата")
        return nil
    }
    
    //    let birthdayDate = dateFormater.date(from: testDate)  // использование гварда
    
    let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
    let now = Date()
    let calcAge = calendar.components(.year, from: birthdayDate, to: now, options: [])
    let age = calcAge.year
    return age
}


struct StudentInformation {
    
    struct DateSetup {
        var day : Int {
            willSet {
                if newValue == 0 || newValue > 31 {
                    print ("ОШИБКА СТРУКТУРЫ!!! - проверьте вводимую дату рождения (значение day должно быть от 1 до 31)")
                }
            }
        }
        
        var month : Int
        var year : Int
    }
    
    var name : String {
        didSet {
            name = name.capitalized
        }
    }
    
    var surName : String {
        didSet {
            surName = surName.capitalized
        }
    }
    
    var fullName : String {
        get {
            return "\(surName) \(name)"
        }
        set {
            let words = newValue.components(separatedBy: " ")
            
            if words.count > 2 {
                print ("!!!Ошибка ввода фамили и имени - введены значения через пробелы или указано неиспользуемое отчество")
            }
            
            if words.count > 0 {
                surName = words[0]
            }
            if words.count > 1 {
                name = words[1]
            }
        }
    }
    
    var dayOfBithday : DateSetup
    
    var fullDoB : String {
        get {
            if dayOfBithday.day == 0 || dayOfBithday.day > 31 {
                print ("!!!Ошибка даты - неверный диаппазон числа рождения")
            }
            if dayOfBithday.month == 0 || dayOfBithday.month > 12 {
                print ("!!!Ошибка даты - неверный диаппазон месяца рождения рождения")
            }
            if dayOfBithday.year < 0 {
                print ("!!!Ошибка даты - вы родились до нашей эры?)")
            }
            return "\(dayOfBithday.day).\(dayOfBithday.month).\(dayOfBithday.year)"
        }
        
        set {
            let seporatedDoB = newValue.components(separatedBy: ".")
            
            if seporatedDoB.count > 0 {
                dayOfBithday.day = Int(seporatedDoB[0])!
            }
            
            if seporatedDoB.count > 1 {
                dayOfBithday.month = Int(seporatedDoB[1])!
            }
            
            if seporatedDoB.count > 2 {
                dayOfBithday.year = Int(seporatedDoB[2])!
            }
        }
    }
    
    var age : Int {
        get {
            if let tempCalcAge = calcAge(birthday: fullDoB) {
                return tempCalcAge
            } else {
                print ("ОШИБКА! в структуре при приеме функции - необходима проверка вводимых дат")
                return 0
            }
        }
    }
    
    var yearsOfStudy : Int {
        get {
            if age >= 17 {
                return 11
            } else if 6...17 ~= age {
                return age - 6
            } else if 1...5 ~= age {
                print ("В школу пока рано, потерпи немного...")
                return 0
            } else {
                print ("\nОшибка расчета возраста, проверьте указанную дату рождения")
                return 0
            }
        }
    }
}

var studentOne = StudentInformation(name: "Sergey", surName: "Ermakov", dayOfBithday: StudentInformation.DateSetup.init(day: 32, month: 13, year: -12))

//studentOne.name
//studentOne.surName
//
//studentOne.fullName    //сделал в обратном порядке - характерно для РФ
//
////test new set fullName
//
//studentOne.fullName = "Pishchalnikov Sergey Юрьевич"
//
//studentOne.name
//studentOne.surName
//
////test расчетки возраста и кол-ва лет обучение по программе 1-4 (11 полных лет обучения)
//
//studentOne.fullDoB    // можно ввести другую дату согласно формату
//
//studentOne.fullDoB = "18.08.1990"
//studentOne.dayOfBithday.day
//studentOne.dayOfBithday.month
//studentOne.dayOfBithday.year
//
////------>
//
//studentOne.age
//studentOne.yearsOfStudy



//MARK: -Block 3 - 4
//


struct Point {
    var x: Double
    var y: Double
}

struct Line {

    var point_A : Point
    var point_B : Point

    var midlePoint: Point {

        get {
            return Point(x: ((point_A.x + point_B.x) / 2), y: ((point_A.y + point_B.y) / 2))
        }

        set{
            let shift_x = newValue.x - midlePoint.x
            let shift_y = newValue.y - midlePoint.y

            point_A.x = point_A.x + shift_x
            point_A.y = point_A.y + shift_y

            point_B.x = point_B.x + shift_x
            point_B.y = point_B.y + shift_y
        }

    }

    var length : Double {

        get {
            let leg_A = point_B.x - point_A.x
            let leg_B = point_B.y - point_A.y

            return sqrt(pow(leg_A, 2) + pow(leg_B, 2)) //FIXME - возведение в степень pow(num, power)  и извлечение корня sqrt(num)
        }

        set {
            let newLengthСoefficient = newValue / length

            let leg_A = point_B.x - point_A.x
            let leg_B = point_B.y - point_A.y

            point_B.x = (leg_A + newValue) / newLengthСoefficient
            point_B.y = (leg_B + newValue) / newLengthСoefficient

        }
    }
}


var testLineOne = Line(point_A: Point.init(x: 2, y: 5), point_B: Point.init(x: 10, y: 8))

testLineOne.point_A.x
testLineOne.point_A.y

testLineOne.point_B.x
testLineOne.point_B.y

testLineOne.midlePoint.x
testLineOne.midlePoint.y

//test middle point new set

testLineOne.midlePoint.x = 7
testLineOne.midlePoint.y = 7.5

testLineOne.point_A.x
testLineOne.point_A.y

testLineOne.point_B.x
testLineOne.point_B.y

//view length

testLineOne.length   //  8.5

//test set new lenght

testLineOne.length = 10

testLineOne.point_B.x
testLineOne.point_B.y



//MARK: -Block 5 by Oxy
//
//две дату
//общая сумма потраченная 14230
//функция принимающая даты, сумму и считающая стоимость одного дня за период
//


let dateOne : String?
let dateTwo : String?
let price : Int?

dateOne = "17.09.2020"
dateTwo = "15.10.2020"
price = 14_230

func calcPriceOfDay(dateOne: String?, dateTwo: String?, fullPrice: Int?) -> Int {

    let dateFormater = DateFormatter()
    var calcDay = 0

    dateFormater.dateFormat = "dd.MM.yyyy"

    if dateOne != nil && dateTwo != nil && fullPrice != nil{

        let dayOneDate = dateFormater.date(from: dateOne!)
        let dayTwoDate = dateFormater.date(from: dateTwo!)

        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let calc = calendar.components(.day , from: dayOneDate!, to: dayTwoDate!, options: [])
        calcDay = calc.day!
        let priceOfDay = fullPrice! / calcDay

        print ("""
            с \(dateOne!) по \(dateTwo!) прошло \(calcDay)
            при общей сумме уплаченной за период \(price!) стоимость одного дня \(priceOfDay)
            \n
        """)
        return priceOfDay

    } else {
        print ("Ошибка ввода данных")
        return 0
    }

}

let test = calcPriceOfDay(dateOne: dateOne, dateTwo: dateTwo, fullPrice: price)

let test2 = calcPriceOfDay(dateOne: "12.08.2005", dateTwo: "30.05.2008", fullPrice: 3_825_200)

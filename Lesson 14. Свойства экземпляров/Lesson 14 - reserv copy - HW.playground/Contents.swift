import UIKit
import Foundation
//import

//1. Самостоятельно повторить проделанное в уроке
//
//2. Добавить студенту property «Дата рождения» (пусть это будет еще одна структура, содержащая день, месяц, год) и два computed property: первое — вычисляющее его возраст, второе — вычисляющее, сколько лет он учился (считать, что он учился в школе с 6 лет, если студенту меньше 6 лет — возвращать 0)
//
//3. Создать структуру «Отрезок», содержащую две внутренние структуры «Точки». Структуру «Точка» создать самостоятельно, несмотря на уже имеющуюся в Swift’е. Таким образом, структура «Отрезок» содержит две структуры «Точки» — точки A и B (stored properties). Добавить два computed properties: « середина отрезка» и «длина» (считать математическими функциями)
//
//4. При изменении середины отрезка должно меняться положение точек A и B. При изменении длины, меняется положение точки B

//MARK: -Block 2

func calcAge(birthday: String) -> Int {
    let dateFormater = DateFormatter()
    dateFormater.dateFormat = "dd.MM.YYYY"
    let birthdayDate = dateFormater.date(from: birthday)
    let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
    let now = Date()
    let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
    let age = calcAge.year
    return age!
}


struct StudentInformation {
    
    struct DateSetup {
        var day : Int
        var month : Int
        var year : Int
    }

    var name : String {
        didSet {
            name = name.capitalized
        }
    }

    var surName : String {
        didSet {
            surName = surName.capitalized
        }
    }

            var fullName : String {
                get {
                    return "\(surName) \(name)"
                }
                set {
                    let words = newValue.components(separatedBy: " ")
                    
                    if words.count > 0 {
                        surName = words[0]
                    }
                    if words.count > 1 {
                        name = words[1]
                    }
                }
            }
            
    var dayOfBithday : DateSetup
            
            var fullDoB : String {
                get {
                    return "\(dayOfBithday.day).\(dayOfBithday.month).\(dayOfBithday.year)"
                }
                
                set {
                    let seporatedDoB = newValue.components(separatedBy: ".")

                    if seporatedDoB.count > 0 {
                        dayOfBithday.day = Int(seporatedDoB[0])!
                    }

                    if seporatedDoB.count > 1 {
                        dayOfBithday.month = Int(seporatedDoB[1])!
                    }

                    if seporatedDoB.count > 2 {
                        dayOfBithday.year = Int(seporatedDoB[2])!
                    }
                }
            }
            
            var age : Int {
                get {
                    return calcAge(birthday: fullDoB)
                }
            }
    
            var yearsOfStudy : Int {
                get {
                    if age >= 6 && (age - 6) >= 11 {
                        return 11
                        } else if (age - 6) < 11 {
                            return age - 6
                            } else {
                                return 0
                                }
                }
            }
}

var studentOne = StudentInformation(name: "Sergey", surName: "Ermakov", dayOfBithday: StudentInformation.DateSetup.init(day: 27, month: 11, year: 2004))

studentOne.name
studentOne.surName

studentOne.fullName    //сделал в обратном порядке - характерно для РФ

//test new set fullName

studentOne.fullName = "Pishchalnikov Sergey"

studentOne.name
studentOne.surName

//test расчетки возраста и кол-ва лет обучение по программе 1-4 (11 полных лет обучения)

studentOne.fullDoB    // можно ввести другую дату согласно формату

//studentOne.fullDoB = "18.08.1990"
studentOne.dayOfBithday.day
studentOne.dayOfBithday.month
studentOne.dayOfBithday.year

//------>

studentOne.age
studentOne.yearsOfStudy



//MARK: -Block 3 - 4

struct Point {
    var x: Double
    var y: Double
}

struct Line {
    
    var point_A : Point
    var point_B : Point
    
    var midlePoint: Point {
        
        get {
            return Point(x: ((point_A.x + point_B.x) / 2), y: ((point_A.y + point_B.y) / 2))
        }
        
            set{
                let shift_x = newValue.x - midlePoint.x
                let shift_y = newValue.y - midlePoint.y
                
                point_A.x = point_A.x + shift_x
                point_A.y = point_A.y + shift_y
                
                point_B.x = point_B.x + shift_x
                point_B.y = point_B.y + shift_y
            }

    }
    
    var length : Double {
        
        get {
            let leg_A = point_B.x - point_A.x
            let leg_B = point_B.y - point_A.y
            
            return sqrt(pow(leg_A, 2) + pow(leg_B, 2)) //FIXME - возведение в степень pow(num, power)  и извлечение корня sqrt(num)
        }
        
            set {
                let newLengthСoefficient = newValue / length
                
                let leg_A = point_B.x - point_A.x
                let leg_B = point_B.y - point_A.y
                
                point_B.x = (leg_A + newValue) / newLengthСoefficient
                point_B.y = (leg_B + newValue) / newLengthСoefficient
                
            }
    }
}


var testLineOne = Line(point_A: Point.init(x: 2, y: 5), point_B: Point.init(x: 10, y: 8))

testLineOne.point_A.x
testLineOne.point_A.y

testLineOne.point_B.x
testLineOne.point_B.y

testLineOne.midlePoint.x
testLineOne.midlePoint.y

//test middle point new set

testLineOne.midlePoint.x = 7
testLineOne.midlePoint.y = 7.5
    
testLineOne.point_A.x
testLineOne.point_A.y

testLineOne.point_B.x
testLineOne.point_B.y

//view length

testLineOne.length   //  8.5

//test set new lenght

testLineOne.length = 10

testLineOne.point_B.x
testLineOne.point_B.y


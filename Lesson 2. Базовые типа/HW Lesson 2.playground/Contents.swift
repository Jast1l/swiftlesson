//Block 1 - Print Min/Max
/*
Int.min
Int.max
UInt.min
UInt.max
Int8.min
Int8.max
*/
print ("HW Block №1")
print ("Примеры минимальных и максимальных значений типов чисел")
print ("\n")
//FIX: Не нужно считать пробелы для красоты используем оператор табуляции \t
print ("Int -->")
print ("\tInt.min = \(Int.min)\n\tInt.max = \(Int.max)")
print ("UInt -->")
print ("\tUInt.min = \(UInt.min)\n\tInt.max = \(UInt.max)")
print ("Int8 -->")
print ("\tInt8.min = \(Int8.min)\n\tInt.max = \(Int8.max)")

//Block 2 - Int_Double_Float
//15 чисиле после запятой
//до 6 чисел после запятой


//Пример на числе Пи
print ("\n")
print ("HW Block №2")
print ("Проверка величин чисел в разных типах")
print ("Пример основан на примере числа \"Пи\" = 3.14")
let a = 3
let b : Float = 3.14159
let c = 3.1415926535

//FIX:( не хватает комментария)
//Приведение типов
let aTest = a + Int(b) + Int(c)
let bTest = Float(a) + b + Float(c)
let cTest = Double(a) + Double(b) + c

print ("\nВыполняемое условие:")  //Заголовок

//Условие
if cTest <= Double(bTest) {
    print ("Значение в Double меньше Flout")
} else if cTest > Double(bTest) {
    print ("Значение в Double больше Flout")
} else {
    //FIX: Строка 50 ничего не выполняет, занимает память под строку
    //("Int - победил")
    print("Int - победил")
}

//Пример вывода сообщения из переменной
let messageToPrint = "Значение в Double меньше Flout"
print ("\(messageToPrint)")

//test 2 - types(... ... ...)
let aa = 3
let bb : Float = 4.2
let cc = 5.3

//let aTest2 = aa + Int(bb) + Int(cc)
//let bTest2 = Float(aa) + bb + Float(cc)
//let cTest2 = Double(aa) + Double(bb) + cc

let aTest2 = Int(Double(aa) + Double(bb) + cc)
let bTest2 = Float(aa) + (bb) + Float(cc)
let cTest2 = Double(aa) + Double(bb) + cc




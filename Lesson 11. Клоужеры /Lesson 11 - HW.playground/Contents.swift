//MARK: пример
//FIXME: пример
//TODO: пример

//MARK: -Block 1
//print ("________Block 1_________________________________________________________________")
//
//func firstFunc (a: () -> ()) {
//    for i in 0...10 {
//        print ("i = \(i)")
//    }
//    a()
//}
//
//firstFunc {
//    print ("clouser - капут")
//}


//MARK: -Block 2
//print ("________Block 2_________________________________________________________________")
//
//
//let names = ["Kate", "Jane", "Qsu", "Nik", "Fil", "Sten", "Alfred", "Henkok", "Bivis", "Jon"]
//let numbers = [4, 2, 7, 23, 14, 85, 43, 21, 99, 16]
//
//func nameSort(s1: String, s2: String) -> Bool {
//    return s1 > s2
//}
//
//let revesedName = names.sorted(by: nameSort)
//print (revesedName)
//
//func numSort(a1: Int, a2: Int) -> Bool {
//    return a1 > a2
//}
//
//let revNum = numbers.sorted(by: numSort)
//print (revNum)
//

//MARK: -Block 3
//print ("________Block 3_________________________________________________________________\n\n")
//
//
//let numbers = [4, 2, 7, 23, 14, 85, 43, 21, 99, 16]
//
//func firstFunc (array: [Int], clouser: (Int?, Int) -> Bool) -> Int {
//    var a : Int?
//    for i in array {
//        if clouser (a, i) {
//            a = i
//        }
//    }
//    return a ?? 0
//}
//
//let minTest = firstFunc(array: numbers) { (a, i) -> Bool in
//        a == nil || i < a!
//    }
//
//let maxTest = firstFunc(array: numbers) { (a, i) -> Bool in
//        a == nil || i > a!
//    }
//
//print ("Проверяемый массив - \(numbers)\n")
//print ("минимальное значение в массиве - \(minTest)")
//print ("максимальное значение в массиве - \(maxTest)")
//

//MARK: -Block 4
//print ("________Block 4_________________________________________________________________\n\n")
//
//
//let alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
//
//let abc = ["c", "g", "h", "x", "k", "l", "s", "m", "f"]
//
//func abcMinMax (array: [String], clouser: (String?, String) -> Bool) -> String {
//    var test : String?
//    for i in array {
//        if clouser (test, i) {
//            test = i
//        }
//    }
//    return test ?? ""
//}
//
//let abcMin = abcMinMax(array: abc) { (test, i) -> Bool in
//    return test == nil || i > test!
//}
//let abcMax = abcMinMax(array: abc) { (test, i) -> Bool in
//    return test == nil || i < test!
//}
//
//print (abcMin)
//print (abcMax)
//
   

//MARK: -Block 5
//print ("________Block 5_________________________________________________________________\n\n")
//
//
//let text = "лв№%№:тп235235352твыт35№дль:№: в:3ытТ№!ИЛОИО;,;.,ывап ореуцеыв а ;ыва;.пывпывпвыц3:%аыыв аоыапвыр;,.%л ывО,..ИРОУРЦИФА ОЛ Цло"

// создание массива через функцию принимающаю строку и клоужер сортирующий массив через функцию строка 118
//func sortArray (text: String, clouser: ([String]) -> ([String])) -> [String] {
//    var charArray = [String]()
//    for i in text {
//        charArray.append(String(i))
//    }
//    let tempArray = clouser(charArray)
//    
//    return tempArray
//}
//
//func classifier (text: String) -> Int {
//    switch text.lowercased() {
//    case "а", "у", "о", "ы", "и", "э", "я", "ю", "ё", "е":
//        return 0
//    case "а"..."я":
//        return 1
//    case "0"..."9":
//        return 2
//    default:
//        return 3
//    }
//}
//
//let assArray = sortArray(text: text) { (charArray) -> ([String]) in
//    let tempArray = charArray.sorted { (a, b) -> Bool in
//        if classifier(text: a) < classifier(text: b) {
//            return true
//        } else if classifier(text: a) == classifier(text: b) {
//            return a.lowercased() < b.lowercased()
//        } else {
//            return false
//        }
//    }
//    return tempArray
//}
//
//print (assArray)


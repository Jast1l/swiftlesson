import UIKit
import Foundation

//1. Создайте тип Комната. У комнаты есть размеры W на H. И создайте тип Персонаж. У негоесть координата в комнате X и Y. Реализуйте функцию, которая красивенько текстом будет показывать положение персонажа в комнате
//
//2. Персонажу добавьте метод идти, который принимает энумчик лево, право, верх, вниз
//Этот метод должен передвигать персонажа. Реализуйте правило что персонаж не должен покинуть пределы комнаты. Подвигайте персонажа и покажите это графически
//
//3. Создать тип Ящик. У ящика также есть координата в комнате X и Y. Ящик также не может покидать пределы комнаты и ящик также должен быть распечатан вместе с персонажем в функции печати.
//
//4. Теперь самое интересное, персонаж может двигать ящик, если он стоит на том месте, куда персонаж хочет попасть. Главное что ни один объект не может покинуть пределы комнаты. Подвигайте ящик :)
//
//5. Добавьте точку в комнате, куда надо ящик передвинуть и двигайте :)
//
//Для суперменов: можете добавить массив ящиков и можете сделать консольное приложение

//FIXME: глобальные переменные
let SetupStepHuman = 1
let SetupMoveBoxByHuman = 1
let SetupBoxBounceBack = 1
let PictureBoard = "\u{2B1C}"
let PictureHuman = "\u{1F468}"
let PictureTarget = "\u{274C}"
let PictureBox = "\u{1F5C4}"
let PictureTargetComplite = "\u{2705}"

//FIXME: Энум выбора направления Движения
enum Move {
    case Right
    case Left
    case Up
    case Down
}

//FIXME: Стуктура - тип Комната
struct Room {
    var length : Int  // H
    var width : Int   // W
}

//FIXME:  Структура - тип Ящик
struct Box {
    static var picture = PictureBox
    
    var x : Int
    var y : Int
}

//FIXME: Структура - тип Цель
struct Target {
    static var pictureCurent = PictureTarget
    
    var x : Int
    var y : Int
    
}

//FIXME: Класс - тип Человек
class Human {
    static let picture = PictureHuman
    
    var x : Int
    var y : Int
    
    //FIXME: Метод - выбора Движения для экземпляра класса
    func move (to: Move) {
        switch to {
        case .Right :
            self.x += SetupStepHuman                                     // движение человека
            if needToMoveBox(positionHuman: human, positionBox: box) {   // движение коробка
                box.x += SetupMoveBoxByHuman
            }
            if self.x > room.length {                                    // проверка комнаты - возврат человека
                print ("Отмена хода \"Вправо\", Вы попытались покинуть игровую площадку")
                self.x -= SetupStepHuman
                if box.x == self.x && box.y == self.y {                  // проверка комнаты - прыжок коробки
                }
            }
            if box.x > room.length {
                print ("При попытке разбить я щик об стену он отскочил на \(SetupBoxBounceBack) назад влево")
                box.x -= (SetupBoxBounceBack + 1)
            }
            
        case .Left :
            self.x -= SetupStepHuman
            if needToMoveBox(positionHuman: human, positionBox: box) {
                box.x -= SetupMoveBoxByHuman
            }
            if self.x <= 0 {
                print ("Отмена хода \"Влево\", Вы попытались покинуть игровую площадку")
                self.x += SetupStepHuman
            }
            if box.x <= 0 {
                ("При попытке разбить я щик об стену он отскочил на \(SetupBoxBounceBack) назад вправо")
                box.x += (SetupBoxBounceBack + 1)
            }
            
        case .Down :
            self.y += SetupStepHuman
            if needToMoveBox(positionHuman: human, positionBox: box) {
                box.y += SetupMoveBoxByHuman
            }
            if self.y > room.width {
                print ("Отмена хода \"Вниз\", Вы попытались покинуть игровую площадку")
                self.y -= SetupStepHuman
            }
            if box.y > room.width {
                print ("При попытке разбить я щик об стену он отскочил на \(SetupBoxBounceBack) назад вверх")
                box.y -= (SetupBoxBounceBack + 1)
            }
            
        case .Up :
            self.y -= SetupStepHuman
            if needToMoveBox(positionHuman: human, positionBox: box) {
                box.y -= SetupMoveBoxByHuman
            }
            if self.y <= 0 {
                print ("Отмена хода \"Вверх\", Вы попытались покинуть игровую площадку")
                self.y += SetupStepHuman
            }
            if box.y <= 0 {
                ("При попытке разбить я щик об стену он отскочил на \(SetupBoxBounceBack) назад вниз")
                box.y += (SetupBoxBounceBack + 1)
            }
        }
        if target.x == box.x && target.y == box.y {
            print ("Цель игры достигнута - ящик на заданной отметке")
            Box.picture = PictureTargetComplite
        } else {
            Box.picture = PictureBox
        }
    }
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
}


func rangeToArray (range: Int) -> [Int] {
    var tempArray = [Int]()
    for num in 1...range {
        tempArray.append(num)
    }
    
    return tempArray
}

func needToMoveBox (positionHuman: Human, positionBox: Box) -> Bool {
    return positionBox.x == positionHuman.x && positionBox.y == positionHuman.y ? true : false
}


func arraysToString (lenght: [Int], width: [Int], arrayToPrint: [String: String]) -> String {
    var finalLine = ""
    
    var firstTitle = ""
    var secondTitle = ""
    
    for num in lenght {
        if num <= 9 {
            firstTitle += " 0 "
            secondTitle += " \(num - (Int(num/10))*10) "
        } else if (num / 10) < 9 {
            firstTitle += " \(Int(num/10)) "
            secondTitle += " \(num - (Int(num/10))*10) "
        }
    }
    
    finalLine = "\(firstTitle)\n\(secondTitle)"
    
    var lineIndex = 0
    
    for num in width {
        var line = ""
        lineIndex += 1
        
        for value in lenght {
            line += "\(arrayToPrint["\(value)\(num)"]!) "
        }
        finalLine += "\n\(line) \(lineIndex)"
    }
    
    return finalLine
}

func roomView (room: Room, human: Human, box: Box) -> String {
    
    let lenght = rangeToArray(range: room.length)
    let width = rangeToArray(range: room.width)
    var roomPrint = [String: String]()
    
    for num in lenght {
        for num2 in width {
            if human.x == num && human.y == num2 {
                roomPrint["\(num)\(num2)"] = Human.picture
            } else if box.x == num && box.y == num2 {
                roomPrint["\(num)\(num2)"] = Box.picture
            } else if target.x == num && target.y == num2 {
                roomPrint["\(num)\(num2)"] = Target.pictureCurent
            } else {
                roomPrint["\(num)\(num2)"] = PictureBoard
            }
        }
    }
    return arraysToString(lenght: lenght, width: width, arrayToPrint: roomPrint)
}

var room = Room(length: 10, width: 10)
var human = Human(x: 3, y: 4)
var box = Box(x: 5, y: 5)
var target = Target(x: 4, y: 1)

box = Box(x: 4, y: 2)
target = Target(x: 6, y: 4)
human = Human(x:4, y: 5)

//human.move(to: .Right)
//human.move(to: .Up)
//human.move(to: .Up)
//human.move(to: .Up)
//human.move(to: .Up)
//human.move(to: .Left)
//human.move(to: .Up)
//human.move(to: .Down)
//human.move(to: .Down)
//human.move(to: .Left)
//human.move(to: .Down)
//human.move(to: .Right)
//human.move(to: .Right)
//human.move(to: .Right)
//human.move(to: .Right)


print (roomView(room: room, human: human, box: box))

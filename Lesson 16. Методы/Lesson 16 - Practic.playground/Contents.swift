import UIKit
import Foundation

struct Point {     // в классах не нужкн мутант
    var x : Int
    var y : Int
    
    
    mutating func moveByX(_ x: Int, andY y: Int ) {   // в методе первый параметр всегда идет внутренний (локальный), второй внешний! к этому при необходимости можно придти использую скрытый параметр "_"!!!
        let tempX = x
        let tempY = y
        self.x += tempX
        self.y += tempY
    }

//FIXME: пример более сложной интерпритации изменения self типа
//
//    mutating func moveByX(_ x: Int, andY y: Int ) {
//        self = Point(x: self.x + x, y: self.y + y)
//    }
//
}


//FIXME: начало рассматриваемой функции как примера
func move(point: Point, byX x: Int, andY y: Int ) -> Point {
    var tempPoint = point
    tempPoint.x += x
    tempPoint.y += y
    
    return tempPoint
}

var p1 = Point(x: 1, y: 1)
var p2 = Point(x: 2, y: 2)


//p = move(point: p, byX: 2, andY: 4)
//FIXME: конец рассматриваемой функции как примера

p1.moveByX(2, andY: 4)

p1.moveByX(5, andY: 7)   // в методе первый параметр всегда идет внутренний (локальный), второй внешний!




print (p1)
print (p2)

enum Color {
    case White
    case Black
    
    static func numberOfElements () -> Int {
        return 2
        print (self)
    }
    
    mutating func invert() {
        
//        if self == .White {
//            self = .Black
//        } else {
//            self = .White
//        }
        
        self = self == .White ? .Black : .White
        print ("тест для пропертис")
    }
}

var c = Color.White
c

print (c)

c.invert()

print (c)

print (Color.numberOfElements())




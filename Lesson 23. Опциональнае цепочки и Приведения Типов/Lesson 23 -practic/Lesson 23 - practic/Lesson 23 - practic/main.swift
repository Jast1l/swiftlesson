import Foundation

class Adress {
    var street = "Deribasovskay"
    var number = "1"
    var city = "Odessa"
    var country = "Ukraine"
}

struct Garage {
    var size = 2
}

class House {
    var rooms = 1
    var adress = Adress()
    var garage : Garage? = Garage()
}

class Car {
    var model = "Zaporojec"
    
    func start() {
    }
}

class Person {
    var cars : [Car]? = [Car()]
    var house : House? = House()
}

let p = Person()

p.cars?[0].model
p.house?.garage?.size



let carCount = p.cars?[0].model
let houseCount = p.house?.garage?.size

var testBinde1 = ""

if let count = p.cars?[0].model {
    testBinde1 = count
}

print (p.cars![0].model)
print (p.house!.garage!.size)

print (carCount)
print (houseCount)
print (testBinde1)

if let house = p.house {
    if let garage = house.garage {
        garage.size
    }
}

import UIKit
import Foundation
import Darwin

//  Сегодня будем строить свою небольшую социальную сеть.
//    1. Сделать класс Человек, у этого класса будут проперти Папа, Мама, Братья, Сестры (всё опционально).
//  Сделать примерно 30 человек, взять одного из них, поставить ему Папу/Маму. Папе и Маме поставить Пап/Мам/Братьев/Сестер. Получится большое дерево иерархии.
//  Посчитать, сколько у этого человека двоюродных Братьев, Сестёр, Теть, Дядь, итд

//    2. Все сестры, матери,... должны быть класса Женщина, Папы, братья,... класса Мужчины.
//  У Мужчин сделать метод Двигать_диван, у Женщин Дать_указание (двигать_диван). Всё должно работать как и ранее.
//  Всех этих людей положить в массив Семья, пройти по массиву посчитать количество Мужчин и Женщин, для каждого Мужчины вызвать метод Двигать_диван, для каждой женщины Дать_указание.

//    3. Расширить класс человек, у него будет проперти Домашние_животные. Животные могут быть разные (попугаи, кошки, собаки...) их может быть несколько, может и не быть вообще.
//  Раздать некоторым людям домашних животных. Пройти по всему массиву людей. Проверить каждого человека на наличие питомца, если такой есть - добавлять всех животных в массив животных. Посчитать сколько каких животных в этом массиве.
//  Вся эта живность должна быть унаследована от класса Животные. У всех животных должен быть метод Издать_звук(крик) и у каждого дочернего класса этот метод переопределён на свой, т.е. каждое животное издаёт свой звук.
//  Когда проходим по массиву животных, каждый представитель вида животных должен издать свой звук.
//
//  Обязательно используем в заданиях Optional chaining и Type casting

//MARK: - Block 1

let MaxHomePetOnHuman = 5

class Human {
    var name : String
    var lastName : String
    
    var mother : Woman?
    var father : Man?
    var brather : [Man]?
    var sister : [Woman]?
    
    var homePet : [Animal]?
    
    static var humanCount = 0
    static var familyArray = [Human]()
    static var woman = 0
    static var man = 0
    static var randomPet = ["Есть любимый питомец", "Нет любимого питомца"]
    
    func doIt () -> String {
        return ""
    }
    
    private func addPapMam () -> () {
        if sister != nil {
            for sis in sister! {
                sis.father = self.father
                sis.mother = self.mother
                sis.sister = self.sister
            }
        }
        if brather != nil {
            for bro in brather! {
                bro.father = self.father
                bro.mother = self.mother
                bro.brather = self.brather
            }
            
        }
    }
    
    func addFamily (mother: Woman?, father: Man?, sister: [Woman]?, brather: [Man]?) {
        self.mother = mother
        self.father = father
        self.sister = sister
        self.brather = brather
        
        addPapMam()
    }
    
    lazy var printFamily : () -> String = {
        [self] in
        
        var baseResultString = ""
        
        var uncle = 0
        var aunt = 0
        var cousinGirl = 0
        var cousinBoy = 0
        
        func gender () -> String {
            if self is Man {
                return " - сын \(self.mother!.name) и \(self.father!.name) с фамилией \(self.father!.lastName). Его всегда поддерживали "
            } else {
                return " - дочь \(self.mother!.name) и \(self.father!.name) с фамилией \(self.father!.lastName). Ее всегда поддерживали "
            }
        }
        
        func textCountSis () -> String {
            if self.sister != nil {
                switch self.sister!.count {
                case var test where test == 1 :
                    return "сестра"
                case var test where test > 1 && test <= 4 :
                    return "сестры"
                default :
                    return "сестер"
                }
            } else {
                return ""
            }
        }
        
        func textCountBro () -> String {
            if self.brather != nil {
                switch self.brather!.count {
                case var test where test == 1 :
                    return "брат"
                case var test where test > 1 && test <= 4 :
                    return "брата"
                default :
                    return "братьев"
                }
            } else {
                return ""
            }
        }
        
        func addAuntAndUnckle () -> String {
            var uncle = 0
            var aunt = 0
            
            var resultString = ""
            
            if aunt > 0 && uncle > 0 {
                var resultString = ", а также "
            }
            
            if self.mother?.sister != nil {
                aunt += self.mother!.sister!.count
            }
            if self.father?.sister != nil {
                aunt += self.father!.sister!.count
            }
            
            if aunt > 0 {
                resultString += "\(aunt)"
                switch aunt {
                case var test where test <= 4 :
                    resultString += " тети"
                default :
                    resultString += " теть"
                }
            }
            
            
            if self.mother?.brather != nil {
                uncle += self.mother!.brather!.count
            }
            if self.father?.brather != nil {
                uncle += self.father!.brather!.count
            }
            
            if aunt > 0 && uncle > 0 {
                resultString += " и "
            }
            
            if uncle > 0 {
                resultString += "\(uncle)"
                switch uncle {
                case var test where test <= 4 :
                    resultString += " дяди"
                default :
                    resultString += " дядь"
                }
            }
            return resultString
        }
        
        func addCousinBandG () -> String {
            var cousinGirl = 0
            var cousinBoy = 0
            
            var resultString2 = ""
            
            if cousinGirl > 0 && cousinBoy > 0 {
                resultString2 = ", а еще "
            }
            
            if self.mother != nil {  //запрос наличия мамы
                if self.mother?.sister != nil {    //запрос наличия сестер у мамы
                    for sisMam in self.mother!.sister! {   //пробег по сесрам матери
                        for human in Human.familyArray {   //пробег по общему масиву людей
                            if let checkSisMam = human as? Woman {   //вычленении женского пола
                                if sisMam.name == checkSisMam.mother?.name {  //сравнение имени одной из сестер матери с именем матери ЧЕЛОВЕКА из массива
                                    cousinGirl += 1
                                }
                            }
                        }
                    }
                }
                if self.mother?.brather != nil {  //запрос наличия братьев у мамы
                    for broMam in self.mother!.brather! {   //пробег по братьям матери
                        for human in Human.familyArray {   //пробег по общему масиву людей
                            if let checkBroMam = human as? Man {   //вычленении мужского пола
                                if broMam.name == checkBroMam.mother?.name {  //сравнение имени одного из братьев матери с именем матери ЧЕЛОВЕКА из массива
                                    cousinGirl += 1
                                }
                            }
                        }
                    }
                }
            }
            
            if cousinGirl > 0 {
                resultString2 += "\(cousinGirl)"
                switch cousinGirl {
                case var test where test == 1 :
                    resultString2 += " кузина"
                case var test where test > 1 && test <= 4 :
                    resultString2 += " кузины"
                default :
                    resultString2 += " кузин"
                }
            }
            
            if self.father != nil {
                if self.father?.sister != nil {
                    for sisPap in self.father!.sister! {
                        for human in Human.familyArray {
                            if let checkSisPap = human as? Woman {
                                if sisPap.name == checkSisPap.father?.name {
                                    cousinBoy += 1
                                }
                            }
                        }
                    }
                }
                if self.father?.brather != nil {
                    for broPap in self.father!.brather! {
                        for human in Human.familyArray {
                            if let checkBroPap = human as? Man {
                                if broPap.name == checkBroPap.father?.name {
                                    cousinBoy += 1
                                }
                            }
                        }
                    }
                }
            }
            
            if cousinGirl > 0 && cousinBoy > 0 {
                resultString2 += " и "
            }
            
            if cousinBoy > 0 {
                resultString2 += "\(cousinBoy)"
                switch cousinBoy {
                case var test where test == 1 :
                    resultString2 += " кузен"
                case var test where test > 1 && test <= 4 :
                    resultString2 += " кузена"
                default :
                    resultString2 += " кузинов"
                }
            }
            return resultString2
        }
        
        if self.sister != nil && self.brather != nil {
            baseResultString = ("\(self.name) \(self.lastName) \(gender()) \(self.sister!.count) \(textCountSis()) и \(self.brather!.count) \(textCountBro()) \(addAuntAndUnckle()) \(addCousinBandG())")
        } else {
            if self.sister != nil {
                baseResultString = ("\(self.name) \(self.lastName) \(gender()) \(self.sister!.count) \(textCountSis()) \(addAuntAndUnckle()) \(addCousinBandG())")
            }
            if self.brather != nil {
                baseResultString = ("\(self.name) \(self.lastName) \(gender()) \(self.brather!.count) \(textCountBro()) \(addAuntAndUnckle()) \(addCousinBandG())")
            }
        }
        
        if self.brather == nil && self.sister == nil {
            baseResultString = ("\(self.name) \(self.lastName) не имеет других родственников кроме папы и мамы.")
        }
        return baseResultString
    }
    
    private func randomPets () -> [Animal]? {
        
        var numberOfAnimal = 0
        var arrayPets = [Animal]()
        let selectPet = [Cat() , Dog(), Parrot()]       // какие бывают животные
        
        func presenceOfAnimals () -> Bool {             //определение наличия животного
            let testAnimal = Int(arc4random_uniform(2))
            if testAnimal == 1 {
                return true
            } else {
                return false
            }
        }
        
        if presenceOfAnimals() {                        // если животное есть, то сколько их (максимум 5)
            numberOfAnimal = Int(arc4random_uniform(UInt32(MaxHomePetOnHuman)))
        }
        
        
        
        if numberOfAnimal > 0 {                         // определение животных для человека
            for _ in 1...numberOfAnimal {
                arrayPets.append(selectPet.randomElement()!)
            }
        }
        
        
        if arrayPets.isEmpty {
            return nil
        } else {
            for pet in arrayPets {
                Animal.arrayAnimal.append(pet)
                switch pet {
                case _ as Cat :
                    Animal.cat += 1
                case _ as Dog :
                    Animal.dog += 1
                case _ as Parrot :
                    Animal.parrot += 1
                default :
                    0
                }
            }
            return arrayPets
        }
    }
    
    func printHomePet () -> String {
        var resultString = ""
        var textCat = ""
        var textDog = ""
        var textParrot = ""
        
        var textPet = [String]()
        
        var cat = 0
        var dog = 0
        var parrot = 0
        
        if self.homePet != nil {
            resultString = "Есть домашние животные:"
            for pet in self.homePet! {
                switch pet {
                case is Cat :
                    cat += 1
                case is Dog :
                    dog += 1
                case is Parrot :
                    parrot += 1
                default :
                    0
                }
            }
            
            if cat != 0 {
                switch cat {
                case 1 :
                    textCat = "кошка"
                case 2...4 :
                    textCat = "кошки"
                default :
                    textCat = "кошек"
                }
                textPet.append("\(cat) \(textCat)")
            }
            
            if dog != 0 {
                switch dog {
                case 1 :
                    textDog = "собака"
                case 2...4 :
                    textDog = "собаки"
                default :
                    textDog = "собак"
                }
                textPet.append("\(dog) \(textDog)")
            }
            
            if parrot != 0 {
                switch parrot {
                case 1 :
                    textParrot = "попугай"
                case 2...4 :
                    textParrot = "попугая"
                default :
                    textParrot = "попугаев"
                }
                textPet.append("\(parrot) \(textParrot)")
            }
            
            if textPet.count == 3 {
                resultString += " \(textPet[0]), \(textPet[1]), а также \(textPet[2])"
            }
            if textPet.count == 2 {
                resultString += " \(textPet[0]) и \(textPet[1])"
            }
            if textPet.count == 1{
                resultString += " \(textPet[0])"
            }
        } else {
            resultString = "Нет домашних животных"
        }
        return resultString
    }
    
    init (name: String, lastName: String) {
        self.name = name
        self.lastName = lastName
        self.homePet = randomPets()
        
        
        Human.humanCount += 1
        Human.familyArray.append(self)
        
        if self is Man {
            Human.man += 1
        } else if self is Woman {
            Human.woman += 1
        }
    }
}


class Woman : Human {
    override func doIt () -> String {
        return ("Может дать указания")
    }
}

class Man : Human {
    override func doIt () -> String {
        return ("Если его попросят, может подвинуть диван")
    }
}

class Animal {
    
    static var cat = 0
    static var dog = 0
    static var parrot = 0
    
    static var arrayAnimal = [Animal]()
    
    static var allPet = Animal.cat + Animal.dog + Animal.parrot
    
    func voice() -> String {
        return ""
    }
}

class Parrot : Animal {
    override func voice() -> String  {
        return "tweet"
    }
}

class Cat : Animal {
    override func voice() -> String  {
        return "meow"
    }
}

class Dog : Animal {
    override func voice() -> String  {
        return "woof"
    }
}


var human1 = Man(name: "Man1", lastName: "Mans1")               // 1 - колено
var human2 = Woman(name: "Woman1", lastName: "Woms1")           // 1 - колено
var human3 = Man(name: "Mannn3", lastName: "Mannns3")                       // 2 - колено
var human4 = Woman(name: "Womaannn4", lastName: "Womaannns4")               // 2 - колено
var human5 = Woman(name: "Womaannn5", lastName: "Womaannns5")               // 2 - колено
var human6 = Woman(name: "Womaannn6", lastName: "Womaannns6")               // 2 - колено
var human7 = Woman(name: "WWWWWoman7", lastName: "WWWWWomans7")                         // 3 - колено
var human8 = Woman(name: "WWWWWoman8", lastName: "WWWWWomans8")                         // 3 - колено
var human9 = Man(name: "Mannn9", lastName: "Mannns9")                                               // 4 - колено
var human10 = Man(name: "Mannn10", lastName: "Mannns10")                                            // 4 - колено
var human11 = Woman(name: "Woman11", lastName: "Womans11")                                          // 4 - колено
var human12 = Woman(name: "Woman12", lastName: "Womans12")                                          // 4 - колено
var human13 = Woman(name: "WWWWWoman13", lastName: "WWWWWomans13")                                  // 4 - колено
var human14 = Woman(name: "WWWWWoman14", lastName: "WWWWWomans14")                                  // 4 - колено
var human15 = Man(name: "WWWomaNNN15", lastName: "WWWomaNNNs15")                                    // 4 - колено
var human16 = Man(name: "WWWomaNNN16", lastName: "WWWomaNNNs15")                                    // 4 - колено
var human17 = Man(name: "MaaaNNN17", lastName: "MaaaNNNs17")                            // 3 - колено
var human18 = Man(name: "MaaaNNN18", lastName: "MaaaNNNs18")                            // 3 - колено
var human19 = Woman(name: "WomaNNN19", lastName: "WomaNNNs19")                          // 3 - колено
var human20 = Woman(name: "WomaNNN20", lastName: "WomaNNNs20")                          // 3 - колено
var human21 = Man(name: "MMMMMan21", lastName: "MMMMMans21")                            // 3 - колено
var human22 = Man(name: "MMMaaaNNN22", lastName: "MMMaaaNNNs22")                        // 3 - колено
var human23 = Woman(name: "WWWomaNNN23", lastName: "WWWomaNNNs23")                      // 3 - колено
var human24 = Woman(name: "WWWomaNNN24", lastName: "WWWomaNNNs24")                      // 3 - колено
var human25 = Man(name: "MMMan25", lastName: "MMMans25")                    // 2 - колено
var human26 = Man(name: "MMMan26", lastName: "MMMans26")                    // 2 - колено
var human27 = Man(name: "MMMan27", lastName: "MMMans27")                    // 2 - колено
var human28 = Man(name: "Maaan28", lastName: "Maaans28")                    // 2 - колено
var human29 = Man(name: "Man2", lastName: "Mans2")             // 1 - колено
var human30 = Woman(name: "Woman2", lastName: "Woms2")         // 1 - колено


human3.addFamily(mother: human2, father: human1, sister: [human4, human5, human6], brather: nil)
human28.addFamily(mother: human30, father: human29, sister: nil, brather: [human27, human26, human25])
human7.addFamily(mother: human4, father: human25, sister: [human23, human24], brather: [human22])
human21.addFamily(mother: human5, father: human26, sister: nil, brather: nil)
human8.addFamily(mother: human6, father: human27, sister: [human19, human20], brather: [human17, human18])
human9.addFamily(mother: human8, father: human21, sister: [human11, human12], brather: [human10])
human13.addFamily(mother: human19, father: human22, sister: [human14], brather: [human15, human16])


var family = Human.familyArray

print ("В семейной древе \(family.count) человек. Из них \(Human.man) женщин и \(Human.woman) мужчин\n")

var  index = 1

for human in family {
    print ("\(index). \(human.printFamily())")
    print ("\t\(human.doIt())")
    print ("\t\(human.printHomePet())")
    print ("\n")
    index += 1
}

var animal = Animal.arrayAnimal
var indexAnimal = 0

print ("У представителей семейного древа есть \(Animal.allPet) домашних животных, а именно: \(Animal.cat) кошек, \(Animal.dog) собак, \(Animal.parrot) попугаев")

for pet in animal {
    indexAnimal += 1
    print ("\(indexAnimal). \(pet.voice())")
}

//ура!!! я это сделал!!!

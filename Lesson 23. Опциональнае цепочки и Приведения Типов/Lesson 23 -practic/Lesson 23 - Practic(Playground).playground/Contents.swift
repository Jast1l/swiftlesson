import Foundation

class Adress {
    var street = "Deribasovskay"
    var number = "1"
    var city = "Odessa"
    var country = "Ukraine"
}

struct Garage {
    var size = 2
}

class House {
    var rooms = 1
    var adress = Adress()
    var garage : Garage? = Garage()
}

class Car {
    var model = "Zaporojec"
    
    func start() {
    }
}

class Person {
    var cars : [Car]? = [Car()]
    var house : House? = House()
}

let p = Person()

p.cars?[0].model
p.house?.garage?.size



let carCount = p.cars?[0].model
let houseCount = p.house?.garage?.size

var testBinde1 = ""

if let model = p.cars?[0].model {
    testBinde1 = model
}

if let house = p.house {
    if let garage = house.garage {
        garage.size
    }
}

//крутой пример
if (p.house?.garage?.size = 3) != nil {    // уставнавливаем новое значение!!! (3)
    print ("Upgrade!")
} else {
    print ("False")
}
p.house?.garage?.size
//

if p.cars?[0].start() != nil {
    print ("Start")
} else {
    print ("failure!")
}

print (p.cars![0].model)
print (p.house!.garage!.size)

print (carCount)
print (houseCount)
print (testBinde1)


//Определение классовой иерархии для приведения типов

class MediaItem {
    var name: String
    init(name: String) {
        self.name = name
    }
}

class Movie: MediaItem {
    var director: String
    init(name: String, director: String) {
        self.director = director
        super.init(name: name)
    }
}
 
class Song: MediaItem {
    var artist: String
    init(name: String, artist: String) {
        self.artist = artist
        super.init(name: name)
    }
}

let library = [
    Movie(name: "Casablanca", director: "Michael Curtiz"),
    Song(name: "Blue Suede Shoes", artist: "Elvis Presley"),
    Movie(name: "Citizen Kane", director: "Orson Welles"),
    Song(name: "The One And Only", artist: "Chesney Hawkes"),
    Song(name: "Never Gonna Give You Up", artist: "Rick Astley")
]
// тип "library" выведен как [MediaItem]


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// групировка наследников

class Symbol {
}

class A : Symbol {
    func aa() {}
}

class B : Symbol{
    func bb() {}
}

let array : [Symbol] = [A(), B(), Symbol(), A(), A(), B()]
var aCount = 0
var bCount = 0
var sCount = 0

for value in array {        //is - ПРОВЕРКА
    switch value {
    case is A :             //is  оператор булевого значения на проверку соотвествия типу
        aCount += 1
        
        let a = value as! A  // ПРИМЕР зоны использовани - ПОЯСНЕНИЕ НИЖЕ
        
    case is B :
        bCount += 1
    case is Symbol :
        sCount += 1
    }
    
//    if value is A {
//        aCount += 1
//    } else if value is B {
//        bCount += 1
//    } else {
//        sCount += 1
//    }
    
    if let a = value as? A {    //приводит к типу и позволяет далле использовать свойства (?) - опционал для учета не всех перечисленных в запросе типов
        a.aa()
    } else if let b = value as? B {    //as - ПРИСВОЕНИЕ
        b.bb()
    }
    
//    if let a = value as! A {   //былабы работоспособны при налии массива [A(), A(), A(), A(), A(), A(), A()]
//        a.aa()
//    }
    
    
}

aCount
bCount
sCount

//test to repository


//AnyObject
//Any


let arrayObject : [AnyObject] = [A(), B(), Symbol(), A(), A(), B(), NSObject()] //import Foundation

let arrayAll : [Any] = [A(), B(), Symbol(), A(), A(), B(), NSObject(), "a", 5, {() -> () in return}]




//Проверка типа  (is)

var movieCount = 0
var songCount = 0
 
for item in library {
    if item is Movie {
        movieCount += 1
    } else if item is Song {
        songCount += 1
    }
}

print("В Media библиотеке содержится \(movieCount) фильма и \(songCount) песни")
// Выведет "В Media библиотеке содержится 2 фильма и 3 песни"


//Понижающее приведение  (as? или as!)

for item in library {
    if let movie = item as? Movie {
        print("Movie: \(movie.name), dir. \(movie.director)")
    } else if let song = item as? Song {
        print("Song: \(song.name), by \(song.artist)")
    }
}
 
// Movie: Casablanca, dir. Michael Curtiz
// Song: Blue Suede Shoes, by Elvis Presley
// Movie: Citizen Kane, dir. Orson Welles
// Song: The One And Only, by Chesney Hawkes
// Song: Never Gonna Give You Up, by Rick Astley


//Приведение типов для Any и AnyObject

var things = [Any]()
 
things.append(0)
things.append(0.0)
things.append(42)
things.append(3.14159)
things.append("hello")
things.append((3.0, 5.0))
things.append(Movie(name: "Ghostbusters", director: "Ivan Reitman"))
things.append({ (name: String) -> String in "Hello, \(name)" })

for thing in things {
    switch thing {
    case 0 as Int:
        print("zero as an Int")
    case 0 as Double:
        print("zero as a Double")
    case let someInt as Int:
        print("an integer value of \(someInt)")
    case let someDouble as Double where someDouble > 0:
        print("a positive double value of \(someDouble)")
    case is Double:
        print("some other double value that I don't want to print")
    case let someString as String:
        print("a string value of \"\(someString)\"")
    case let (x, y) as (Double, Double):
        print("an (x, y) point at \(x), \(y)")
    case let movie as Movie:
        print("a movie called \(movie.name), dir. \(movie.director)")
    case let stringConverter as (String) -> String:
        print(stringConverter("Michael"))
    default:
        print("something else")
    }
}
 
// zero as an Int
// zero as a Double
// an integer value of 42
// a positive double value of 3.14159
// a string value of "hello"
// an (x, y) point at 3.0, 5.0
// a movie called Ghostbusters, dir. Ivan Reitman
// Hello, Michael

let optionalNumber: Int? = 3
things.append(optionalNumber)        // Warning
things.append(optionalNumber as Any) // No warning

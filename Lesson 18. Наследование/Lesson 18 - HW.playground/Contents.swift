import UIKit
import Foundation

//1. У нас есть базовый клас "Артист" и у него есть имя и фамилия. И есть метод "Выступление". У каждого артиста должно быть свое выступление: танцор танцует, певец поет и тд. А для художника, что бы вы не пытались ставить, пусть он ставит что-то свое (пусть меняет имя на свое артистическое). Когда вызываем метод "выступление" показать в консоле имя и фамилию артиста и собственно само выступление.
//Полиморфизм используем для артистов. Положить их всех в массив, пройтись по нему и вызвать их метод "выступление"

//2. Создать базовый клас "транспортное средство" и добавить три разных проперти: скорость, вместимость и стоимость одной перевозки (computed). Создайте несколько дочерних класов и переопределите их компютед проперти у всех. Создайте класс самолет, корабль, вертолет, машина и у каждого по одному объекту. В компютед пропертис каждого класса напишите свои значения скорости, вместимости, стоимости перевозки. + у вас должен быть свой метод который считает сколько уйдет денег и времени что бы перевести из пункта А в пункт В определенное количество людей с использованимем наших транспортных средств. Вывести в кольсоль результат (как быстро сможем перевести, стоимость, количество перевозок).
//Используем полиморфизм

//3. Есть 5 классов: люди, крокодилы, обезьяны, собаки, жирафы. (в этом задании вы будете создавать не дочерние классы, а родительские и ваша задача создать родительский таким образом, что бы группировать эти 5).
//- Создайте по пару объектов каждого класса.
//- Посчитайте присмыкающихся (создайте масив, поместите туда присмыкающихся и скажите сколько в нем объектов)
//- Сколько четвероногих?
//- Сколько здесь животных?
//- Сколько живых существ?

//MARK: -Block 1

//class Artist {
//
//    var name : String = ""
//    var lastName : String = ""
//
//    var fullName : String {
//        return name + " " + lastName
//    }
//
//    func Perfomance () -> String {
//        return "Perfomans"
//    }
//}
//
//class Singer : Artist {
//    override func Perfomance () -> String {
//        return  super.fullName + " performs solo with concert or instrumental accompaniment"
//    }
//}
//
//class Dancer : Artist {
//    override func Perfomance () -> String {
//        return  super.fullName + " specializes in hip-hop and break dance"
//    }
//}
//
//class Painter : Artist {
//    override var fullName : String {
//        return "Black Fox"
//    }
//
//    override func Perfomance () -> String {
//        return self.fullName + " draws in almost any style, but prefers cartoons"
//    }
//}
//
//let singer = Singer()
//singer.name = "Serj"
//singer.lastName = "Tankian"
//singer.fullName
//singer.Perfomance()
//
//
//let dancer = Dancer()
//dancer.name = "Jon"
//dancer.lastName = "Bim"
//dancer.fullName
//dancer.Perfomance()
//
//let painter = Painter()
//painter.name = "Jozef"
//painter.lastName = "Skott"
//painter.fullName
//painter.Perfomance()
//
//let arrayArtist = [singer, dancer, painter]
//
//for artist in arrayArtist {
//    print (artist.Perfomance())
//}


//MARK: -Block 2
//Создать базовый клас "транспортное средство" и добавить три разных проперти: скорость, вместимость и стоимость одной перевозки (computed). Создайте несколько дочерних класов и переопределите их компютед проперти у всех. Создайте класс самолет, корабль, вертолет, машина и у каждого по одному объекту. В компютед пропертис каждого класса напишите свои значения скорости, вместимости, стоимости перевозки. + у вас должен быть свой метод который считает сколько уйдет денег и времени что бы перевести из пункта А в пункт В определенное количество людей с использованимем наших транспортных средств. Вывести в кольсоль результат (как быстро сможем перевести, стоимость, количество перевозок).
//Используем полиморфизм

//class Transport {
//    var name : String = ""
//
//    var speed : Float {
//        return 0
//    }
//
//    var capacity : Int {
//        return 0
//    }
//
//    var price : Float {
//        return 0
//    }
//
//    func calculationTrip (distance: Float, human: Int) {
//        let tempHuman = Float(human)
//        let hour = Int(distance / speed)
//        let minute = Int(((distance / speed) - Float(hour)) * 60)
//        let trip = (human / capacity) + 1
//        let fullPrice = (price * tempHuman)
//
//        print ("""
//            C использованием "\(name)":
//                - \(hour) часа(ов)
//                - \(minute) минут
//                - \(trip) рейсов
//                Общая стоимость перевозки составит \(fullPrice) рублей
//        """)
//    }
//
//    init (name: String) {
//        self.name = name
//    }
//}
//
//class Plane : Transport {
//
//    override var speed : Float {
//        return 933
//    }
//
//    override var capacity : Int {
//        return 149
//    }
//
//    override var price : Float {
//        return 900
//    }
//}
//
//class Ship : Transport {
//    override var speed : Float {
//        return 74.8
//    }
//
//    override var capacity: Int {
//        return 3646
//    }
//    override var price: Float {
//        return 750
//    }
//}
//
//class Helicopter : Transport {
//    override var speed: Float {
//        return 330
//    }
//
//    override var capacity: Int {
//        return 28
//    }
//    override var price: Float {
//        return 1423
//    }
//}
//
//class Car : Transport {
//    override var speed: Float {
//        return 120
//    }
//
//    override var capacity: Int {
//        return 4
//    }
//    override var price: Float {
//        return 340
//    }
//}
//
//let plane = Plane(name: "Самолет - боинг")
//let ship = Ship(name: "Круизный лайнер")
//let helicopter = Helicopter(name: "Вертолет - гражданский")
//let car = Car(name: "Легковое такси")
//
//
//let transportArray = [plane, ship, helicopter, car]
//
//let distance = 1000
//let passengers = 400
//
//
//print ("""
//Для перевозки \(passengers) пассажирова на расстояние \(distance) км
//с использованием различных транспортынх средств потребуется:
//""")
//for item in transportArray {
//    print (item.calculationTrip(distance: Float(distance), human: passengers))
//}



//MARK: -Block3
//Есть 5 классов: люди, крокодилы, обезьяны, собаки, жирафы. (в этом задании вы будете создавать не дочерние классы, а родительские и ваша задача создать родительский таким образом, что бы группировать эти 5).
//- Создайте по пару объектов каждого класса.
//- Посчитайте присмыкающихся (создайте масив, поместите туда присмыкающихся и скажите сколько в нем объектов)
//- Сколько четвероногих?
//- Сколько здесь животных?
//- Сколько живых существ?

//enum Species {
//    case Human
//    case Animal
//    case Reptile
//}
//
//class Object {
//    var spicie : Species = .Animal
//    var legs = 4
//}
//
//class Human : Object {
//    override init() {
//        super.init()
//        spicie = .Human
//        legs = 2
//    }
//}
//
//class Crocodile : Object {
//    override init() {
//        super.init()
//        spicie = .Reptile
//        legs = 4
//    }
//}
//
//class Monkey : Object {
//    override init() {
//        super.init()
//        legs = 2
//    }
//}
//
//class Dog : Object {
//}
//
//class Giraffe : Object {
//}
//
//let human1 = Human()
//let human2 = Human()
//let monkey1 = Monkey()
//let monkey2 = Monkey()
//let monkey3 = Monkey()
//let dog1 = Dog()
//let dog2 = Dog()
//let dog3 = Dog()
//let giraffe1 = Giraffe()
//let giraffe2 = Giraffe()
//let crocodile1 = Crocodile()
//let crocodile2 = Crocodile()
//
//let livingBeing = [human1, human2, monkey1, monkey2, monkey3, dog1, dog2, dog3, giraffe1, giraffe2, crocodile1, crocodile2]
//
//func calculationAndPrint (array: [Object]) -> String {
//    var livingBeing = 0
//    var reptile = 0
//    var fourLegs = 0
//    var animal = 0
//
//    var resaultString = ""
//
//    for value in array {
//        if value.spicie == .Animal {
//            animal += 1
//        }
//        if value.spicie == .Reptile {
//            reptile += 1
//        }
//        if value.legs == 4 {
//            fourLegs += 1
//        }
//        if array.count > 0 {
//            livingBeing = array.count
//            resaultString = """
//            В переданном на обработку массиве живых существ содержатся следующие количества
//            единиц по запрашиваемым для подсчета показателям:
//                животоных - \(animal)
//                пресмыкающихся - \(reptile)
//                четвероногих - \(fourLegs)
//            Всего живых существ - \(livingBeing)
//            """
//        } else {
//            resaultString = "В переданном на обработку массиве пусто, нечего посчитать :("
//        }
//    }
//
//    return resaultString
//}
//
//print (calculationAndPrint(array: livingBeing))


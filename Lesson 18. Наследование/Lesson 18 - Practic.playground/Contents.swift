import UIKit
import Foundation

class Human {
    
    //final - защита для переопределения при наследовании
    
    var firstName : String = ""
    var lastName : String = ""
    
    var fullName : String {
        return firstName + " " + lastName
    }
    
    func syHello () -> String {
        return "Hello"
    }
    
//    init(firstName: String, lastName: String) {
//        self.firstName = firstName
//        self.lastName = lastName
//    }
}

class Student : Human {
    
    override func syHello () -> String {
        return super.syHello() + " my friend"
    }
}

class Kid : Human {
    
    var favoritToy : String = "iMac"

    override func syHello () -> String {
        return "agu"
    }
    
    override var fullName : String {
        return firstName
    }
    
    override var firstName: String {
        get {
            return super.firstName
        }
        set {
            super.firstName = newValue + " :)"
        }
    }
    
    override var lastName : String {
        didSet {
            print ("new value " + self.lastName)
        }
    }
}

var human = Human()

human.firstName = "Alex"
human.lastName = "Skutarenko"
human.syHello()


let student = Student()

student.firstName = "Max"
student.lastName = "Mix"
student.fullName
student.syHello()


let kid = Kid()

kid.firstName = "Kid"
kid.lastName = "123456"
kid.fullName
kid.syHello()

let array : [Human] = [kid, student, human]

for value in array {
    print (value.syHello())
}

